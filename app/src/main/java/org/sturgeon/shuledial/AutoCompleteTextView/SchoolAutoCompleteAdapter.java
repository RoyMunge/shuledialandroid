package org.sturgeon.shuledial.AutoCompleteTextView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.School;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 15/09/2016.
 */
public class SchoolAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private Context mContext;
    private List<School> schoolList = new ArrayList<School>();

    public SchoolAutoCompleteAdapter(Context context){
        mContext = context;
    }

    @Override
    public int getCount() {
        return schoolList.size();
    }

    @Override
    public Object getItem(int position) {
        return schoolList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.autocomplete_dropdown_item, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.text1)).setText(((School) getItem(position)).getSchoolName());
        ((TextView) convertView.findViewById(R.id.text2)).setText(((School)getItem(position)).getSchoolShortName());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List<School> schools = findSchools(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = schools;
                    filterResults.count = schools.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    schoolList = (List<School>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }

    private List<School> findSchools(String searchString) {
        ArrayList<School> schools = Loader.loadSchools(VolleySingleton.getInstance().getRequestQueue(),searchString);
        return schools;
    }
}
