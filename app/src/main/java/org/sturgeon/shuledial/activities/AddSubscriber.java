package org.sturgeon.shuledial.activities;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.student_accounts.AddStudentActivity;
import org.sturgeon.shuledial.transport.ReferralRequest;
import org.sturgeon.shuledial.transport.SubscriberAddRequest;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.CustomVerifyEditText;
import org.sturgeon.shuledial.utils.SessionManager;

import java.util.ArrayList;

public class AddSubscriber extends AppCompatActivity {

    SessionManager sessionManager;
    String currentUserPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subscriber);
        setTitle("Add User");

        sessionManager = new SessionManager(this);
        currentUserPhone = sessionManager.GetUser();

        final CustomVerifyEditText etPhoneNumber = (CustomVerifyEditText) findViewById(R.id.phone_number);
        final CustomVerifyEditText etName = (CustomVerifyEditText) findViewById(R.id.name);

        final Spinner spinnerStudents = (Spinner) findViewById(R.id.spinner_student);

        ArrayList<Student> students = MyApplication.getWritableDatabase().readStudents();
        ArrayAdapter<Student> studentAdapter = new ArrayAdapter<Student>(this,android.R.layout.simple_spinner_item, students);
        spinnerStudents.setAdapter(studentAdapter);
        spinnerStudents.setSelection(0);



        Button btnSubmit = (Button) findViewById(R.id.submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FormEditText[] allFields = {etPhoneNumber, etName};

                boolean allValid = true;
                for (FormEditText field : allFields) {
                    allValid = field.testValidity() && allValid;
                }
                if (allValid) {
                    Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            JSONObject jsonResponse = null;
                            try {
                                jsonResponse = response;
                                boolean isRegistered = jsonResponse.getBoolean("isRegistered");

                                if (isRegistered) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AddSubscriber.this);
                                    builder.setTitle("Success")
                                            .setMessage("User added successfully")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    AddSubscriber.this.finish();
                                                }
                                            })
                                            .create()
                                            .show();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AddSubscriber.this);
                                    builder.setTitle("Sorry")
                                            .setMessage("Unfortunately only the primary parent is allowed to add users")
                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    AddSubscriber.this.finish();
                                                }
                                            })
                                            .create()
                                            .show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    Student student = (Student) spinnerStudents.getSelectedItem();

                    SubscriberAddForm subscriberAddForm = new SubscriberAddForm(currentUserPhone, etName.getText().toString(), etPhoneNumber.getText().toString(), student.getStudentid().toString() );
                    SubscriberAddRequest subscriberAddRequest = null;
                    try{
                        subscriberAddRequest = new SubscriberAddRequest(new JSONObject(new Gson().toJson(subscriberAddForm)), responseListener);
                        VolleySingleton volleySingleton = VolleySingleton.getInstance();
                        RequestQueue queue = volleySingleton.getRequestQueue();
                        queue.add(subscriberAddRequest);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddSubscriber.this);
                    builder.setTitle("Submission Error")
                            .setMessage("Please confirm input details")
                            .setNegativeButton("Retry", null)
                            .create()
                            .show();
                }
            }
        });



    }

    private class SubscriberAddForm {
        private String msisdn;
        private String phoneNumber;
        private String name;
        private String studentid;

        public SubscriberAddForm(String msisdn, String phoneNumber, String name, String studentid) {
            this.msisdn = msisdn;
            this.phoneNumber = phoneNumber;
            this.name = name;
            this.studentid = studentid;
        }
    }
}
