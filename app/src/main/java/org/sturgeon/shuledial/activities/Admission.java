package org.sturgeon.shuledial.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.AdmissionDropdowns;
import org.sturgeon.shuledial.transport.AdmissionRequest;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.CustomVerifyEditText;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;
import java.util.Collection;

public class Admission extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admission);

        final Context context = this;

        final String schoolid = getIntent().getExtras().getString("schoolid");
        String schoolname = getIntent().getExtras().getString("schoolname");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(schoolname);

        Response.Listener<AdmissionDropdowns> responseListener = new Response.Listener<AdmissionDropdowns>() {
            @Override
            public void onResponse(AdmissionDropdowns response) {
                if (response != null) {

                    AdmissionDropdowns admissionDropdowns = response;
                    final Spinner schoolLevelSpinner = (Spinner) findViewById(R.id.admission_school_level);
                    final Spinner qualificationLevelSpinner = (Spinner) findViewById(R.id.admission_qualification_level);

                    ArrayList<String> schoolLevels = new ArrayList<>();
                    schoolLevels.add(0, "Select Joining Class");
                    schoolLevels.addAll(admissionDropdowns.getSchoolLevels());
                    ArrayList<String> qualificationLevels = new ArrayList<>();
                    qualificationLevels.add(0, "Select highest qualification level");
                    qualificationLevels.addAll(admissionDropdowns.getQualificationLevels());

                    ArrayAdapter<String> schoolLevelAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, schoolLevels);
                    schoolLevelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    schoolLevelSpinner.setAdapter(schoolLevelAdapter);
                    schoolLevelSpinner.setPrompt("Select Joining Class");

                    ArrayAdapter<String> qualificationLevelAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item,qualificationLevels);
                    qualificationLevelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    qualificationLevelSpinner.setAdapter(qualificationLevelAdapter);
                    qualificationLevelSpinner.setPrompt("Select Qualification Level");

                    final CustomVerifyEditText admissionParentName = (CustomVerifyEditText) findViewById(R.id.admission_parent_name);
                    final CustomVerifyEditText admissionStudentName= (CustomVerifyEditText) findViewById(R.id.admission_student_name);
                    final CustomVerifyEditText admissionPhoneNumber = (CustomVerifyEditText) findViewById(R.id.admission_phone_number);
                    final CustomVerifyEditText admissionEmail= (CustomVerifyEditText) findViewById(R.id.admission_email);
                    final CustomVerifyEditText admissionPreviousSchool= (CustomVerifyEditText) findViewById(R.id.admission_previous_school);
                    final CustomVerifyEditText admissionGrade = (CustomVerifyEditText) findViewById(R.id.admission_grade);
                    final CustomVerifyEditText admissionDescription = (CustomVerifyEditText) findViewById(R.id.admission_description);

                    Button admissionSubmit = (Button) findViewById(R.id.admission_submit);

                    admissionSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            FormEditText[] allFields = {admissionParentName, admissionStudentName, admissionPhoneNumber, admissionEmail, admissionPreviousSchool, admissionGrade, admissionDescription};
                            boolean allValid = true;
                            for (FormEditText field : allFields) {
                                allValid = field.testValidity() && allValid;
                            }
                            if (allValid) {

                                Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        JSONObject jsonResponse = null;
                                        try {
                                            jsonResponse = response;
                                            boolean isRegistered = jsonResponse.getBoolean("isRegistered");

                                            if (isRegistered) {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                                builder.setTitle("Success")
                                                        .setMessage("Admission Request Sent Successfully")
                                                        .setPositiveButton("OK", null)
                                                        .create()
                                                        .show();
                                            } else {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                                builder.setTitle("Request Failed")
                                                        .setMessage("System error")
                                                        .setNegativeButton("Retry", null)
                                                        .create()
                                                        .show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                };

                                AdmissionForm admissionForm = new AdmissionForm(admissionParentName.getText().toString(), admissionStudentName.getText().toString(), admissionPhoneNumber.getText().toString(), admissionEmail.getText().toString(), admissionPreviousSchool.getText().toString(), schoolLevelSpinner.getSelectedItem().toString(), qualificationLevelSpinner.getSelectedItem().toString(), admissionGrade.getText().toString(), Long.parseLong(schoolid) ,admissionDescription.getText().toString());
                                AdmissionRequest admissionRequest = null;
                                try {
                                    admissionRequest = new AdmissionRequest(new JSONObject(new Gson().toJson(admissionForm)), responseListener);
                                    VolleySingleton volleySingleton = VolleySingleton.getInstance();
                                    RequestQueue queue = volleySingleton.getRequestQueue();
                                    queue.add(admissionRequest);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Admission Request Error")
                                        .setMessage("Please confirm input details")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        }
                    });


                } else {

                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                L.m(error + "");
            }
        };

        Loader.loadAdmissionDropdowns(VolleySingleton.getInstance().getRequestQueue(), schoolid, responseListener, errorListener);




    }


    private class AdmissionForm{
        private String parentName;
        private String studentName;
        private String phonenumber;
        private String email;
        private String previousSchool;
        private String schoollevel;
        private String qualificationlevel;
        private String qualificationLevelResult;
        private Long schoolid;
        private String description;

        public AdmissionForm(String parentName, String studentName, String phonenumber, String email, String previousSchool, String schoollevel, String qualificationlevel, String qualificationLevelResult, Long schoolid, String description) {
            this.parentName = parentName;
            this.studentName = studentName;
            this.phonenumber = phonenumber;
            this.email = email;
            this.previousSchool = previousSchool;
            this.schoollevel = schoollevel;
            this.qualificationlevel = qualificationlevel;
            this.qualificationLevelResult = qualificationLevelResult;
            this.schoolid = schoolid;
            this.description = description;
        }
    }

}
