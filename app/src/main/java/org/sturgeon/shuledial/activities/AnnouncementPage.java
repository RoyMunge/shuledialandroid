package org.sturgeon.shuledial.activities;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.pojo.Announcement;

import java.text.SimpleDateFormat;

public class AnnouncementPage extends AppCompatActivity {

    SimpleDateFormat mFormatter = new SimpleDateFormat("EEEE, MMMM d");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement_page);
        String announcementid = getIntent().getExtras().getString("announcementid");
        Announcement announcement = MyApplication.getWritableDatabase().readAnnouncement(announcementid);

        setTitle("View announcement");

        TextView announcementTitle = (TextView) findViewById(R.id.announcement_title);
        TextView announcementdate = (TextView) findViewById(R.id.announcementdate);
        TextView announcementcontent = (TextView) findViewById(R.id.announcementcontent);

        announcementTitle.setText(announcement.getAnnouncementName());
        announcementdate.setText(mFormatter.format(announcement.getPostingDate()));
        announcementcontent.setText(announcement.getDescription());
    }
}
