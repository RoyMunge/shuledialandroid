package org.sturgeon.shuledial.activities;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.utils.ColorGenerator;
import org.sturgeon.shuledial.utils.SaveEventDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
public class EventPage extends AppCompatActivity {

    private DateFormat mFormatter = new SimpleDateFormat("EEEE, MMMM d");
    private DateFormat dateMonthFormat = new SimpleDateFormat("dd/MM");
    private DateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM HH:mm");
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    private int color;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_page);
        setTitle("View event");
        String eventid = getIntent().getExtras().getString("eventid");
        final Event event = MyApplication.getWritableDatabase().readEvent(eventid);

        LinearLayout titleLayout = (LinearLayout) findViewById(R.id.event_title_layout);
        color = generator.getColor(dateMonthFormat.format(event.getEventDate()));
        titleLayout.setBackgroundColor(color);

        LinearLayout locationLayout = (LinearLayout) findViewById(R.id.location);
        LinearLayout endTimeLayout = (LinearLayout) findViewById(R.id.end_time_layout);

        TextView eventTitle = (TextView) findViewById(R.id.event_title_text);
        TextView eventdate = (TextView) findViewById(R.id.eventdate_text);
        TextView eventcontent = (TextView) findViewById(R.id.eventcontent);
        TextView location = (TextView) findViewById(R.id.location_text);
        TextView startTime = (TextView) findViewById(R.id.start_time);
        TextView endTime = (TextView) findViewById(R.id.end_time);

        eventTitle.setText(event.getEventName());
        eventdate.setText(mFormatter.format(event.getEventDate()));
        eventcontent.setText(event.getDescription());
        startTime.setText(timeFormat.format(event.getEventDate()));

        if (event.getLocation().equals("")) {
            locationLayout.setVisibility(View.GONE);
        } else {
            locationLayout.setVisibility(View.VISIBLE);
            location.setText(event.getLocation());
        }

        if (event.getEndDate().getTime() != 0) {
            endTimeLayout.setVisibility(View.VISIBLE);
            if (dateMonthFormat.format(event.getEventDate()).equals(dateMonthFormat.format(event.getEndDate()))) {
                endTime.setText(timeFormat.format(event.getEndDate()));
            } else {
                endTime.setText(dateTimeFormat.format(event.getEndDate()));
            }
        } else {
            endTimeLayout.setVisibility(View.GONE);
        }

        Button btnSave = (Button) findViewById(R.id.button_save);
        Button btnShare = (Button) findViewById(R.id.button_share);
        btnSave.setBackgroundColor(color);
        btnShare.setBackgroundColor(color);

        assert btnSave!=null;
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveEventDialog viewDialog = new SaveEventDialog();
                viewDialog.showDialog(v.getContext(),event,"Save Event","Save event on your main calender");
            }
        });

        assert btnShare!=null;
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Shiners Girls High School event. \n" + event.getEventName() + "\n at : " + event.getLocation() + "\n on date: " + event.getEventDate() + " " + timeFormat.format(event.getEventDate()) + " - " + timeFormat.format(event.getEndDate()));
                sendIntent.setType("text/plain");
                v.getContext().startActivity(sendIntent);
            }
        });

    }
}
