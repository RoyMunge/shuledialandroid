package org.sturgeon.shuledial.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.EventsLoadedListener;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.recycler_adapters.AdapterEvents;
import org.sturgeon.shuledial.tasks.CalendarEventsTask;
import org.sturgeon.shuledial.tasks.TaskLoadEvents;
import org.sturgeon.shuledial.utils.CalendarDecorators.OneDayDecorator;
import org.sturgeon.shuledial.utils.SessionManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.Executors;

public class EventsCalendar extends AppCompatActivity implements OnDateSelectedListener, EventsLoadedListener {

    private static final String STATE_EVENTS = "state_events";
    private MaterialCalendarView widget;
    private DateFormat mFormatter = new SimpleDateFormat("dd/MM");
    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    private ArrayList<Event> events = new ArrayList<>();
    private SessionManager sessionManager;
    private String currentStudentId;
    private AdapterEvents adapterEvents;
    private RecyclerView recyclerEvents;
    private static final DateFormat FORMATTER = SimpleDateFormat.getDateInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_calendar);
        sessionManager = new SessionManager(this);
        currentStudentId = sessionManager.GetCurrentStudent();
        recyclerEvents = (RecyclerView) findViewById(R.id.dayEvents);
        recyclerEvents.setLayoutManager(new LinearLayoutManager((this)));
        adapterEvents = new AdapterEvents(this);
        recyclerEvents.setAdapter(adapterEvents);

        widget = (MaterialCalendarView) findViewById(R.id.calendarView);
        widget.addDecorator(oneDayDecorator);
        widget.setOnDateChangedListener(this);

        if (savedInstanceState != null) {
            events = savedInstanceState.getParcelableArrayList(STATE_EVENTS);
        } else {
            events = MyApplication.getWritableDatabase().readEvents(Long.parseLong(currentStudentId));
            if (events.isEmpty()) {
                new TaskLoadEvents(this, currentStudentId).execute();
            }
        }

        if (!events.isEmpty()) {
            //new CalendarEventsTask(events).executeOnExecutor(Executors.newSingleThreadExecutor());
        }
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        setupRecyclerView();
        Toast.makeText(this,FORMATTER.format(date.getDate()),Toast.LENGTH_SHORT);
        System.out.println("Selected date:  "+  FORMATTER.format(date.getDate()));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Event")
                .setMessage(FORMATTER.format(date.getDate()))
                .setPositiveButton("OK", null)
                .create()
                .show();
    }

    private String getSelectedDatesString() {
        CalendarDay date = widget.getSelectedDate();
        if (date == null) {
            return "No Selection";
        }

        return FORMATTER.format(date.getDate());
    }

    @Override
    public void onEventsLoaded(ArrayList<Event> events) {
        //new CalendarEventsTask(events).executeOnExecutor(Executors.newSingleThreadExecutor());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_EVENTS, events);
    }

    private void setupRecyclerView() {
        ArrayList<Event> dayEvents;
        System.out.println("date: " + Long.toString(widget.getSelectedDate().getDate().getTime()));
        dayEvents = MyApplication.getWritableDatabase().readEventsbyDate(Long.toString(widget.getSelectedDate().getDate().getTime()));
        if (!dayEvents.isEmpty()) {
            adapterEvents.setAllEvents(dayEvents);
        }
    }
}
