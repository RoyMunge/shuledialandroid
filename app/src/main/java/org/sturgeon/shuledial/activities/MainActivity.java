package org.sturgeon.shuledial.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.firebase_cloud_messaging.MyFirebaseInstanceIDService;
import org.sturgeon.shuledial.fragments.FragmentFees;
import org.sturgeon.shuledial.student_accounts.NoStudent;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.fragments.FragmentAnnouncements;
import org.sturgeon.shuledial.fragments.FragmentEvents;
import org.sturgeon.shuledial.fragments.FragmentResults;
import org.sturgeon.shuledial.navigation.NavigationDrawer;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.tasks.TaskUpdateFCMToken;
import org.sturgeon.shuledial.tasks.TaskUpdateStudents;
import org.sturgeon.shuledial.tasks.TaskUpdateValidUntil;
import org.sturgeon.shuledial.utils.SessionManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String currentUserPhone;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private SessionManager sessionManager;
    private Context context;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle("ShuleDial");
        sessionManager = new SessionManager(this);
        currentUserPhone = sessionManager.GetUser();
        ArrayList<Student> students = MyApplication.getWritableDatabase().readStudents();

        if(students.isEmpty()){
            Intent intent = new Intent(this,NoStudent.class);
            startActivity(intent);
            finish();
        } else {
            String token = sessionManager.GetFCMToken();
            if(!token.equals("") && !token.equals("null")) {
                new TaskUpdateFCMToken(currentUserPhone, token).execute();
            } else {
                Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
                startService(intent);
            }
            new TaskUpdateStudents(currentUserPhone).execute();
            new TaskUpdateValidUntil(currentUserPhone, sessionManager).execute();
            setupDrawer(students);
            setupViewPager();
        }
    }

    private void setupDrawer(ArrayList<Student> students) {
        NavigationDrawer navigationDrawer = new NavigationDrawer(this,mToolbar);
        navigationDrawer.setupMainDrawer();
        navigationDrawer.addStudents(students);
    }

    private void setupViewPager() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.content_main);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.main_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        /*tabLayout.getTabAt(0).setIcon(new IconicsDrawable(context)
                .icon(FontAwesome.Icon.faw_list_alt)
                .color(Color.WHITE)
                .sizeDp(24));*/

        tabLayout.getTabAt(0).setIcon( new IconicsDrawable(context)
                .icon(FontAwesome.Icon.faw_bullhorn)
                .color(Color.GRAY)
                .sizeDp(24));

        tabLayout.getTabAt(1).setIcon(new IconicsDrawable(context)
                .icon(FontAwesome.Icon.faw_calendar)
                .color(Color.GRAY)
                .sizeDp(24));

        tabLayout.getTabAt(2).setIcon( new IconicsDrawable(context)
                .icon(FontAwesome.Icon.faw_bar_chart)
                .color(Color.GRAY)
                .sizeDp(24));

        tabLayout.getTabAt(3).setIcon(new IconicsDrawable(context)
                .icon(FontAwesome.Icon.faw_money)
                .color(Color.GRAY)
                .sizeDp(24));

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mViewPager.setCurrentItem(position);
                switch(position) {
                   /* case 0:

                        mToolbar.setTitle("ShuleDial Timeline");
                        break;*/
                    case 0:
                        mToolbar.setTitle("News and Announcements");
                        break;
                    case 1:
                        mToolbar.setTitle("Calendar of Events");
                        break;
                    case 2:
                        mToolbar.setTitle("Exam Results");
                        break;
                    case 3:
                        mToolbar.setTitle("Fees");
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        FragmentManager fragmentManager;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentManager = fm;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Fragment fragment = null;
            switch (position) {
               /* case 0:
                    fragment = FragmentAnnouncements.newInstance();
                    break;*/
                case 0:
                    fragment = FragmentAnnouncements.newInstance();
                    break;
                case 1:
                    fragment = FragmentEvents.newInstance();
                    break;
                case 2:
                    fragment = FragmentResults.newInstance();
                    break;
                case 3:
                    fragment = FragmentFees.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }


}
