package org.sturgeon.shuledial.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.ExamResultsOverviewLoadedListener;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.tasks.TaskLoadResultsOverview;
import org.sturgeon.shuledial.utils.SessionManager;
import org.sturgeon.shuledial.utils.Sorter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ResultsGraph extends AppCompatActivity implements ExamResultsOverviewLoadedListener {

    private static final String STATE_RESULTS = "state_results";
    private ArrayList<ExamResultsOverview> results = new ArrayList<>();
    private SessionManager sessionManager;
    private String currentStudentId;
    private Sorter sorter;
    private BarChart resultsChart;
    private ArrayList<BarEntry> barEntries;
    private ArrayList<String> exams;
    private final DateFormat mFormatter = new SimpleDateFormat("dd/MM");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_graph);
        sessionManager = new SessionManager(this);
        currentStudentId = sessionManager.GetCurrentStudent();
        resultsChart = (BarChart) findViewById(R.id.results_chart);

        if(savedInstanceState != null){
            results = savedInstanceState.getParcelableArrayList(STATE_RESULTS);
        } else {
            results = MyApplication.getWritableDatabase().readExamResultsOverview(currentStudentId);
            if(results.isEmpty()){
                new TaskLoadResultsOverview(this, currentStudentId).execute();
            }
        }

        sorter = new Sorter();
        if(!results.isEmpty()) {
            createResultsGraph(results);
        }
    }

    private void createResultsGraph(ArrayList<ExamResultsOverview> results){
        exams = new ArrayList<>();
        barEntries = new ArrayList<>();
        for(int i = 0; i < results.size(); i++){
            exams.add(results.get(i).getExamName());
            try {
                barEntries.add(new BarEntry(Float.valueOf(results.get(i).getTotal()), i));
            } catch (NumberFormatException e){
                barEntries.add(new BarEntry(50F, i));
            }
        }
        BarDataSet barDataSet = new BarDataSet(barEntries,"Total Marks");
        //BarData barData = new BarData(exams,barDataSet);
        //resultsChart.setData(barData);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_RESULTS,results);
    }

    @Override
    public void onExamResultsOverviewLoaded(ArrayList<ExamResultsOverview> results) {
        if(!results.isEmpty()) {
            createResultsGraph(results);
        }
    }
}
