package org.sturgeon.shuledial.activities;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.animation.DecelerateInterpolator;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.AnalysisMetricLoadedListener;
import org.sturgeon.shuledial.callbacks.ExamMarksLoadedListener;
import org.sturgeon.shuledial.pojo.AnalysisMetric;
import org.sturgeon.shuledial.pojo.ExamMarks;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.tasks.TaskLoadAnalysisMetrics;
import org.sturgeon.shuledial.tasks.TaskLoadExamMarks;
import org.sturgeon.shuledial.utils.ArcProgress.ArcProgress;
import org.sturgeon.shuledial.utils.ArcProgress.ArcProgressUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

public class ResultsPage extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ExamMarksLoadedListener, AnalysisMetricLoadedListener {

    DateFormat mFormatter = SimpleDateFormat.getDateInstance();

    private static final String STATE_ANALYSIS_METRICS = "state_analysis_metrics";
    private static final String STATE_EXAM_MARKS = "state_exam_marks";
    private static final String STATE_RESULTS_OVERVIEW = "state_results_overview";

    private ArrayList<ExamMarks> examMarksArrayList = new ArrayList<>();
    private ArrayList<AnalysisMetric> analysisMetricArrayList = new ArrayList<>();
    private ExamResultsOverview examResultsOverview;
    private SwipeRefreshLayout swipeRefreshLayout;
    TableLayout tableResults;
    TableLayout tableAnalysis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_page);
        String examid = getIntent().getExtras().getString("examid");
        String studentid = getIntent().getExtras().getString("studentid");

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeResultsPage);
        swipeRefreshLayout.setOnRefreshListener(this);

        tableResults = (TableLayout) findViewById(R.id.table_results);
        tableAnalysis = (TableLayout) findViewById(R.id.table_analysis);



        if (savedInstanceState != null) {
            examResultsOverview = savedInstanceState.getParcelable(STATE_RESULTS_OVERVIEW);
            analysisMetricArrayList = savedInstanceState.getParcelableArrayList(STATE_ANALYSIS_METRICS);
            examMarksArrayList = savedInstanceState.getParcelableArrayList(STATE_EXAM_MARKS);
            setupExamMarks();
            setupAnalysisMetrics();
        } else {
            examResultsOverview = MyApplication.getWritableDatabase().readSingleExamResultsOverview(studentid, examid);
            analysisMetricArrayList = MyApplication.getWritableDatabase().readAnalysisMetrics(studentid, examid);
            examMarksArrayList = MyApplication.getWritableDatabase().readExamMarks(studentid, examid);
            if (!analysisMetricArrayList.isEmpty()) {
                setupAnalysisMetrics();
            } else {
                new TaskLoadAnalysisMetrics(this, studentid, examid).execute();
            }
            if (!examMarksArrayList.isEmpty()) {
                setupExamMarks();
            } else {
                new TaskLoadExamMarks(this, studentid, examid).execute();
            }
        }

        setTitle("view Results");
        TextView examName = (TextView) findViewById(R.id.examName);
        TextView resultdate = (TextView) findViewById(R.id.resultdate);
        if(examResultsOverview != null) {
            examName.setText(examResultsOverview.getExamName().toUpperCase());
            resultdate.setText(mFormatter.format(examResultsOverview.getExamDate()));
            setupResultsOverview();
        }

    }

    private void setupResultsOverview() {
        //averageArcProgress.setProgress(Integer.valueOf(examResults.getAverage()));
        try {
            Typeface digital = Typeface.createFromAsset(getAssets(), "fonts/digital.ttf");
            TextView gradeVariable = (TextView) findViewById(R.id.gradeVariable);
            gradeVariable.setText(examResultsOverview.getGrade());

            TextView positionVariable = (TextView) findViewById(R.id.positionVariable);
            positionVariable.setText(examResultsOverview.getPosition());

            ArcProgress averageArcProgress = (ArcProgress) findViewById(R.id.arc_progress);
            ObjectAnimator animator;
            try {
                animator = ObjectAnimator.ofInt(averageArcProgress, "progress", 0, (int) Float.parseFloat(examResultsOverview.getAverage()));
            } catch (NumberFormatException e){
                animator = ObjectAnimator.ofInt(averageArcProgress, "progress", 0, 0);
            }
            animator.setDuration(5000);
            animator.setInterpolator(new DecelerateInterpolator());
            animator.start();

            TextView totalMarks = (TextView) findViewById(R.id.totalMarks);
            totalMarks.setTypeface(digital);
            Shader shader = new LinearGradient(
                    0, 0, 0, totalMarks.getTextSize(),
                    Color.parseColor("#FF003333"), Color.parseColor("#FF05C1FF"),
                    Shader.TileMode.CLAMP);
            totalMarks.getPaint().setShader(shader);
            totalMarks.setText(examResultsOverview.getTotal());
            //animateTextView(0, Integer.valueOf(examResultsOverview.getTotal()), totalMarks);
        }catch (Exception e){

        }
    }

    private void setupExamMarks() {
        tableResults.removeAllViews();
        for (ExamMarks examMarks : examMarksArrayList) {
            TableRow tableRow = new TableRow(this);
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setMinimumHeight((int) ArcProgressUtils.sp2px(getResources(), 30));
            TextView subjectText = new TextView(this);
            subjectText.setGravity(Gravity.CENTER);
            subjectText.setText(examMarks.getShortName());
            TextView marksText = new TextView(this);
            marksText.setGravity(Gravity.CENTER);
            marksText.setText(examMarks.getMarks());
            TextView gradeText = new TextView(this);
            gradeText.setGravity(Gravity.CENTER);
            gradeText.setText(examMarks.getPosition() == null ? "" : examMarks.getGrade());
            TextView positionText = new TextView(this);
            positionText.setGravity(Gravity.CENTER);
            positionText.setText(examMarks.getPosition() == null ? "" : examMarks.getPosition());
            tableRow.addView(subjectText);
            tableRow.addView(marksText);
            tableRow.addView(gradeText);
            tableRow.addView(positionText);
            tableResults.addView(tableRow);
        }
    }

    private void setupAnalysisMetrics() {
        tableAnalysis.removeAllViews();
        Iterator<AnalysisMetric> analysisMetricIterator = analysisMetricArrayList.iterator();
        while (analysisMetricIterator.hasNext()){
            TableRow tableRow = new TableRow(this);
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setMinimumHeight((int) ArcProgressUtils.sp2px(getResources(), 30));

            AnalysisMetric analysisMetric1 = analysisMetricIterator.next();

            TextView analysisText1 = new TextView(this);
            analysisText1.setGravity(Gravity.CENTER);
            analysisText1.setText(analysisMetric1.getAnalysisMetricName());
            TextView entryText1 = new TextView(this);
            entryText1.setGravity(Gravity.CENTER);
            entryText1.setText(analysisMetric1.getEntry());

            tableRow.addView(analysisText1);
            tableRow.addView(entryText1);

            if(analysisMetricIterator.hasNext()){
                AnalysisMetric analysisMetric2 = analysisMetricIterator.next();

                TextView analysisText2 = new TextView(this);
                analysisText2.setGravity(Gravity.CENTER);
                analysisText2.setText(analysisMetric2.getShortName());
                TextView entryText2 = new TextView(this);
                entryText2.setGravity(Gravity.CENTER);
                entryText2.setText(analysisMetric2.getEntry());

                tableRow.addView(analysisText2);
                tableRow.addView(entryText2);

            }

            tableAnalysis.addView(tableRow);

        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(5000);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue().toString());

            }
        });
        valueAnimator.start();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_ANALYSIS_METRICS, analysisMetricArrayList);
        outState.putParcelableArrayList(STATE_EXAM_MARKS, examMarksArrayList);
        outState.putParcelable(STATE_RESULTS_OVERVIEW, examResultsOverview);
    }

    @Override
    public void onExamMarksLoaded(ArrayList<ExamMarks> examMarksArrayList) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if (examMarksArrayList != null && !examMarksArrayList.isEmpty()) {
            this.examMarksArrayList = examMarksArrayList;
            setupExamMarks();
        }
    }

    @Override
    public void onAnalysisMetricLoaded(ArrayList<AnalysisMetric> analysisMetricArrayList) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if (analysisMetricArrayList != null && !analysisMetricArrayList.isEmpty()) {
            this.analysisMetricArrayList = analysisMetricArrayList;
            setupAnalysisMetrics();
        }
    }

    @Override
    public void onRefresh() {
        new TaskLoadAnalysisMetrics(this, examResultsOverview.getStudentid().toString(), examResultsOverview.getExamid().toString()).execute();
        new TaskLoadExamMarks(this, examResultsOverview.getStudentid().toString(), examResultsOverview.getExamid().toString()).execute();
    }
}
