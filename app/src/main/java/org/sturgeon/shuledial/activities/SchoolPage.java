package org.sturgeon.shuledial.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.callbacks.SchoolDetailsLoadedListener;
import org.sturgeon.shuledial.fragments.FragmentSchoolAdmission;
import org.sturgeon.shuledial.fragments.FragmentSchoolBio;
import org.sturgeon.shuledial.fragments.FragmentSchoolContacts;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolDetails;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;


public class SchoolPage extends AppCompatActivity implements SchoolDetailsLoadedListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private String schoolid;
    private String schoolname;
    private ViewPager mViewPager;
    private String schoolShortName;

    TextView tvSchoolName;
    TextView tvSchoolMotto;
    TextView tvSchoolWebsite;
    TextView tvSchoolCounty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_page);

        schoolShortName = getIntent().getExtras().getString("shortname");

        new TaskLoadSchoolDetails(this, schoolShortName).execute();

        tvSchoolName = (TextView) findViewById(R.id.school_name);
        tvSchoolMotto = (TextView) findViewById(R.id.school_motto);
        tvSchoolWebsite = (TextView) findViewById(R.id.school_website);
        tvSchoolCounty = (TextView) findViewById(R.id.school_county);

        setupViewPager();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.admission_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Admission.class);
                intent.putExtra("schoolid", schoolid);
                intent.putExtra("schoolname", schoolname);
                startActivity(intent);
            }
        });


    }

    private void setupViewPager() {
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.school_page_content);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.school_page_tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public void onSchoolDetialsLoadedListener(SchoolBio schoolBio) {
        if (schoolBio != null){
            tvSchoolName.setText(schoolBio.getName());
            tvSchoolMotto.setText(schoolBio.getMotto());
            tvSchoolWebsite.setText(schoolBio.getWebsite());
            tvSchoolCounty.setText(schoolBio.getCounty() + " county; " + schoolBio.getSubCounty() + " sub-county" );

            schoolid = schoolBio.getSchoolid().toString();
            schoolname = schoolBio.getName();
        }

    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        FragmentManager fragmentManager;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentManager = fm;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = FragmentSchoolBio.newInstance(schoolShortName);
                    break;
                case 1:
                    fragment = FragmentSchoolContacts.newInstance(schoolShortName);
                    break;
                case 2:
                    fragment = FragmentSchoolAdmission.newInstance(schoolShortName);
                    break;
            }
            return fragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String pageTitle = "";
            switch (position) {
                case 0:
                    pageTitle = "BIO";
                    break;
                case 1:
                    pageTitle = "CONTACT";
                    break;
                case 2:
                    pageTitle = "ADMISSIONS";
                    break;
            }
            return pageTitle;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
