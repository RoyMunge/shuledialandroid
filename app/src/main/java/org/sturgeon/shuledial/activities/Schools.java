package org.sturgeon.shuledial.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.sturgeon.shuledial.AutoCompleteTextView.DelayAutoCompleteTextView;
import org.sturgeon.shuledial.AutoCompleteTextView.SchoolAutoCompleteAdapter;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.callbacks.SchoolSearchDropdownsLoadedListener;
import org.sturgeon.shuledial.callbacks.SchoolsBioLoadedListener;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.School;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.pojo.SchoolBioMinimal;
import org.sturgeon.shuledial.pojo.SchoolSearchDropdowns;
import org.sturgeon.shuledial.recycler_adapters.AdapterSchools;
import org.sturgeon.shuledial.student_accounts.SetupStudentFragment;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolSearchDropdowns;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolsBio;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;
import org.sturgeon.shuledial.utils.RecyclerViewListener.ClickListener;
import org.sturgeon.shuledial.utils.RecyclerViewListener.RecyclerTouchListener;

import java.util.ArrayList;

public class Schools extends AppCompatActivity implements SchoolsBioLoadedListener, SchoolSearchDropdownsLoadedListener,SwipeRefreshLayout.OnRefreshListener {
    private static final String STATE_SCHOOLS = "state_schools";
    private ArrayList<SchoolBioMinimal> schoolsBio = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerSchoolsBio;
    private AdapterSchools adapterSchools;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schools);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Advanced school search", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        new TaskLoadSchoolSearchDropdowns(this).execute();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeSchoolsBio);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerSchoolsBio = (RecyclerView) findViewById(R.id.listSchoolBio);
        recyclerSchoolsBio.setLayoutManager(new LinearLayoutManager(this));
        adapterSchools = new AdapterSchools(this);
        recyclerSchoolsBio.setAdapter(adapterSchools);

        recyclerSchoolsBio.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerSchoolsBio, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (position!=0) {
                    Intent intent = new Intent(view.getContext(), SchoolPage.class);
                    intent.putExtra("shortname", schoolsBio.get(position - 1).getShortName());
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        if(savedInstanceState != null){
            schoolsBio = savedInstanceState.getParcelableArrayList(STATE_SCHOOLS);
        } else {
            new TaskLoadSchoolsBio(this).execute();
        }

        if (schoolsBio.isEmpty()){
            adapterSchools.setSchoolsBio(schoolsBio);
        }

        final DelayAutoCompleteTextView schoolName = (DelayAutoCompleteTextView) findViewById(R.id.search_school);
        schoolName.setThreshold(3);
        schoolName.setAdapter(new SchoolAutoCompleteAdapter(this));
        schoolName.setLoadingIndicator(
                (android.widget.ProgressBar) findViewById(R.id.pb_loading_indicator));
        schoolName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                School school = (School) adapterView.getItemAtPosition(position);
                schoolName.setText(school.getSchoolName());
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_SCHOOLS,schoolsBio);
    }

    @Override
    public void onRefresh() {
        L.t(this, "Refreshing Schools");
        new TaskLoadSchoolsBio(this).execute();
    }

    @Override
    public void onSchoolsBioLoadedListener(ArrayList<SchoolBioMinimal> schoolBio) {
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
        if(!schoolBio.isEmpty()) {
            this.schoolsBio = schoolBio;
            adapterSchools.setSchoolsBio(schoolBio);
        }
    }

    @Override
    public void onSchoolSearchDropdownLoaded(SchoolSearchDropdowns schoolSearchDropdowns) {
        if (schoolSearchDropdowns != null) {
            adapterSchools.setSchoolSearchDropdowns(schoolSearchDropdowns);
        }
    }
}
