package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.AnalysisMetric;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public interface AnalysisMetricLoadedListener {
   void onAnalysisMetricLoaded(ArrayList<AnalysisMetric> analysisMetricArrayList);
}
