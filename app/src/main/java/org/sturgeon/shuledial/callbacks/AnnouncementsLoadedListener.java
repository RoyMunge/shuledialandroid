package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.Announcement;

import java.util.ArrayList;

/**
 * Created by User on 28/05/2016.
 */
public interface AnnouncementsLoadedListener {
    void onAnnouncementsLoaded(ArrayList<Announcement> announcements);
}
