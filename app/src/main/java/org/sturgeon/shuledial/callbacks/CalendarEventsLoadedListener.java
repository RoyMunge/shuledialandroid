package org.sturgeon.shuledial.callbacks;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.util.ArrayList;

/**
 * Created by User on 08/11/2016.
 */
public interface CalendarEventsLoadedListener {
    void onCalendarEventsLoaded(ArrayList<CalendarDay> calendarDays);
}
