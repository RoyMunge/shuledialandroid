package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.Event;

import java.util.ArrayList;

/**
 * Created by User on 28/05/2016.
 */
public interface EventsLoadedListener {
    void onEventsLoaded(ArrayList<Event> events);
}
