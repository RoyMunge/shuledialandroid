package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.ExamMarks;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public interface ExamMarksLoadedListener {
    void onExamMarksLoaded(ArrayList<ExamMarks> examMarksArrayList);
}
