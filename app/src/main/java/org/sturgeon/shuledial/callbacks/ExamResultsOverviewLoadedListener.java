package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.ExamResultsOverview;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public interface ExamResultsOverviewLoadedListener {
    void onExamResultsOverviewLoaded(ArrayList<ExamResultsOverview> examResultsOverviewArrayList);
}
