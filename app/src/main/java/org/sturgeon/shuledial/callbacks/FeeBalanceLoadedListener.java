package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.FeeBalance;

/**
 * Created by User on 06/02/2017.
 */

public interface FeeBalanceLoadedListener {
    void onFeeBalanceLoaded(FeeBalance feeBalance);
}
