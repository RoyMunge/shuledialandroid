package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.FeeDropdowns;

/**
 * Created by User on 06/02/2017.
 */

public interface FeeDropdownsLoadedListener {
    void onFeeDropdownsLoaded(FeeDropdowns feeDropdowns);
}
