package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.FeeItem;

import java.util.ArrayList;

/**
 * Created by User on 06/02/2017.
 */

public interface FeeItemLoadedListener {
    void onFeeItemLoaded(ArrayList<FeeItem> feeItems);
}
