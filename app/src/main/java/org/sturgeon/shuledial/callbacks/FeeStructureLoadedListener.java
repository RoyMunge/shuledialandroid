package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.FeeStructure;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public interface FeeStructureLoadedListener {
    void onFeeStructureLoaded(ArrayList<FeeStructure> feeStructureArrayList);
}
