package org.sturgeon.shuledial.callbacks;

/**
 * Created by Roy on 04/04/2018.
 */

public interface PhoneVerifiedListener {
    void onPhoneVerified(String verificationStatus);
}
