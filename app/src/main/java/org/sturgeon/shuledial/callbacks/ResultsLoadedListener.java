package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.ExamResultsMinimal;

import java.util.ArrayList;

/**
 * Created by User on 15/06/2016.
 */
public interface ResultsLoadedListener {
    void onResultsLoaded(ArrayList<ExamResultsMinimal> results);
}
