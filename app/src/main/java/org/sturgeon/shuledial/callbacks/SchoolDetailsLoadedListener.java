package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.SchoolBio;

/**
 * Created by User on 12/02/2017.
 */

public interface SchoolDetailsLoadedListener {
    void onSchoolDetialsLoadedListener(SchoolBio schoolBio);
}
