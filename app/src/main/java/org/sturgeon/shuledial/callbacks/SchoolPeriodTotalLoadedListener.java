package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.SchoolPeriodTotal;

/**
 * Created by User on 06/02/2017.
 */

public interface SchoolPeriodTotalLoadedListener {
    void onSchoolPeriodTotalLoaded(SchoolPeriodTotal schoolPeriodTotal);
}
