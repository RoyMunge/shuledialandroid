package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.SchoolSearchDropdowns;

import java.util.ArrayList;

/**
 * Created by User on 31/01/2017.
 */

public interface SchoolSearchDropdownsLoadedListener {
    void onSchoolSearchDropdownLoaded(SchoolSearchDropdowns schoolSearchDropdowns);
}
