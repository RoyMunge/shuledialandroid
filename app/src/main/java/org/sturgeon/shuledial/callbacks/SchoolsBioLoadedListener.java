package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.SchoolBioMinimal;

import java.util.ArrayList;

/**
 * Created by User on 24/11/2016.
 */
public interface SchoolsBioLoadedListener {
    void onSchoolsBioLoadedListener(ArrayList<SchoolBioMinimal> schoolBio);

}
