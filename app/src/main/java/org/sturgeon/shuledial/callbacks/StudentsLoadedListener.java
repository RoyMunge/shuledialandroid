package org.sturgeon.shuledial.callbacks;

import org.sturgeon.shuledial.pojo.Student;

import java.util.ArrayList;

/**
 * Created by Roy on 5/25/2017.
 */

public interface StudentsLoadedListener {
    void onStudentsLoaded(ArrayList<Student> students);
}
