package org.sturgeon.shuledial.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.AnalysisMetric;
import org.sturgeon.shuledial.pojo.Announcement;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.pojo.ExamMarks;
import org.sturgeon.shuledial.pojo.ExamResults;
import org.sturgeon.shuledial.pojo.ExamResultsMinimal;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.pojo.FeeBalance;
import org.sturgeon.shuledial.pojo.FeeStructure;
import org.sturgeon.shuledial.pojo.Student;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 28/05/2016.
 */
public class Database {
    private DatabaseHelper mHelper;
    private SQLiteDatabase mDatabase;

    public Database(Context context) {
        mHelper = new DatabaseHelper(context);
        mDatabase = mHelper.getWritableDatabase();
    }

    public void insertStudent(Student student) {
        String sql = "INSERT INTO students VALUES (?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        statement.bindLong(1, student.getStudentid());
        statement.bindString(2, student.getAdmission());
        statement.bindString(3, student.getStudentName());
        statement.bindString(4, student.getSchoolName());
        statement.execute();
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }

    public Student readStudent(Long studentid) {
        String sql = "SELECT * from students where studentid = " + studentid.toString() + ";";
        Cursor cursor = mDatabase.rawQuery(sql, null);
        Student student = new Student();

        if (cursor != null && cursor.moveToFirst()) {
            try {
                student.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                student.setAdmission(cursor.getString(cursor.getColumnIndex("admission")));
                student.setStudentName(cursor.getString(cursor.getColumnIndex("studentName")));
                student.setSchoolName(cursor.getString(cursor.getColumnIndex("schoolName")));
                return student;
            } finally {
                cursor.close();
            }
        } else {
            return null;
        }

    }

    public void insertStudents(ArrayList<Student> students, boolean clearPrevious) {
        if (clearPrevious) {
            mDatabase.delete("students", null, null);
        }

        String sql = "INSERT INTO students VALUES (?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < students.size(); i++) {
            Student student = students.get(i);
            statement.clearBindings();
            statement.bindLong(1, student.getStudentid());
            statement.bindString(2, student.getAdmission());
            statement.bindString(3, student.getStudentName());
            statement.bindString(4, student.getSchoolName());
            statement.execute();
        }
        //set the transaction as successful and end the transaction
        L.m("inserting entries " + students.size() + new Date(System.currentTimeMillis()));
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }

    public ArrayList<Student> readStudents() {
        ArrayList<Student> students = new ArrayList<>();
        String[] columnns = {"studentid", "admission", "studentName", "schoolName"};
        Cursor cursor = mDatabase.query("students", columnns, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Student student = new Student();
                student.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                student.setAdmission(cursor.getString(cursor.getColumnIndex("admission")));
                student.setStudentName(cursor.getString(cursor.getColumnIndex("studentName")));
                student.setSchoolName(cursor.getString(cursor.getColumnIndex("schoolName")));
                students.add(student);

            } while (cursor.moveToNext());
        }
        return students;
    }

    public void insertAnnouncements(ArrayList<Announcement> announcements, boolean clearPrevious) {
        if (clearPrevious) {
            mDatabase.delete("announcements", null, null);
        }

        String sql = "INSERT INTO announcements VALUES (?,?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < announcements.size(); i++) {
            Announcement announcement = announcements.get(i);
            statement.clearBindings();
            statement.bindLong(1, announcement.getAnnouncementid());
            statement.bindLong(2, announcement.getStudentid());
            statement.bindString(3, announcement.getAnnouncementName());
            statement.bindLong(4, announcement.getPostingDate().getTime());
            statement.bindString(5, announcement.getDescription());
            statement.execute();
        }
        //set the transaction as successful and end the transaction
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }

    public Announcement readAnnouncement(String annoucementid){
        String sql = "SELECT * from announcements where announcementid = " + annoucementid + ";";
        Cursor cursor = mDatabase.rawQuery(sql, null);
        Announcement announcement = new Announcement();

        if(cursor != null && cursor.moveToFirst()){
            try {
                announcement.setAnnouncementid(cursor.getLong(cursor.getColumnIndex("announcementid")));
                announcement.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                announcement.setAnnouncementName(cursor.getString(cursor.getColumnIndex("announcementName")));
                Long dateMillis = cursor.getLong(cursor.getColumnIndex("postingDate"));
                announcement.setPostingDate(new Date(dateMillis));
                announcement.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                return announcement;
            } finally {
                cursor.close();
            }
        } else {
            return null;
        }
    }

    public ArrayList<Announcement> readAnnouncements(Long studentid) {
        ArrayList<Announcement> announcements = new ArrayList<>();
        String[] columns = {"announcementid", "studentid" ,"announcementName", "postingDate", "description"};
        String selection = "studentid=?";
        String[] selectionArgs = {studentid.toString()};
        Cursor cursor = mDatabase.query("announcements", columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Announcement announcement = new Announcement();
                announcement.setAnnouncementid(cursor.getLong(cursor.getColumnIndex("announcementid")));
                announcement.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                announcement.setAnnouncementName(cursor.getString(cursor.getColumnIndex("announcementName")));
                Long dateMillis = cursor.getLong(cursor.getColumnIndex("postingDate"));
                announcement.setPostingDate(new Date(dateMillis));
                announcement.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                announcements.add(announcement);
            } while (cursor.moveToNext());
        }

        return announcements;
    }

    public void insertEvents(ArrayList<Event> events, boolean clearPrevious) {
        if (clearPrevious) {
            mDatabase.delete("events", null, null);
        }

        String sql = "INSERT INTO events VALUES (?,?,?,?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < events.size(); i++) {
            Event event = events.get(i);
            statement.clearBindings();
            statement.bindLong(1, event.getEventid());
            statement.bindLong(2, event.getStudentid());
            statement.bindString(3, event.getEventName());
            statement.bindString(4, event.getLocation() == null ? "" : event.getLocation());
            statement.bindLong(5, event.getEventDate().getTime());
            if(event.getEndDate() == null){
                statement.bindLong(6, 0L);
            } else {
                statement.bindLong(6, event.getEndDate().getTime());
            }

            statement.bindString(7, event.getDescription() == null ? "" : event.getDescription());
            statement.execute();
        }
        //set the transaction as successful and end the transaction
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }

    public Event readEvent(String eventid) {
        String sql = "SELECT * from events where eventid = " + eventid + ";";
        Cursor cursor = mDatabase.rawQuery(sql, null);
        Event event = new Event();

        if (cursor != null && cursor.moveToFirst()) {
            try {
                event.setEventid(cursor.getLong(cursor.getColumnIndex("eventid")));
                event.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                event.setEventName(cursor.getString(cursor.getColumnIndex("eventName")));
                event.setLocation(cursor.getString(cursor.getColumnIndex("location")));
                Long dateMillis = cursor.getLong(cursor.getColumnIndex("eventDate"));
                event.setEventDate(new Date(dateMillis));
                Long endDateMillis = cursor.getLong(cursor.getColumnIndex("endDate"));
                event.setEndDate(new Date(endDateMillis));
                event.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                return event;
            } finally {
                cursor.close();
            }
        } else {
            return null;
        }
    }

    public ArrayList<Event> readEventsbyDate(String dateInMillis){
        ArrayList<Event> events = new ArrayList<>();
        String[] columns = {"eventid", "studentid" ,"eventName", "location","eventDate", "endDate","description"};
        String selection = "eventDate=?";
        String[] selectionArgs = {dateInMillis};
        Cursor cursor = mDatabase.query("events", columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Event event = new Event();
                event.setEventid(cursor.getLong(cursor.getColumnIndex("eventid")));
                event.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                event.setEventName(cursor.getString(cursor.getColumnIndex("eventName")));
                event.setLocation(cursor.getString(cursor.getColumnIndex("location")));
                Long dateMillis = cursor.getLong(cursor.getColumnIndex("eventDate"));
                event.setEventDate(new Date(dateMillis));
                Long endDateMillis = cursor.getLong(cursor.getColumnIndex("endDate"));
                event.setEndDate(new Date(endDateMillis));
                event.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                events.add(event);
            } while (cursor.moveToNext());
        }

        return events;
    }

    public ArrayList<Event> readEvents(Long studentid) {
        ArrayList<Event> events = new ArrayList<>();
        String[] columns = {"eventid", "studentid" ,"eventName", "location","eventDate", "endDate","description"};
        String selection = "studentid=?";
        String[] selectionArgs = {studentid.toString()};
        Cursor cursor = mDatabase.query("events", columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Event event = new Event();
                event.setEventid(cursor.getLong(cursor.getColumnIndex("eventid")));
                event.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                event.setEventName(cursor.getString(cursor.getColumnIndex("eventName")));
                event.setLocation(cursor.getString(cursor.getColumnIndex("location")));
                Long dateMillis = cursor.getLong(cursor.getColumnIndex("eventDate"));
                event.setEventDate(new Date(dateMillis));
                Long endDateMillis = cursor.getLong(cursor.getColumnIndex("endDate"));
                event.setEndDate(new Date(endDateMillis));
                event.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                events.add(event);
            } while (cursor.moveToNext());
        }

        return events;
    }

    public void insertExamResultsOverview(ArrayList<ExamResultsOverview> examResultsOverviewArrayList, boolean clearPrevious){
        if (clearPrevious) {
            mDatabase.delete("exam_results_overview", null, null);
        }
        String sql = "INSERT INTO exam_results_overview VALUES (?,?,?,?,?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for(ExamResultsOverview examResultsOverview : examResultsOverviewArrayList){
            statement.clearBindings();
            statement.bindLong(1,examResultsOverview.getStudentid());
            statement.bindLong(2, examResultsOverview.getExamid());
            statement.bindString(3, examResultsOverview.getExamName());
            statement.bindLong(4, examResultsOverview.getExamDate().getTime());
            statement.bindString(5, examResultsOverview.getTotal() == null ? "" : examResultsOverview.getTotal());
            statement.bindString(6, examResultsOverview.getAverage() == null ? "" : examResultsOverview.getAverage());
            statement.bindString(7, examResultsOverview.getGrade() == null ? "" : examResultsOverview.getGrade());
            statement.bindString(8, examResultsOverview.getPosition() == null ? "" : examResultsOverview.getPosition());
            statement.execute();
        }
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }


    public ArrayList<ExamResultsOverview> readExamResultsOverview(String studentid) {
        ArrayList<ExamResultsOverview> examResultsOverviewArrayList = new ArrayList<>();
        String[] columns = {"studentid", "examid", "examName", "examDate", "total", "average", "grade", "position"};
        String selection = "studentid=?";
        String[] selectionArgs = {studentid};
        Cursor cursor = mDatabase.query("exam_results_overview", columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                ExamResultsOverview results = new ExamResultsOverview();
                results.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                results.setExamid(cursor.getLong(cursor.getColumnIndex("examid")));
                results.setExamName(cursor.getString(cursor.getColumnIndex("examName")));
                Long dateMillis = cursor.getLong(cursor.getColumnIndex("examDate"));
                results.setTotal(cursor.getString(cursor.getColumnIndex("total")));
                results.setAverage(cursor.getString(cursor.getColumnIndex("average")));
                results.setGrade(cursor.getString(cursor.getColumnIndex("grade")));
                results.setPosition(cursor.getString(cursor.getColumnIndex("position")));
                results.setExamDate(new Date(dateMillis));
                examResultsOverviewArrayList.add(results);
            } while (cursor.moveToNext());
        }
        return examResultsOverviewArrayList;
    }

    public ExamResultsOverview readSingleExamResultsOverview(String studentid, String examid){
        String sql = "SELECT * from exam_results_overview where studentid = " + studentid + " and examid = " + examid + ";";
        Cursor cursor = mDatabase.rawQuery(sql, null);
        ExamResultsOverview examResultsOverview = new ExamResultsOverview();

        if (cursor != null && cursor.moveToFirst()) {
            try {
                examResultsOverview.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                examResultsOverview.setExamid(cursor.getLong(cursor.getColumnIndex("examid")));
                examResultsOverview.setExamName(cursor.getString(cursor.getColumnIndex("examName")));
                Long dateMillis = cursor.getLong(cursor.getColumnIndex("examDate"));
                examResultsOverview.setExamDate(new Date(dateMillis));
                examResultsOverview.setTotal(cursor.getString(cursor.getColumnIndex("total")));
                examResultsOverview.setAverage(cursor.getString(cursor.getColumnIndex("average")));
                examResultsOverview.setGrade(cursor.getString(cursor.getColumnIndex("grade")));
                examResultsOverview.setPosition(cursor.getString(cursor.getColumnIndex("position")));
                return examResultsOverview;
            } finally {
                cursor.close();
            }
        } else {
            return null;
        }
    }

    public void insertExamMarks(ArrayList<ExamMarks> examMarksArrayList, String studentid, String examid){
        ArrayList<ExamMarks> savedExamMarksList = readExamMarks(studentid, examid);
        if(savedExamMarksList != null && !savedExamMarksList.isEmpty()){
            String sql = "DELETE from exam_marks where studentid = " + studentid + " and examid = " + examid + ";";
            mDatabase.execSQL(sql);
        }

        String sql = "INSERT INTO exam_marks VALUES (?,?,?,?,?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for(ExamMarks examMarks : examMarksArrayList){
            statement.clearBindings();
            statement.bindLong(1,examMarks.getStudentid());
            statement.bindLong(2, examMarks.getExamid());
            statement.bindLong(3, examMarks.getSubjectid());
            statement.bindString(4, examMarks.getSubjectName());
            statement.bindString(5, examMarks.getShortName());
            statement.bindString(6, examMarks.getMarks());
            statement.bindString(7, examMarks.getGrade() == null ? "" : examMarks.getGrade());
            statement.bindString(8, examMarks.getPosition() == null ? "" : examMarks.getPosition());
            statement.execute();
        }
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }


    public ArrayList<ExamMarks> readExamMarks(String studentid, String examid) {
        ArrayList<ExamMarks> examMarksArrayList = new ArrayList<>();
        String[] columns = {"studentid", "examid", "subjectid", "subjectName", "shortName", "marks", "grade", "position"};
        String selection = "studentid=? and examid=?";
        String[] selectionArgs = {studentid, examid};
        Cursor cursor = mDatabase.query("exam_marks", columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                ExamMarks examMarks = new ExamMarks();
                examMarks.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                examMarks.setExamid(cursor.getLong(cursor.getColumnIndex("examid")));
                examMarks.setSubjectid(cursor.getLong(cursor.getColumnIndex("subjectid")));
                examMarks.setSubjectName(cursor.getString(cursor.getColumnIndex("subjectName")));
                examMarks.setShortName(cursor.getString(cursor.getColumnIndex("shortName")));
                examMarks.setMarks(cursor.getString(cursor.getColumnIndex("marks")));
                examMarks.setGrade(cursor.getString(cursor.getColumnIndex("grade")));
                examMarks.setPosition(cursor.getString(cursor.getColumnIndex("position")));
                examMarksArrayList.add(examMarks);
            } while (cursor.moveToNext());
        }
        return examMarksArrayList;
    }

    public void insertAnalysisMetric(ArrayList<AnalysisMetric> analysisMetricArrayList, String studentid, String examid){
        ArrayList<AnalysisMetric> savedAnalysisMetricsList = readAnalysisMetrics(studentid, examid);
        if(savedAnalysisMetricsList != null && !savedAnalysisMetricsList.isEmpty()){
            String sql = "DELETE from analysis_metrics where studentid = " + studentid + " and examid = " + examid + ";";
            mDatabase.execSQL(sql);
        }

        String sql = "INSERT INTO analysis_metrics VALUES (?,?,?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for(AnalysisMetric analysisMetric : analysisMetricArrayList){
            statement.clearBindings();
            statement.bindLong(1,analysisMetric.getStudentid());
            statement.bindLong(2, analysisMetric.getExamid());
            statement.bindLong(3, analysisMetric.getAnalysismetricid());
            statement.bindString(4, analysisMetric.getAnalysisMetricName());
            statement.bindString(5, analysisMetric.getShortName());
            statement.bindString(6, analysisMetric.getEntry());
            statement.execute();
        }
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }


    public ArrayList<AnalysisMetric> readAnalysisMetrics(String studentid, String examid) {
        ArrayList<AnalysisMetric> analysisMetricArrayList = new ArrayList<>();
        String[] columns = {"studentid", "examid", "analysismetricid", "analysisMetricName", "shortName", "entry"};
        String selection = "studentid=? and examid=?";
        String[] selectionArgs = {studentid, examid};
        Cursor cursor = mDatabase.query("analysis_metrics", columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                AnalysisMetric analysisMetric = new AnalysisMetric();
                analysisMetric.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                analysisMetric.setExamid(cursor.getLong(cursor.getColumnIndex("examid")));
                analysisMetric.setAnalysismetricid(cursor.getLong(cursor.getColumnIndex("analysismetricid")));
                analysisMetric.setAnalysisMetricName(cursor.getString(cursor.getColumnIndex("analysisMetricName")));
                analysisMetric.setShortName(cursor.getString(cursor.getColumnIndex("shortName")));
                analysisMetric.setEntry(cursor.getString(cursor.getColumnIndex("entry")));
                analysisMetricArrayList.add(analysisMetric);
            } while (cursor.moveToNext());
        }
        return analysisMetricArrayList;
    }

    public void insertFeeBalance(FeeBalance feeBalance, String studentid) {
        FeeBalance savedFeeBalance = readFeeBalance(Long.parseLong(studentid));
        if (savedFeeBalance != null){
            ContentValues data = new ContentValues();
            data.put("feeBalance",feeBalance.getBalance() == null ? "" : feeBalance.getBalance());
            data.put("feePaid",feeBalance.getPaid() == null ? "" :feeBalance.getPaid());
            data.put("paymentInfo", feeBalance.getPaymentInfo() == null ? "" :feeBalance.getPaymentInfo());
            mDatabase.update("fee_balance", data, "studentid="+studentid, null);

        } else {
            String sql = "INSERT INTO fee_balance VALUES (?,?,?,?);";
            SQLiteStatement statement = mDatabase.compileStatement(sql);
            mDatabase.beginTransaction();
            statement.bindLong(1, Long.valueOf(studentid));
            statement.bindString(2, feeBalance.getBalance() == null ? "" : feeBalance.getBalance());
            statement.bindString(3, feeBalance.getPaid() == null ? "" : feeBalance.getPaid());
            statement.bindString(4, feeBalance.getPaymentInfo() == null ? "" :feeBalance.getPaymentInfo());
            statement.execute();
            mDatabase.setTransactionSuccessful();
            mDatabase.endTransaction();
        }

    }

    public FeeBalance readFeeBalance(Long studentid) {
        String sql = "SELECT * from fee_balance where studentid = " + studentid.toString() + ";";
        Cursor cursor = mDatabase.rawQuery(sql, null);
        FeeBalance feeBalance = new FeeBalance();

        if (cursor != null && cursor.moveToFirst()) {
            try {
                feeBalance.setBalance(cursor.getString(cursor.getColumnIndex("feeBalance")));
                feeBalance.setPaid(cursor.getString(cursor.getColumnIndex("feePaid")));
                feeBalance.setPaymentInfo(cursor.getString(cursor.getColumnIndex("paymentInfo")));
                return feeBalance;
            } finally {
                cursor.close();
            }
        } else {
            return null;
        }

    }

    public void insertFeeStructure(ArrayList<FeeStructure> feeStructureArrayList, String studentid){
        ArrayList<FeeStructure> savedFeeStructureList = readFeeStructure(studentid);
        if(savedFeeStructureList != null && !savedFeeStructureList.isEmpty()){
            String sql = "DELETE from fee_structure where studentid = " + studentid + ";";
            mDatabase.execSQL(sql);
        }

        String sql = "INSERT INTO fee_structure VALUES (?,?,?,?,?);";
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for(FeeStructure feeStructure : feeStructureArrayList){
            statement.clearBindings();
            statement.bindLong(1,feeStructure.getStudentid());
            statement.bindLong(2, feeStructure.getFeeitemid());
            statement.bindString(3, feeStructure.getFeeItemName());
            statement.bindString(4, feeStructure.getShortName());
            statement.bindString(5, feeStructure.getAmount());
            statement.execute();
        }
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }


    public ArrayList<FeeStructure> readFeeStructure(String studentid) {
        ArrayList<FeeStructure> feeStructureArrayList = new ArrayList<>();
        String[] columns = {"studentid", "feeitemid", "feeItemName", "shortName", "amount"};
        String selection = "studentid=?";
        String[] selectionArgs = {studentid};
        Cursor cursor = mDatabase.query("fee_structure", columns, selection, selectionArgs, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                FeeStructure feeStructure = new FeeStructure();
                feeStructure.setStudentid(cursor.getLong(cursor.getColumnIndex("studentid")));
                feeStructure.setFeeitemid(cursor.getLong(cursor.getColumnIndex("feeitemid")));
                feeStructure.setFeeItemName(cursor.getString(cursor.getColumnIndex("feeItemName")));
                feeStructure.setShortName(cursor.getString(cursor.getColumnIndex("shortName")));
                feeStructure.setAmount(cursor.getString(cursor.getColumnIndex("amount")));
                feeStructureArrayList.add(feeStructure);
            } while (cursor.moveToNext());
        }
        return feeStructureArrayList;
    }



    private static class DatabaseHelper extends SQLiteOpenHelper {

        private static final String DB_NAME = "shuledial_db";
        private static final int DB_VERSION = 1;
        private Context mContext;

        private static final String CREATE_TABLE_STUDENTS = "CREATE TABLE students (studentid INTEGER PRIMARY KEY, admission TEXT, studentName TEXT,schoolName TEXT);";
        private static final String CREATE_TABLE_ANNOUNCEMENTS = "CREATE TABLE announcements (announcementid INTEGER PRIMARY KEY, studentid INTEGER, announcementName TEXT, postingDate INTEGER, description TEXT);";
        private static final String CREATE_TABLE_EVENTS = "CREATE TABLE events (eventid INTEGER PRIMARY KEY, studentid INTEGER, eventName TEXT, location TEXT, eventDate INTEGER,endDate INTEGER, description TEXT);";
        private static final String CREATE_TABLE_RESULTS_OVERVIEW = "CREATE TABLE exam_results_overview (studentid INTEGER, examid INTEGER, examName TEXT, examDate INTEGER, total TEXT, average TEXT, grade TEXT, position TEXT);";
        private static final String CREATE_TABLE_EXAM_MARKS = "CREATE TABLE exam_marks (studentid INTEGER, examid INTEGER, subjectid INTEGER, subjectName TEXT, shortName TEXT, marks TEXT, grade TEXT, position TEXT);";
        private static final String CREATE_TABLE_ANALYSIS_METRICS = "CREATE TABLE analysis_metrics (studentid INTEGER, examid INTEGER, analysismetricid INTEGER, analysisMetricName TEXT, shortName TEXT, entry TEXT)";
        private static final String CREATE_TABLE_FEE_BALANCE = "CREATE TABLE fee_balance (studentid INTEGER PRIMARY KEY, feeBalance TEXT, feePaid TEXT, paymentInfo TEXT);";
        private static final String CREATE_TABLE_FEE_STRUCTURE = "CREATE TABLE fee_structure (studentid INTEGER, feeitemid INTEGER, feeItemName TEXT, shortName TEXT, amount TEXT)";

        public DatabaseHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            mContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE_STUDENTS);
                db.execSQL(CREATE_TABLE_ANNOUNCEMENTS);
                db.execSQL(CREATE_TABLE_EVENTS);
                db.execSQL(CREATE_TABLE_RESULTS_OVERVIEW);
                db.execSQL(CREATE_TABLE_EXAM_MARKS);
                db.execSQL(CREATE_TABLE_ANALYSIS_METRICS);
                db.execSQL(CREATE_TABLE_FEE_BALANCE);
                db.execSQL(CREATE_TABLE_FEE_STRUCTURE);
            } catch (SQLiteException exception) {
                L.t(mContext, exception + "");
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                L.m("upgrade table box office executed");
                db.execSQL(" DROP TABLE students IF EXISTS;");
                db.execSQL(" DROP TABLE announcements IF EXISTS;");
                db.execSQL(" DROP TABLE events IF EXISTS;");
                db.execSQL(" DROP TABLE exam_results_overview IF EXISTS ");
                db.execSQL("DROP TABLE exam_marks IF EXISTS");
                db.execSQL("DROP TABLE analysis_metrics IF EXISTS");
                db.execSQL("DROP TABLE fee_balance IF EXISTS");
                db.execSQL("DROP TABLE fee_structure IF EXISTS");
                onCreate(db);
            } catch (SQLiteException exception) {
                L.t(mContext, exception + "");
            }
        }
    }
}