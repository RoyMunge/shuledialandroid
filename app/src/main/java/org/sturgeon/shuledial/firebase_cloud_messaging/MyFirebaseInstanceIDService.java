package org.sturgeon.shuledial.firebase_cloud_messaging;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.sturgeon.shuledial.tasks.TaskUpdateFCMToken;
import org.sturgeon.shuledial.utils.SessionManager;

/**
 * Created by Roy on 5/16/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //calling the method store token and passing token
        updateToken(refreshedToken);

    }

    private void updateToken(String refreshedToken){
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        sessionManager.saveFCMToken(refreshedToken);
    }

}
