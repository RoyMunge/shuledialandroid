package org.sturgeon.shuledial.firebase_cloud_messaging;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;

import static android.content.ContentValues.TAG;

/**
 * Created by Roy on 5/16/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private Context mCtx;

    /*@Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.i(TAG, "Notification recieved............");
        if (remoteMessage.getData().size() > 0) {
            Log.i(TAG, remoteMessage.getData().toString());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());

                try {
                    //getting the json data
                    JSONObject data = json.getJSONObject("data");

                    //parsing json data
                    String title = data.getString("title");
                    Log.i(TAG, title);
                    String message = data.getString("body");
                    Log.i(TAG, message);
                    //creating an intent for the notification
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    showNotification(title, message, intent);

                } catch (JSONException e) {
                } catch (Exception e) {
                }
            } catch (Exception e) {
            }
        }
    }*/

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
        String title = intent.getExtras().get("title").toString();
        Log.i(TAG, title);
        String message = intent.getExtras().get("body").toString();
        Log.i(TAG, message);
        Intent toopen = new Intent(getApplicationContext(), MainActivity.class);
        showNotification(title, message, toopen);

    }

    public void showNotification(String title, String message, Intent intent) {
        mCtx = getApplicationContext();
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        235,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mCtx.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(235, notification);
    }
}
