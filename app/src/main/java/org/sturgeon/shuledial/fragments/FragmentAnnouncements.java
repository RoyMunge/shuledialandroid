package org.sturgeon.shuledial.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.AnnouncementPage;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.AnnouncementsLoadedListener;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.Announcement;
import org.sturgeon.shuledial.recycler_adapters.AdapterAnnouncements;
import org.sturgeon.shuledial.tasks.TaskLoadAnnouncements;
import org.sturgeon.shuledial.utils.RecyclerViewListener.ClickListener;
import org.sturgeon.shuledial.utils.RecyclerViewListener.RecyclerTouchListener;
import org.sturgeon.shuledial.utils.SessionManager;

import java.util.ArrayList;


public class FragmentAnnouncements extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AnnouncementsLoadedListener{
    private static final String STATE_ANNOUNCEMENTS = "state_announcements";
    private ArrayList<Announcement> announcements = new ArrayList<>();
    private AdapterAnnouncements adapterAnnouncements;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerAnnouncements;
    private SessionManager sessionManager;
    private String currentStudentId;

    public FragmentAnnouncements() {
    }


    public static FragmentAnnouncements newInstance() {
        FragmentAnnouncements fragment = new FragmentAnnouncements();
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_announcements, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeAnnouncements);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerAnnouncements = (RecyclerView) layout.findViewById(R.id.listAnnouncements);
        recyclerAnnouncements.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterAnnouncements = new AdapterAnnouncements(getActivity());
        recyclerAnnouncements.setAdapter(adapterAnnouncements);
        sessionManager = new SessionManager(getActivity());
        currentStudentId = sessionManager.GetCurrentStudent();
        if(savedInstanceState != null){
            announcements = savedInstanceState.getParcelableArrayList(STATE_ANNOUNCEMENTS);
        } else {
            announcements = MyApplication.getWritableDatabase().readAnnouncements(Long.parseLong(currentStudentId));
            if(announcements.isEmpty()){
                new TaskLoadAnnouncements(this, currentStudentId).execute();
            }
        }

        if(!announcements.isEmpty()){
            adapterAnnouncements.setAnnouncements(announcements);
        }
        recyclerAnnouncements.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerAnnouncements, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(getActivity(), AnnouncementPage.class);
                intent.putExtra("announcementid", announcements.get(position).getAnnouncementid().toString());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
            outState.putParcelableArrayList(STATE_ANNOUNCEMENTS, announcements);
    }

    @Override
    public void onRefresh() {
        L.t(getActivity(), "Refreshing Announcements");
        //load the whole feed again on refresh, dont try this at home :)
        final MainActivity mainActivity = (MainActivity) getActivity();
        if(mainActivity.isOnline()) {
            new TaskLoadAnnouncements(this, currentStudentId).execute();
        }
    }

    @Override
    public void onAnnouncementsLoaded(ArrayList<Announcement> announcements) {
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
        if(!announcements.isEmpty())
        {
            this.announcements = announcements;
            adapterAnnouncements.setAnnouncements(announcements);
        }
    }
}
