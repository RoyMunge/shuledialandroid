package org.sturgeon.shuledial.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.EventPage;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.CalendarEventsLoadedListener;
import org.sturgeon.shuledial.callbacks.EventsLoadedListener;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.recycler_adapters.AdapterEvents;
import org.sturgeon.shuledial.tasks.CalendarEventsTask;
import org.sturgeon.shuledial.tasks.TaskLoadEvents;
import org.sturgeon.shuledial.utils.CalendarDecorators.EventDecorator;
import org.sturgeon.shuledial.utils.CalendarDecorators.OneDayDecorator;
import org.sturgeon.shuledial.utils.RecyclerViewListener.ClickListener;
import org.sturgeon.shuledial.utils.RecyclerViewListener.RecyclerTouchListener;
import org.sturgeon.shuledial.utils.SchoolPeriod;
import org.sturgeon.shuledial.utils.SessionManager;
import org.sturgeon.shuledial.utils.Sorter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.Executors;


public class FragmentEvents extends Fragment implements SwipeRefreshLayout.OnRefreshListener, EventsLoadedListener,OnDateSelectedListener, OnMonthChangedListener, CalendarEventsLoadedListener {
    private static final String STATE_EVENTS = "state_events";
    private ArrayList<Event> events = new ArrayList<>();
    private ArrayList<Event> eventsInAdapter = new ArrayList<>();
    private AdapterEvents adapterEvents;
    private Sorter sorter = new Sorter();
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerEvents;
    private SessionManager sessionManager;
    private String currentStudentId;

    private MaterialCalendarView widget;
    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    private TextView schoolPeriodTextView;
    private TextView eventsHeader;

    private DateFormat dateMonthFullFormat = new SimpleDateFormat("MMMM dd");
    private DateFormat getDateMonthYearFormat = new SimpleDateFormat("dd/MM/yyyy");
    private DateFormat monthFormat = new SimpleDateFormat("MM");
    private DateFormat monthFullFormat = new SimpleDateFormat("MMMM");


    public FragmentEvents() {
    }

    public static FragmentEvents newInstance() {
        FragmentEvents fragment = new FragmentEvents();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_events, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeEvents);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerEvents = (RecyclerView) layout.findViewById(R.id.listEvents);
        recyclerEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterEvents = new AdapterEvents(getActivity());
        recyclerEvents.setAdapter(adapterEvents);
        sessionManager = new SessionManager(getActivity());
        currentStudentId = sessionManager.GetCurrentStudent();


        if(savedInstanceState != null){
            events = savedInstanceState.getParcelableArrayList(STATE_EVENTS);
        } else {
            events = MyApplication.getWritableDatabase().readEvents(Long.parseLong(currentStudentId));
            if(events.isEmpty()){
                new TaskLoadEvents(this, currentStudentId).execute();
            }
        }
        if(!events.isEmpty()){
            sorter.sortEventsByDate(events);
            setupCalendarView(layout);
            //adapterEvents.setAllEvents(events);
        }

        recyclerEvents.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerEvents, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                    Intent intent = new Intent(getActivity(), EventPage.class);
                    intent.putExtra("eventid", eventsInAdapter.get(position).getEventid().toString());
                    startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }

    private void setupCalendarView(View layout){
        widget = (MaterialCalendarView) layout.findViewById(R.id.calendarView);
        widget.addDecorator(oneDayDecorator);
        widget.setOnDateChangedListener(this);
        widget.setOnMonthChangedListener(this);
        schoolPeriodTextView = (TextView) layout.findViewById(R.id.school_period);
        SchoolPeriod schoolPeriod = new SchoolPeriod(widget.getCurrentDate().getDate());
        eventsHeader = (TextView) layout.findViewById(R.id.events_header);
        ArrayList<Event> eventsForMonth = getEventsforMonth(events, monthFormat.format(widget.getCurrentDate().getDate()));
        if (eventsForMonth.isEmpty()) {
            eventsHeader.setText("NO EVENTS IN " + monthFullFormat.format(widget.getCurrentDate().getDate()));
        } else {
            eventsInAdapter = eventsForMonth;
            recyclerEvents.getRecycledViewPool().clear();
            adapterEvents.setAdapterEvents(eventsForMonth);
            eventsHeader.setText("EVENTS IN " + monthFullFormat.format(widget.getCurrentDate().getDate()));
        }
        schoolPeriodTextView.setText("Term " + Integer.toString(schoolPeriod.getTerm()) + " " + Integer.toString(schoolPeriod.getYear()));
        new CalendarEventsTask(events, this).executeOnExecutor(Executors.newSingleThreadExecutor());
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        final ArrayList<Event> eventsForDate = getEventsforDate(events, getDateMonthYearFormat.format(date.getDate()));
        if (eventsForDate.isEmpty()) {
            Toast.makeText(widget.getContext(), "No Events on this date", Toast.LENGTH_SHORT).show();
            onMonthChanged(widget, date);
        } else {
            eventsInAdapter = eventsForDate;
            eventsHeader.setText("EVENTS ON " + dateMonthFullFormat.format(date.getDate()));
            recyclerEvents.getRecycledViewPool().clear();
            adapterEvents.setAdapterEvents(eventsForDate);
        }
    }

    @Override
    public void onMonthChanged(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date) {
        SchoolPeriod schoolPeriod = new SchoolPeriod(date.getDate());
        schoolPeriodTextView.setText("Term " + Integer.toString(schoolPeriod.getTerm()) + " " + Integer.toString(schoolPeriod.getYear()));
        final ArrayList<Event> eventsForMonth = getEventsforMonth(events, monthFormat.format(date.getDate()));
        if (eventsForMonth.isEmpty()) {
            Toast.makeText(widget.getContext(), "No events in " + monthFullFormat.format(date.getDate()), Toast.LENGTH_SHORT).show();
        } else {
            eventsHeader.setText("EVENTS IN " + monthFullFormat.format(date.getDate()));
            recyclerEvents.getRecycledViewPool().clear();
            adapterEvents.setAdapterEvents(eventsForMonth);
        }
    }

    @Override
    public void onCalendarEventsLoaded(ArrayList<CalendarDay> calendarDays) {
        widget.addDecorator(new EventDecorator(calendarDays));
    }

    public ArrayList<Event> getEventsforMonth(ArrayList<Event> events, String month) {
        final ArrayList<Event> eventsForMonth = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            if (monthFormat.format(events.get(i).getEventDate()).equals(month)) {
                eventsForMonth.add(events.get(i));
            }
        }
        return eventsForMonth;
    }

    public ArrayList<Event> getEventsforDate(ArrayList<Event> events, String date) {
        ArrayList<Event> eventsForDate = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            if (getDateMonthYearFormat.format(events.get(i).getEventDate()).equals(date)) {
                eventsForDate.add(events.get(i));
            }
        }
        return eventsForDate;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_EVENTS,events);
    }

    @Override
    public void onRefresh() {
        L.t(getActivity(), "Refreshing Events");
        //load the whole feed again on refresh, dont try this at home :)
        final MainActivity mainActivity = (MainActivity) getActivity();
        if(mainActivity.isOnline()) {
            new TaskLoadEvents(this, currentStudentId).execute();
        }
    }

    @Override
    public void onEventsLoaded(ArrayList<Event> events) {
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
        if(!events.isEmpty()) {
            this.events = events;
            adapterEvents.setAllEvents(events);
        }
    }
}

