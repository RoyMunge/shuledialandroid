package org.sturgeon.shuledial.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.activities.Payment;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.FeeBalanceLoadedListener;
import org.sturgeon.shuledial.callbacks.FeeDropdownsLoadedListener;
import org.sturgeon.shuledial.callbacks.FeeItemLoadedListener;
import org.sturgeon.shuledial.callbacks.FeeStructureLoadedListener;
import org.sturgeon.shuledial.callbacks.SchoolPeriodTotalLoadedListener;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.FeeBalance;
import org.sturgeon.shuledial.pojo.FeeDropdowns;
import org.sturgeon.shuledial.pojo.FeeItem;
import org.sturgeon.shuledial.pojo.FeeStructure;
import org.sturgeon.shuledial.pojo.SchoolLevel;
import org.sturgeon.shuledial.pojo.SchoolPeriod;
import org.sturgeon.shuledial.pojo.SchoolPeriodTotal;
import org.sturgeon.shuledial.recycler_adapters.AdapterFeeItems;
import org.sturgeon.shuledial.tasks.TaskLoadAnnouncements;
import org.sturgeon.shuledial.tasks.TaskLoadFeeBalance;
import org.sturgeon.shuledial.tasks.TaskLoadFeeDropdowns;
import org.sturgeon.shuledial.tasks.TaskLoadFeeItems;
import org.sturgeon.shuledial.tasks.TaskLoadFeeStructure;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolPeriodTotal;
import org.sturgeon.shuledial.utils.ArcProgress.ArcProgressUtils;
import org.sturgeon.shuledial.utils.Loader;
import org.sturgeon.shuledial.utils.SessionManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 23/11/2016.
 */
public class FragmentFees extends Fragment implements SwipeRefreshLayout.OnRefreshListener, FeeBalanceLoadedListener, FeeStructureLoadedListener {
    private static final String STATE_FEE_BALANCE = "state_fee_balance";
    private static final String STATE_FEE_STRUCTURE = "state_fee_structure";
    private FeeBalance feeBalance;
    private ArrayList<FeeStructure> feeStructureArrayList = new ArrayList<>();
    private SessionManager sessionManager;
    private String currentStudentId;
    private TextView feesPaid;
    private TextView feesBalance;
    private TextView feesDate;
    private TextView feePaymentInfo;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TableLayout tableFeeStructure;


    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public FragmentFees() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_fees, container, false);
        sessionManager = new SessionManager(getActivity());
        currentStudentId = sessionManager.GetCurrentStudent();

        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeFees);
        swipeRefreshLayout.setOnRefreshListener(this);
        LinearLayout noPremium = (LinearLayout) layout.findViewById(R.id.layoutPremium);

        String validUntil = sessionManager.GetValidUntil();
        if (!validUntil.equals("")) {
            Date date = StringtoDate(validUntil);

            if (date != null) {
                if (date.compareTo(new Date()) < 0) {
                    swipeRefreshLayout.setVisibility(View.GONE);

                    Button btnPremium = (Button) layout.findViewById(R.id.btnPremium);
                    noPremium.setVisibility(View.VISIBLE);

                    btnPremium.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getActivity(), Payment.class));
                        }
                    });
                } else {
                    noPremium.setVisibility(View.GONE);
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                }
            } else {
                noPremium.setVisibility(View.GONE);
                swipeRefreshLayout.setVisibility(View.VISIBLE);
            }
        } else {
            noPremium.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);
        }

         tableFeeStructure = (TableLayout) layout.findViewById(R.id.feebreakdowntable);


        feesPaid = (TextView) layout.findViewById(R.id.feesPaid);
        feesBalance = (TextView) layout.findViewById(R.id.feesBalance);
        feesDate = (TextView) layout.findViewById(R.id.feeDate);
        feePaymentInfo = (TextView) layout.findViewById(R.id.feePaymentInfo);

        if (savedInstanceState != null) {
            feeBalance = savedInstanceState.getParcelable(STATE_FEE_BALANCE);
            feeStructureArrayList = savedInstanceState.getParcelableArrayList(STATE_FEE_STRUCTURE);
            LoadFeeBalance();
            LoadFeeStructure();
        } else {
            feeBalance = MyApplication.getWritableDatabase().readFeeBalance(Long.parseLong(currentStudentId));
            feeStructureArrayList = MyApplication.getWritableDatabase().readFeeStructure(currentStudentId);
            if (feeBalance != null) {
                LoadFeeBalance();
            } else {
                new TaskLoadFeeBalance(this, currentStudentId).execute();
            }
            if(feeStructureArrayList.isEmpty()){
                new TaskLoadFeeStructure(this, currentStudentId).execute();
            } else {
                LoadFeeStructure();
            }
        }

        return layout;
    }

    public static FragmentFees newInstance() {
        FragmentFees fragment = new FragmentFees();
        return fragment;
    }

    private void LoadFeeBalance() {
        feesPaid.setText(feeBalance.getPaid());
        feesBalance.setText(feeBalance.getBalance());
        feesDate.setText("is your balance as of  " + dateFormat.format(new Date()));
        feePaymentInfo.setText(feeBalance.getPaymentInfo());
    }

    private void LoadFeeStructure(){
        tableFeeStructure.removeAllViews();
        for (FeeStructure feeStructure : feeStructureArrayList){
            TableRow tableRow = new TableRow(getContext());
            tableRow.setGravity(Gravity.CENTER);
            tableRow.setMinimumHeight((int) ArcProgressUtils.sp2px(getResources(), 30));
            TextView feeItemText = new TextView(getContext());
            feeItemText.setGravity(Gravity.CENTER);
            feeItemText.setText(feeStructure.getFeeItemName());
            TextView amountText = new TextView(getContext());
            amountText.setGravity(Gravity.CENTER);
            amountText.setText(feeStructure.getAmount());
            tableRow.addView(feeItemText);
            tableRow.addView(amountText);

            tableFeeStructure.addView(tableRow);
        }
    }


    @Override
    public void onFeeBalanceLoaded(FeeBalance feeBalance) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if (feeBalance != null) {
            this.feeBalance = feeBalance;
            LoadFeeBalance();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_FEE_BALANCE, feeBalance);
        outState.putParcelableArrayList(STATE_FEE_STRUCTURE, feeStructureArrayList);
    }

    @Override
    public void onFeeStructureLoaded(ArrayList<FeeStructure> feeStructureArrayList) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if(feeStructureArrayList != null && !feeStructureArrayList.isEmpty()){
            this.feeStructureArrayList = feeStructureArrayList;
            LoadFeeStructure();
        }
    }

    private Date StringtoDate(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        try {
            Date parsedDate = formatter.parse(dateInString);
            return parsedDate;
        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    public void onRefresh() {
        L.t(getActivity(), "Refreshing Fee Balance");
        //load the whole feed again on refresh, dont try this at home :)
        final MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity.isOnline()) {
            new TaskLoadFeeBalance(this, currentStudentId).execute();
            new TaskLoadFeeStructure(this, currentStudentId).execute();
        }
    }

}
