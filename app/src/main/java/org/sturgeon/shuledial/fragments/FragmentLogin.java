package org.sturgeon.shuledial.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.activities.RegisterLoginActivity;
import org.sturgeon.shuledial.transport.LoginRequest;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.CustomEditText;
import org.sturgeon.shuledial.utils.SessionManager;


public class FragmentLogin extends Fragment {
    SessionManager sessionManager;

    public FragmentLogin() {
    }

    public static FragmentLogin newInstance() {
        FragmentLogin fragment = new FragmentLogin();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.fragment_login, container, false);

        final CustomEditText etLoginPhoneNumber = (CustomEditText) layout.findViewById(R.id.etLoginPhoneNumber);
        final CustomEditText etLoginPin = (CustomEditText) layout.findViewById(R.id.etLoginPin);
        final CheckBox chkShowPin = (CheckBox) layout.findViewById(R.id.chkShowPin);

        etLoginPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.GONE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.GONE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.GONE);
                    }

                }else {
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.VISIBLE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.VISIBLE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        etLoginPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.GONE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.GONE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.GONE);
                    }
                }else {
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.VISIBLE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.VISIBLE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        chkShowPin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked) {
                    //etLoginPin.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etLoginPin.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    //etLoginPin.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etLoginPin.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        LinearLayout signin = (LinearLayout) layout.findViewById(R.id.tvSignIn);
        assert signin != null;
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RegisterLoginActivity) getActivity()).switchTabs(0);
            }
        });

        final Button btnSignIn = (Button) layout.findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                JSONObject jsonResponse = null;
                                try {
                                    jsonResponse = response;
                                    boolean isValidUser = jsonResponse.getBoolean("isValidUser");

                                    System.out.println(" is valid user  = " + isValidUser);

                                    if (isValidUser) {
                                        sessionManager.createLoginSession(processPhoneNumber(etLoginPhoneNumber.getText().toString()));
                                        Intent intent = new Intent(getActivity(), MainActivity.class);
                                        getActivity().startActivity(intent);
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                                        builder.setTitle("Login Error")
                                                .setMessage("Invalid phone number or PIN.")
                                                .setNegativeButton("Retry", null)
                                                .create()
                                                .show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        };

                        LoginForm loginForm = new LoginForm(processPhoneNumber(etLoginPhoneNumber.getText().toString()),etLoginPin.getText().toString());
                        LoginRequest loginRequest = null;
                        try {
                            loginRequest = new LoginRequest(new JSONObject(new Gson().toJson(loginForm)), responseListener);
                            VolleySingleton volleySingleton = VolleySingleton.getInstance();
                            RequestQueue queue = volleySingleton.getRequestQueue();
                            queue.add(loginRequest);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
        );

        return layout;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    private String processPhoneNumber(String phoneNumber){
        String intlPhoneNumber = new String();
        if (phoneNumber.startsWith("07")){
            intlPhoneNumber = phoneNumber.replace("07","2547");
        }
        return intlPhoneNumber;
    }

    public class LoginForm {
        private String subscriberPhoneNumber;
        private String subscriberPin;

        public LoginForm(String subscriberPhoneNumber, String subscriberPin) {
            this.subscriberPhoneNumber = subscriberPhoneNumber;
            this.subscriberPin = subscriberPin;
        }
    }
}
