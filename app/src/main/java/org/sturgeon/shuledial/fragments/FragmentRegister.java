package org.sturgeon.shuledial.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.activities.RegisterLoginActivity;
import org.sturgeon.shuledial.transport.RegisterRequest;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.CustomVerifyEditText;
import org.sturgeon.shuledial.utils.SessionManager;

public class FragmentRegister extends Fragment {
    SessionManager sessionManager;

    public FragmentRegister() {
        // Required empty public constructor
    }

    public static FragmentRegister newInstance() {
        FragmentRegister fragment = new FragmentRegister();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View layout = inflater.inflate(R.layout.fragment_register, container, false);

        final CustomVerifyEditText etPhoneNumber = (CustomVerifyEditText) layout.findViewById(R.id.etPhoneNumber);
        final CustomVerifyEditText etPin = (CustomVerifyEditText) layout.findViewById(R.id.etPin);
        final CustomVerifyEditText etConfirmPin = (CustomVerifyEditText) layout.findViewById(R.id.etConfirmPin);

        LinearLayout signup = (LinearLayout) layout.findViewById(R.id.tvSignUp);
        assert signup != null;
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RegisterLoginActivity) getActivity()).switchTabs(1);
            }
        });

        etPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etConfirmPin.setVisibility(View.VISIBLE);
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.GONE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.GONE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.GONE);
                    }
                }else {
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.VISIBLE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.VISIBLE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        etPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etConfirmPin.setVisibility(View.VISIBLE);
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.GONE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.GONE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.GONE);
                    }
                }else {
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.VISIBLE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.VISIBLE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        etConfirmPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.GONE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.GONE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.VISIBLE);
                    }
                }else {
                    ((RegisterLoginActivity)getActivity()).welcomeText.setVisibility(View.VISIBLE);
                    ((RegisterLoginActivity)getActivity()).introText.setVisibility(View.VISIBLE);
                    if (((RegisterLoginActivity)getActivity()).introImage != null) {
                        ((RegisterLoginActivity) getActivity()).introImage.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        Button btnRegister = (Button) layout.findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {

                                               FormEditText[] allFields = {etPhoneNumber, etPin, etConfirmPin};
                                               boolean allValid = true;
                                               for (FormEditText field : allFields) {
                                                   allValid = field.testValidity() && allValid;
                                               }
                                               if (allValid) {

                                                   if (etPin.getText().toString().equals(etConfirmPin.getText().toString())) {

                                                       Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                                                           @Override
                                                           public void onResponse(JSONObject response) {

                                                               JSONObject jsonResponse = null;
                                                               try {
                                                                   jsonResponse = response;
                                                                   boolean isRegistered = jsonResponse.getBoolean("isRegistered");

                                                                   if (isRegistered) {
                                                                       sessionManager.createLoginSession(processPhoneNumber(etPhoneNumber.getText().toString()));
                                                                       Intent intent = new Intent(layout.getContext(), MainActivity.class);
                                                                       layout.getContext().startActivity(intent);
                                                                   } else {
                                                                       AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                                                                       builder.setTitle("Registration Failed")
                                                                               .setMessage("System error")
                                                                               .setNegativeButton("Retry", null)
                                                                               .create()
                                                                               .show();
                                                                   }
                                                               } catch (JSONException e) {
                                                                   e.printStackTrace();
                                                               }

                                                           }
                                                       };

                                                       RegistrationForm registrationForm = new RegistrationForm("", "", processPhoneNumber(etPhoneNumber.getText().toString()), "", etPin.getText().toString());
                                                       RegisterRequest registerRequest = null;
                                                       try {
                                                           registerRequest = new RegisterRequest(new JSONObject(new Gson().toJson(registrationForm)), responseListener);
                                                           VolleySingleton volleySingleton = VolleySingleton.getInstance();
                                                           RequestQueue queue = volleySingleton.getRequestQueue();
                                                           queue.add(registerRequest);
                                                       } catch (JSONException e) {
                                                           e.printStackTrace();
                                                       }

                                                   } else {
                                                       AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                                                       builder.setTitle("PIN Error")
                                                               .setMessage("PIN and confirmation PIN should match")
                                                               .setNegativeButton("Retry", null)
                                                               .create()
                                                               .show();
                                                   }

                                               } else {
                                                   AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                                                   builder.setTitle("Registration Error")
                                                           .setMessage("Please confirm input")
                                                           .setNegativeButton("Retry", null)
                                                           .create()
                                                           .show();
                                               }

                                           }
                                       }
        );

        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private String processPhoneNumber(String phoneNumber){
        String intlPhoneNumber = new String();
        if (phoneNumber.startsWith("07")){
            intlPhoneNumber = phoneNumber.replace("07","2547");
        }
        return intlPhoneNumber;
    }


    public class RegistrationForm {
        private String firstName;
        private String otherNames;
        private String userPhone;
        private String userEmail;
        private String password;

        public RegistrationForm(String firstName, String otherNames, String phoneNumber, String email, String pin) {
            this.firstName = firstName;
            this.otherNames = otherNames;
            this.userPhone = phoneNumber;
            this.userEmail = email;
            this.password = pin;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getOtherNames() {
            return otherNames;
        }

        public String getUserPhone() {
            return userPhone;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public String getPassword() {
            return password;
        }
    }

}
