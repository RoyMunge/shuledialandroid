package org.sturgeon.shuledial.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.activities.Payment;
import org.sturgeon.shuledial.activities.ResultsPage;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.ExamResultsOverviewLoadedListener;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.recycler_adapters.AdapterResults;
import org.sturgeon.shuledial.tasks.TaskLoadResultsOverview;
import org.sturgeon.shuledial.utils.RecyclerViewListener.ClickListener;
import org.sturgeon.shuledial.utils.RecyclerViewListener.RecyclerTouchListener;
import org.sturgeon.shuledial.utils.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class FragmentResults extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ExamResultsOverviewLoadedListener {
    private static final String STATE_RESULTS = "state_results";
    private ArrayList<ExamResultsOverview> results = new ArrayList<>();
    private AdapterResults adapterResults;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerResults;
    private SessionManager sessionManager;
    private String currentStudentId;


    public FragmentResults() {
    }


    public static FragmentResults newInstance() {
        FragmentResults fragment = new FragmentResults();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_results, container, false);

        sessionManager = new SessionManager(getActivity());

        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeResults);
        LinearLayout noPremium = (LinearLayout) layout.findViewById(R.id.layoutPremium);

        String validUntil = sessionManager.GetValidUntil();

        if (!validUntil.equals("")) {
        Date date = StringtoDate(validUntil);
            if (date != null) {
                if (date.compareTo(new Date()) < 0) {


                    swipeRefreshLayout.setVisibility(View.GONE);
                    Button btnPremium = (Button) layout.findViewById(R.id.btnPremium);
                    noPremium.setVisibility(View.VISIBLE);
                    btnPremium.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getActivity(), Payment.class));
                        }
                    });
                } else {

                    noPremium.setVisibility(View.GONE);
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                }
            } else {
                noPremium.setVisibility(View.GONE);
                swipeRefreshLayout.setVisibility(View.VISIBLE);
            }
        } else {
            noPremium.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);
        }


        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerResults = (RecyclerView) layout.findViewById(R.id.listResults);
        recyclerResults.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterResults = new AdapterResults(getActivity());
        recyclerResults.setAdapter(adapterResults);
        currentStudentId = sessionManager.GetCurrentStudent();


        if (savedInstanceState != null) {
            results = savedInstanceState.getParcelableArrayList(STATE_RESULTS);
        } else {
            results = MyApplication.getWritableDatabase().readExamResultsOverview(currentStudentId);
            if (results == null ||  results.isEmpty()) {
                new TaskLoadResultsOverview(this, currentStudentId).execute();
            }
        }

        if (!results.isEmpty()) {
            adapterResults.setResults(results);
        }

        recyclerResults.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerResults, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (position != 0) {
                    Intent intent = new Intent(getActivity(), ResultsPage.class);
                    intent.putExtra("examid", results.get(position - 1).getExamid().toString());
                    intent.putExtra("studentid", results.get(position - 1).getStudentid().toString());
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(STATE_RESULTS, results);
    }

    @Override
    public void onRefresh() {
        L.t(getActivity(), "Refreshing Results");
        //load the whole feed again on refresh, dont try this at home :)
        final MainActivity mainActivity = (MainActivity) getActivity();
        if(mainActivity.isOnline()) {
            new TaskLoadResultsOverview(this, currentStudentId).execute();
        }
    }



    @Override
    public void onExamResultsOverviewLoaded(ArrayList<ExamResultsOverview> examResultsOverviewArrayList) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
        if (examResultsOverviewArrayList != null && !examResultsOverviewArrayList.isEmpty()) {
            this.results = examResultsOverviewArrayList;
            adapterResults.setResults(examResultsOverviewArrayList);
        }
    }

    private Date StringtoDate(String dateInString) {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        try {
            Date parsedDate = formatter.parse(dateInString);
            return parsedDate;
        } catch (ParseException e) {
            return null;
        }
    }

}