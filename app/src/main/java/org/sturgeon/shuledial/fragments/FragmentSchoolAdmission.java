package org.sturgeon.shuledial.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.callbacks.SchoolDetailsLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolDetails;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSchoolAdmission extends Fragment implements SchoolDetailsLoadedListener {

    TextView schoolAdmissionInfo;


    public FragmentSchoolAdmission() {
        // Required empty public constructor
    }



    public static FragmentSchoolAdmission newInstance(String shortname){
        FragmentSchoolAdmission fragmentSchoolAdmission = new FragmentSchoolAdmission();
        Bundle bundle = new Bundle();
        bundle.putString("shortname", shortname);
        fragmentSchoolAdmission.setArguments(bundle);
        return fragmentSchoolAdmission;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_school_admission, container, false);

        String schortname = getArguments().getString("shortname");

        new TaskLoadSchoolDetails(this, schortname).execute();

        schoolAdmissionInfo = (TextView) layout.findViewById(R.id.admission_info);

        return layout;
    }

    @Override
    public void onSchoolDetialsLoadedListener(SchoolBio schoolBio) {
        if (schoolBio != null) {
            schoolAdmissionInfo.setText(schoolBio.getAdmissionInfo());
        }

    }
}