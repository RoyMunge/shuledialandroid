package org.sturgeon.shuledial.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.callbacks.SchoolDetailsLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolDetails;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSchoolBio extends Fragment implements SchoolDetailsLoadedListener {


    TextView schoolCategory;
    TextView bio;


    public FragmentSchoolBio() {
        // Required empty public constructor
    }

    public static FragmentSchoolBio newInstance(String shortname){
        FragmentSchoolBio fragmentSchoolBio = new FragmentSchoolBio();
        Bundle bundle = new Bundle();
        bundle.putString("shortname", shortname);
        fragmentSchoolBio.setArguments(bundle);
        return fragmentSchoolBio;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_school_bio, container, false);

       String shortname = getArguments().getString("shortname");

       new TaskLoadSchoolDetails(this, shortname).execute();

        schoolCategory= (TextView) layout.findViewById(R.id.school_category);
        bio= (TextView) layout.findViewById(R.id.school_biography);

        return layout;
    }

    @Override
    public void onSchoolDetialsLoadedListener(SchoolBio schoolBio) {
        if (schoolBio != null){
            if (schoolBio.getSchoolTypeSet() == null || schoolBio.getSchoolTypeSet().isEmpty()){
                schoolCategory.setVisibility(View.GONE);
            } else {
                schoolCategory.setVisibility(View.VISIBLE);
                String categories = "";
                for (String category : schoolBio.getSchoolTypeSet()) {
                    categories = categories + category  + "; ";
                }
                schoolCategory.setText(categories);
            }

            bio.setText(schoolBio.getBio());
        }
    }
}
