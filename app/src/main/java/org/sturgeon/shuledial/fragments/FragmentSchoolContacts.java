package org.sturgeon.shuledial.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.callbacks.SchoolDetailsLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolDetails;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSchoolContacts extends Fragment implements SchoolDetailsLoadedListener {

    TextView schoolPhoneNumbers;
    TextView schoolEmails;
    TextView schoolPhysicalLocation;
    TextView schoolPostalAddress;




    public FragmentSchoolContacts() {
        // Required empty public constructor
    }

    public static FragmentSchoolContacts newInstance(String shortname){
        FragmentSchoolContacts fragmentSchoolContacts = new FragmentSchoolContacts();
        Bundle bundle = new Bundle();
        bundle.putString("shortname", shortname);
        fragmentSchoolContacts.setArguments(bundle);
        return fragmentSchoolContacts;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout =  inflater.inflate(R.layout.fragment_school_contacts, container, false);

        String shortname = getArguments().getString("shortname");

        new TaskLoadSchoolDetails(this, shortname).execute();

         schoolPhoneNumbers= (TextView) layout.findViewById(R.id.school_phone_numbers);
         schoolEmails= (TextView) layout.findViewById(R.id.school_email);
         schoolPhysicalLocation= (TextView) layout.findViewById(R.id.school_physical_address);
         schoolPostalAddress = (TextView) layout.findViewById(R.id.school_postal_address);

        return layout;
    }

    @Override
    public void onSchoolDetialsLoadedListener(SchoolBio schoolBio) {
        if (schoolBio != null){
            if (schoolBio.getPhoneNumberSet() == null || schoolBio.getPhoneNumberSet().isEmpty()){
                schoolPhoneNumbers.setVisibility(View.GONE);
            } else {
                schoolPhoneNumbers.setVisibility(View.VISIBLE);
                String phoneNumbers = new String();
                for (String phoneNumber: schoolBio.getPhoneNumberSet()){
                    phoneNumbers = phoneNumbers +  phoneNumber + "; " ;
                }
                schoolPhoneNumbers.setText(phoneNumbers);
            }

            if (schoolBio.getEmailSet() == null || schoolBio.getEmailSet().isEmpty()){
                schoolEmails.setVisibility(View.GONE);

            } else {
                schoolEmails.setVisibility(View.VISIBLE);
                String emails = new String();
                for(String email: schoolBio.getEmailSet()){
                    emails = emails + email + "; ";
                }
                schoolEmails.setText(emails);
            }

            if (schoolBio.getPhysicalAddress() == null){
                schoolPhysicalLocation.setVisibility(View.GONE);

            } else {
                schoolPhysicalLocation.setVisibility(View.VISIBLE);
                schoolPhysicalLocation.setText(schoolBio.getPhysicalAddress());

            }

            if (schoolBio.getPostalAddress() == null){
                schoolPostalAddress.setVisibility(View.GONE);

            } else {
                schoolPostalAddress.setVisibility(View.VISIBLE);
                schoolPostalAddress.setText(schoolBio.getPostalAddress());
            }
        }
    }
}
