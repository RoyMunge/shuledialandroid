package org.sturgeon.shuledial.navigation;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.amulyakhare.textdrawable.TextDrawable;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.AddSubscriber;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.activities.Schools;
import org.sturgeon.shuledial.student_accounts.AddStudentActivity;
import org.sturgeon.shuledial.activities.EventsCalendar;
import org.sturgeon.shuledial.activities.ResultsGraph;
import org.sturgeon.shuledial.student_accounts.ManageStudentsActivity;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.utils.ColorGenerator;
import org.sturgeon.shuledial.utils.SessionManager;

import java.util.ArrayList;

/**
 * Created by User on 08/06/2016.
 */
public class NavigationDrawer {
    private Activity activity;
    private Toolbar toolbar;
    private AccountHeader accountHeader;
    private Drawer drawer;
    private SessionManager sessionManager;

    public NavigationDrawer(Activity activity, Toolbar toolbar) {
        this.activity = activity;
        this.toolbar = toolbar;
        this.sessionManager = new SessionManager(activity);
    }

    public void setupMainDrawer() {
        accountHeader = new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.header2)
                /*.addProfiles(
                        new ProfileSettingDrawerItem().withName("Add Student").withIcon(new IconicsDrawable(activity, GoogleMaterial.Icon.gmd_add).actionBar().paddingDp(5).colorRes(R.color.material_drawer_primary_text))
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        //activity.startActivity(new Intent(activity, AddStudentActivity.class));
                                        return false;
                                    }
                                }),
                        new ProfileSettingDrawerItem().withName("Manage Students").withIcon(GoogleMaterial.Icon.gmd_settings)
                                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                                    @Override
                                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                        //activity.startActivity(new Intent(activity, ManageStudentsActivity.class));
                                        return false;
                                    }
                                })

                )*/
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        sessionManager.createStudentSession(Long.toString(profile.getIdentifier()));
                        activity.startActivity(new Intent(activity, MainActivity.class));
                        activity.finish();
                        return false;
                    }
                })
                .build();

        drawer = new DrawerBuilder()
                .withActivity(activity)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Add User").withIcon(FontAwesome.Icon.faw_user_plus).withIdentifier(1),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName("My Account").withIcon(R.drawable.ic_menu_manage).withIdentifier(2)

                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent = null;
                        if (drawerItem.getIdentifier() == 1) {
                            intent = new Intent(activity, AddSubscriber.class);
                        }
                        if (intent != null) {
                            activity.startActivity(intent);
                        }

                        return false;
                    }
                })
                .build();
    }

    public void addStudents(ArrayList<Student> students) {
        String currentStudent = sessionManager.GetCurrentStudent();

        for (int i = 0; i < students.size(); i++) {
            IProfile iProfile = new ProfileDrawerItem().withIdentifier(students.get(i).getStudentid()).withName(students.get(i).getSchoolName()).withEmail(students.get(i).getStudentName() + "    ADM: " + students.get(i).getAdmission()).withIcon(getIcon(students.get(i).getStudentName()));
            accountHeader.addProfile(iProfile, i);
            if(students.get(i).getStudentid().toString().equals(currentStudent)){
                accountHeader.setActiveProfile(iProfile);
            }

        }
        if(currentStudent.equals("")) {
            sessionManager.createStudentSession(Long.toString(accountHeader.getActiveProfile().getIdentifier()));
        }
    }

    public TextDrawable getIcon(String Name) {
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(Name);
        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                .withBorder(0)
                .endConfig()
                .round();
        TextDrawable drawable = builder.build(Name.substring(0, 1), color);
        return drawable;
    }

}
