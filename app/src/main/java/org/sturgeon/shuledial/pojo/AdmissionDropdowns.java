package org.sturgeon.shuledial.pojo;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by User on 30/01/2017.
 */

public class AdmissionDropdowns {

    private ArrayList<String> schoolLevels;
    private ArrayList<String> qualificationLevels;

    public AdmissionDropdowns(){}

    public AdmissionDropdowns(ArrayList<String> schoolLevels, ArrayList<String> qualificationLevels) {
        schoolLevels = schoolLevels;
        qualificationLevels = qualificationLevels;
    }

    public ArrayList<String> getSchoolLevels() {
        return schoolLevels;
    }

    public void setSchoolLevels(ArrayList<String> schoolLevels) {
        schoolLevels = schoolLevels;
    }

    public ArrayList<String> getQualificationLevels() {
        return qualificationLevels;
    }

    public void setQualificationLevels(ArrayList<String> qualificationLevels) {
        qualificationLevels = qualificationLevels;
    }

    @Override
    public String toString() {
        return getSchoolLevels().get(0);
    }
}
