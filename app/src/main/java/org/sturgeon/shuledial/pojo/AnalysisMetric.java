package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Roy on 03/04/2018.
 */

public class AnalysisMetric implements Parcelable {

    public static final Parcelable.Creator<AnalysisMetric> CREATOR
            = new Parcelable.Creator<AnalysisMetric>() {
        public AnalysisMetric createFromParcel(Parcel in) {
            return new AnalysisMetric(in);
        }

        public AnalysisMetric[] newArray(int size) {
            return new AnalysisMetric[size];
        }
    };

    private Long studentid;
    private Long examid;
    private long analysismetricid;
    private String analysisMetricName;
    private String shortName;
    private String entry;

    public AnalysisMetric() {
    }

    public AnalysisMetric(Parcel input){
        this.studentid = input.readLong();
        this.examid = input.readLong();
        this.analysismetricid = input.readLong();
        this.analysisMetricName = input.readString();
        this.shortName = input.readString();
        this.entry = input.readString();
    }

    public AnalysisMetric(Long studentid, Long examid, long analysismetricid, String analysisMetricName, String shortName, String entry) {
        this.studentid = studentid;
        this.examid = examid;
        this.analysismetricid = analysismetricid;
        this.analysisMetricName = analysisMetricName;
        this.shortName = shortName;
        this.entry = entry;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Long getExamid() {
        return examid;
    }

    public void setExamid(Long examid) {
        this.examid = examid;
    }

    public long getAnalysismetricid() {
        return analysismetricid;
    }

    public void setAnalysismetricid(long analysismetricid) {
        this.analysismetricid = analysismetricid;
    }

    public String getAnalysisMetricName() {
        return analysisMetricName;
    }

    public void setAnalysisMetricName(String analysisMetricName) {
        this.analysisMetricName = analysisMetricName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(studentid);
        dest.writeLong(examid);
        dest.writeLong(analysismetricid);
        dest.writeString(analysisMetricName);
        dest.writeString(shortName);
        dest.writeString(entry);
    }

    @Override
    public String toString() {
        return "AnalysisMetric{" +
                "studentid=" + studentid +
                ", examid=" + examid +
                ", analysismetricid=" + analysismetricid +
                ", analysisMetricName='" + analysisMetricName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", entry='" + entry + '\'' +
                '}';
    }
}
