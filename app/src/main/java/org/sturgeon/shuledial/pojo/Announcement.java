package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by User on 28/05/2016.
 */
public class Announcement implements Parcelable {

    public static final Parcelable.Creator<Announcement> CREATOR
            = new Parcelable.Creator<Announcement>() {
        public Announcement createFromParcel(Parcel in) {
            return new Announcement(in);
        }

        public Announcement[] newArray(int size) {
            return new Announcement[size];
        }
    };
    private Long announcementid;
    private Long studentid;
    private String announcementName;
    private Date postingDate;
    private String description;

    public Announcement() {
    }

    public Announcement(Parcel input) {
        announcementid = input.readLong();
        studentid = input.readLong();
        announcementName = input.readString();
        long dateMillis = input.readLong();
        postingDate = new Date(dateMillis);
        description = input.readString();
    }

    public Announcement(Long announcementid, Long studentid, String announcementName, Date postingDate, String description) {
        this.announcementid = announcementid;
        this.studentid = studentid;
        this.announcementName = announcementName;
        this.postingDate = postingDate;
        this.description = description;
    }

    public Long getAnnouncementid() {
        return announcementid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public void setAnnouncementid(Long announcementid) {
        this.announcementid = announcementid;
    }

    public String getAnnouncementName() {
        return announcementName;
    }

    public void setAnnouncementName(String announcementName) {
        this.announcementName = announcementName;
    }

    public Date getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(Date postingDate) {
        this.postingDate = postingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(announcementid);
        dest.writeLong(studentid);
        dest.writeString(announcementName);
        dest.writeLong(postingDate.getTime());
        dest.writeString(description);
    }

    @Override
    public String toString() {
        return "Announcement{" +
                "announcementid=" + announcementid +
                ", studentid=" + studentid +
                ", announcementName='" + announcementName + '\'' +
                ", postingDate=" + postingDate +
                ", description='" + description + '\'' +
                '}';
    }
}
