package org.sturgeon.shuledial.pojo;


/**
 * Created by User on 30/01/2017.
 */

public class County {
    private Long countyid;
    private String countyName;

    public County(){}

    public County(Long countyid, String countyName) {
        this.countyid = countyid;
        this.countyName = countyName;
    }

    public Long getCountyid() {
        return countyid;
    }

    public void setCountyid(Long countyid) {
        this.countyid = countyid;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    @Override
    public String toString() {
        return countyName;
    }
}
