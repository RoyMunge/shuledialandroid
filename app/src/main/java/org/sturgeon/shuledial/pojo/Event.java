package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by User on 28/05/2016.
 */
public class Event implements Parcelable{

    public static final Parcelable.Creator<Event> CREATOR
            = new Parcelable.Creator<Event>() {
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    private Long eventid;
    private Long studentid;
    private String eventName;
    private String location;
    private Date eventDate;
    private Date endDate;
    private String description;

    public Event(){}

    public Event(Parcel input){
        eventid = input.readLong();
        studentid = input.readLong();
        eventName = input.readString();
        location = input.readString();
        long dateMillis = input.readLong();
        eventDate = new Date(dateMillis);
        long endDateMillis = input.readLong();
        endDate = new Date(endDateMillis);
        description = input.readString();
    }

    public Event(Long eventid, Long studentid, String eventName, String location, Date eventDate, Date endDate, String description) {
        this.eventid = eventid;
        this.studentid = studentid;
        this.eventName = eventName;
        this.location = location;
        this.eventDate = eventDate;
        this.endDate = endDate;
        this.description = description;
    }

    public Long getEventid() {
        return eventid;
    }

    public void setEventid(Long eventid) {
        this.eventid = eventid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(eventid);
        dest.writeLong(studentid);
        dest.writeString(eventName);
        dest.writeString(location);
        dest.writeLong(eventDate.getTime());
        dest.writeLong(endDate.getTime());
        dest.writeString(description);
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventid=" + eventid +
                ", studentid=" + studentid +
                ", eventName='" + eventName + '\'' +
                ", location='" + location + '\'' +
                ", eventDate=" + eventDate +
                ", endDate=" + endDate +
                ", description='" + description + '\'' +
                '}';
    }
}
