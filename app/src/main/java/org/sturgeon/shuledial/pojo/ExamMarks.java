package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Roy on 03/04/2018.
 */

public class ExamMarks implements Parcelable {
    public static final Parcelable.Creator<ExamMarks> CREATOR
            = new Parcelable.Creator<ExamMarks>() {
        public ExamMarks createFromParcel(Parcel in) {
            return new ExamMarks(in);
        }

        public ExamMarks[] newArray(int size) {
            return new ExamMarks[size];
        }
    };

    private Long studentid;
    private Long examid;
    private Long subjectid;
    private String subjectName;
    private String shortName;
    private String marks;
    private String grade;
    private String position;

    public ExamMarks() {
    }

    public ExamMarks(Parcel input){
        this.studentid = input.readLong();
        this.examid = input.readLong();
        this.subjectid = input.readLong();
        this.subjectName = input.readString();
        this.shortName = input.readString();
        this.marks = input.readString();
        this.grade = input.readString();
        this.position = input.readString();
    }

    public ExamMarks(Long studentid, Long examid, Long subjectid, String subjectName, String shortName, String marks, String grade, String position) {
        this.studentid = studentid;
        this.examid = examid;
        this.subjectid = subjectid;
        this.subjectName = subjectName;
        this.shortName = shortName;
        this.marks = marks;
        this.grade = grade;
        this.position = position;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Long getExamid() {
        return examid;
    }

    public void setExamid(Long examid) {
        this.examid = examid;
    }

    public Long getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(Long subjectid) {
        this.subjectid = subjectid;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(studentid);
        dest.writeLong(examid);
        dest.writeLong(subjectid);
        dest.writeString(subjectName);
        dest.writeString(shortName);
        dest.writeString(marks);
        dest.writeString(grade);
        dest.writeString(position);
    }

    @Override
    public String toString() {
        return "ExamMarks{" +
                "studentid=" + studentid +
                ", examid=" + examid +
                ", subjectid=" + subjectid +
                ", subjectName='" + subjectName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", marks='" + marks + '\'' +
                ", grade='" + grade + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
