package org.sturgeon.shuledial.pojo;

import java.util.Date;

/**
 * Created by User on 20/06/2016.
 */
public class ExamResults {
    private Long examresultid;
    private Long studentid;
    private String examName;
    private Date examDate;
    private String english;
    private String kiswahili;
    private String maths;
    private String science;
    private String biology;
    private String physics;
    private String chemistry;
    private String history;
    private String geography;
    private String cre;
    private String ire;
    private String hre;
    private String homescience;
    private String art;
    private String agriculture;
    private String computer;
    private String aviation;
    private String french;
    private String german;
    private String arabic;
    private String music;
    private String businessstudies;
    private String socialstudies;
    private String ssre;
    private String total;
    private String average;
    private String grade;
    private String position;

    public Long getExamresultid() {
        return examresultid;
    }

    public void setExamresultid(Long examresultid) {
        this.examresultid = examresultid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getKiswahili() {
        return kiswahili;
    }

    public void setKiswahili(String kiswahili) {
        this.kiswahili = kiswahili;
    }

    public String getMaths() {
        return maths;
    }

    public void setMaths(String maths) {
        this.maths = maths;
    }

    public String getScience() {
        return science;
    }

    public void setScience(String science) {
        this.science = science;
    }

    public String getBiology() {
        return biology;
    }

    public void setBiology(String biology) {
        this.biology = biology;
    }

    public String getPhysics() {
        return physics;
    }

    public void setPhysics(String physics) {
        this.physics = physics;
    }

    public String getChemistry() {
        return chemistry;
    }

    public void setChemistry(String chemistry) {
        this.chemistry = chemistry;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getGeography() {
        return geography;
    }

    public void setGeography(String geography) {
        this.geography = geography;
    }

    public String getCre() {
        return cre;
    }

    public void setCre(String cre) {
        this.cre = cre;
    }

    public String getIre() {
        return ire;
    }

    public void setIre(String ire) {
        this.ire = ire;
    }

    public String getHre() {
        return hre;
    }

    public void setHre(String hre) {
        this.hre = hre;
    }

    public String getHomescience() {
        return homescience;
    }

    public void setHomescience(String homescience) {
        this.homescience = homescience;
    }

    public String getArt() {
        return art;
    }

    public void setArt(String art) {
        this.art = art;
    }

    public String getAgriculture() {
        return agriculture;
    }

    public void setAgriculture(String agriculture) {
        this.agriculture = agriculture;
    }

    public String getComputer() {
        return computer;
    }

    public void setComputer(String commputer) {
        this.computer = commputer;
    }

    public String getAviation() {
        return aviation;
    }

    public void setAviation(String aviation) {
        this.aviation = aviation;
    }

    public String getFrench() {
        return french;
    }

    public void setFrench(String french) {
        this.french = french;
    }

    public String getGerman() {
        return german;
    }

    public void setGerman(String german) {
        this.german = german;
    }

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }

    public String getBusinessstudies() {
        return businessstudies;
    }

    public void setBusinessstudies(String businessstudies) {
        this.businessstudies = businessstudies;
    }

    public String getSocialstudies() {
        return socialstudies;
    }

    public void setSocialstudies(String socialstudies) {
        this.socialstudies = socialstudies;
    }

    public String getSsre() {
        return ssre;
    }

    public void setSsre(String ssre) {
        this.ssre = ssre;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "ExamResults{" +
                "examresultid=" + examresultid +
                ", studentid=" + studentid +
                ", examName='" + examName + '\'' +
                ", examDate=" + examDate +
                ", english='" + english + '\'' +
                ", kiswahili='" + kiswahili + '\'' +
                ", maths='" + maths + '\'' +
                ", science='" + science + '\'' +
                ", biology='" + biology + '\'' +
                ", physics='" + physics + '\'' +
                ", chemistry='" + chemistry + '\'' +
                ", history='" + history + '\'' +
                ", geography='" + geography + '\'' +
                ", cre='" + cre + '\'' +
                ", ire='" + ire + '\'' +
                ", hre='" + hre + '\'' +
                ", homescience='" + homescience + '\'' +
                ", art='" + art + '\'' +
                ", agriculture='" + agriculture + '\'' +
                ", computer='" + computer + '\'' +
                ", aviation='" + aviation + '\'' +
                ", french='" + french + '\'' +
                ", german='" + german + '\'' +
                ", arabic='" + arabic + '\'' +
                ", music='" + music + '\'' +
                ", businessstudies='" + businessstudies + '\'' +
                ", socialstudies='" + socialstudies + '\'' +
                ", ssre='" + ssre + '\'' +
                ", total='" + total + '\'' +
                ", average='" + average + '\'' +
                ", grade='" + grade + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
