package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by User on 15/06/2016.
 */
public class ExamResultsMinimal implements Parcelable{

    public static final Parcelable.Creator<ExamResultsMinimal> CREATOR
            = new Parcelable.Creator<ExamResultsMinimal>() {
        public ExamResultsMinimal createFromParcel(Parcel in) {
            return new ExamResultsMinimal(in);
        }
        public ExamResultsMinimal[] newArray(int size) {
            return new ExamResultsMinimal[size];
        }
    };

    private Long examresultid;
    private Long studentid;
    private String examName;
    private Date examDate;
    private String total;
    private String average;
    private String grade;
    private String position;

    public ExamResultsMinimal(){}

    public ExamResultsMinimal(Parcel input){
        examresultid = input.readLong();
        studentid = input.readLong();
        examName = input.readString();
        long dateMillis=input.readLong();
        examDate =new Date(dateMillis);
        total = input.readString();
        average = input.readString();
        grade = input.readString();
        position =input.readString();
    }

    public ExamResultsMinimal(Long examresultid, Long studentid, String examName, Date examDate, String total, String average, String grade, String position) {
        this.examresultid = examresultid;
        this.studentid = studentid;
        this.examName = examName;
        this.examDate = examDate;
        this.total = total;
        this.average = average;
        this.grade = grade;
        this.position = position;
    }

    public Long getExamresultid() {
        return examresultid;
    }

    public void setExamresultid(Long examresultid) {
        this.examresultid = examresultid;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(examresultid);
        dest.writeLong(studentid);
        dest.writeString(examName);
        dest.writeLong(examDate.getTime());
        dest.writeString(total);
        dest.writeString(average);
        dest.writeString(grade);
        dest.writeString(position);
    }

    @Override
    public String toString() {
        return "ExamResultsMinimal{" +
                "examresultid=" + examresultid +
                ", studentid=" + studentid +
                ", examName='" + examName + '\'' +
                ", examDate=" + examDate +
                ", total='" + total + '\'' +
                ", average='" + average + '\'' +
                ", grade='" + grade + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
