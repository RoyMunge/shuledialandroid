package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Roy on 03/04/2018.
 */

public class ExamResultsOverview implements Parcelable {
    public static final Parcelable.Creator<ExamResultsOverview> CREATOR
            = new Parcelable.Creator<ExamResultsOverview>() {
        public ExamResultsOverview createFromParcel(Parcel in) {
            return new ExamResultsOverview(in);
        }
        public ExamResultsOverview[] newArray(int size) {
            return new ExamResultsOverview[size];
        }
    };

    private Long studentid;
    private Long examid;
    private String examName;
    private Date examDate;
    private String total;
    private String average;
    private String grade;
    private String position;

    public ExamResultsOverview() {
    }

    public ExamResultsOverview(Parcel input){
        this.studentid = input.readLong();
        this.examid = input.readLong();
        this.examName = input.readString();
        long dateMillis = input.readLong();
        this.examDate = new Date(dateMillis);
        this.total = input.readString();
        this.average = input.readString();
        this.grade = input.readString();
        this.position = input.readString();
    }

    public ExamResultsOverview(Long studentid, Long examid, String examName, Date examDate, String total, String average, String grade, String position) {
        this.studentid = studentid;
        this.examid = examid;
        this.examName = examName;
        this.examDate = examDate;
        this.total = total;
        this.average = average;
        this.grade = grade;
        this.position = position;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Long getExamid() {
        return examid;
    }

    public void setExamid(Long examid) {
        this.examid = examid;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(studentid);
        dest.writeLong(examid);
        dest.writeString(examName);
        dest.writeLong(examDate.getTime());
        dest.writeString(total);
        dest.writeString(average);
        dest.writeString(grade);
        dest.writeString(position);
    }

    @Override
    public String toString() {
        return "ExamResultsOverview{" +
                "studentid=" + studentid +
                ", examid=" + examid +
                ", examName='" + examName + '\'' +
                ", examDate=" + examDate +
                ", total='" + total + '\'' +
                ", average='" + average + '\'' +
                ", grade='" + grade + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
