package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 06/02/2017.
 */

public class FeeBalance implements Parcelable{
    private String balance;
    private String paid;
    private String paymentInfo;

    public FeeBalance() {
    }

    public FeeBalance(String balance, String paid, String paymentInfo) {
        this.balance = balance;
        this.paid = paid;
        this.paymentInfo = paymentInfo;
    }

    protected FeeBalance(Parcel in) {
        balance = in.readString();
        paid = in.readString();
        paymentInfo = in.readString();
    }

    public static final Creator<FeeBalance> CREATOR = new Creator<FeeBalance>() {
        @Override
        public FeeBalance createFromParcel(Parcel in) {
            return new FeeBalance(in);
        }

        @Override
        public FeeBalance[] newArray(int size) {
            return new FeeBalance[size];
        }
    };

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(String paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(balance);
        parcel.writeString(paid);
        parcel.writeString(paymentInfo);
    }
}
