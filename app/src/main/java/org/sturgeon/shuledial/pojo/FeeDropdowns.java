package org.sturgeon.shuledial.pojo;

import java.util.ArrayList;

/**
 * Created by User on 06/02/2017.
 */

public class FeeDropdowns {
    ArrayList<SchoolPeriod> schoolPeriods;
    ArrayList<SchoolLevel> schoolLevels;

    public FeeDropdowns(ArrayList<SchoolPeriod> schoolPeriods, ArrayList<SchoolLevel> schoolLevels) {
        this.schoolPeriods = schoolPeriods;
        this.schoolLevels = schoolLevels;
    }

    public ArrayList<SchoolPeriod> getSchoolPeriods() {
        return schoolPeriods;
    }

    public void setSchoolPeriods(ArrayList<SchoolPeriod> schoolPeriods) {
        this.schoolPeriods = schoolPeriods;
    }

    public ArrayList<SchoolLevel> getSchoolLevels() {
        return schoolLevels;
    }

    public void setSchoolLevels(ArrayList<SchoolLevel> schoolLevels) {
        this.schoolLevels = schoolLevels;
    }
}
