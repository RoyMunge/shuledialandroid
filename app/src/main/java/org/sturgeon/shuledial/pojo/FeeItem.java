package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 06/02/2017.
 */

public class FeeItem implements Parcelable {
    private String name;
    private String amount;

    public FeeItem(String name, String amount) {
        this.name = name;
        this.amount = amount;
    }

    protected FeeItem(Parcel in) {
        name = in.readString();
        amount = in.readString();
    }

    public static final Creator<FeeItem> CREATOR = new Creator<FeeItem>() {
        @Override
        public FeeItem createFromParcel(Parcel in) {
            return new FeeItem(in);
        }

        @Override
        public FeeItem[] newArray(int size) {
            return new FeeItem[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(amount);
    }
}
