package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Roy on 03/04/2018.
 */

public class FeeStructure implements Parcelable {
    public static final Parcelable.Creator<FeeStructure> CREATOR
            = new Parcelable.Creator<FeeStructure>() {
        public FeeStructure createFromParcel(Parcel in) {
            return new FeeStructure(in);
        }
        public FeeStructure[] newArray(int size) {
            return new FeeStructure[size];
        }
    };

    private Long studentid;
    private Long feeitemid;
    private String feeItemName;
    private String shortName;
    private String amount;

    public FeeStructure() {
    }

    public FeeStructure(Parcel input){
        this.studentid = input.readLong();
        this.feeitemid = input.readLong();
        this.feeItemName = input.readString();
        this.shortName = input.readString();
        this.amount = input.readString();
    }

    public FeeStructure(Long studentid, Long feeitemid, String feeItemName, String shortName, String amount) {
        this.studentid = studentid;
        this.feeitemid = feeitemid;
        this.feeItemName = feeItemName;
        this.shortName = shortName;
        this.amount = amount;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public Long getFeeitemid() {
        return feeitemid;
    }

    public void setFeeitemid(Long feeitemid) {
        this.feeitemid = feeitemid;
    }

    public String getFeeItemName() {
        return feeItemName;
    }

    public void setFeeItemName(String feeItemName) {
        this.feeItemName = feeItemName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(studentid);
        dest.writeLong(feeitemid);
        dest.writeString(feeItemName);
        dest.writeString(shortName);
        dest.writeString(amount);
    }

    @Override
    public String toString() {
        return "FeeStructure{" +
                "studentid=" + studentid +
                ", feeitemid=" + feeitemid +
                ", feeItemName='" + feeItemName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
