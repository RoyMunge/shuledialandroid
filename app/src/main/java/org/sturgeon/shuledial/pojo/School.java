package org.sturgeon.shuledial.pojo;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 13/07/2016.
 */
public class School implements Parcelable {

    public static final Parcelable.Creator<School> CREATOR = new Parcelable.Creator<School>() {
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        public School[] newArray(int size) {
            return new School[size];
        }
    };

    private String schoolShortName;
    private String schoolName;

    public School() {
    }

    public School(Parcel input) {
        schoolShortName = input.readString();
        schoolName = input.readString();
    }

    public School(String schoolShortName, String schoolName) {
        this.schoolShortName = schoolShortName;
        this.schoolName = schoolName;
    }

    public String getSchoolShortName() {
        return schoolShortName;
    }

    public void setSchoolShortName(String schoolShortName) {
        this.schoolShortName = schoolShortName;
    }

    public String getSchoolName() {
        return schoolName;
    }


    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(schoolShortName);
        dest.writeString(schoolName);
    }
}
