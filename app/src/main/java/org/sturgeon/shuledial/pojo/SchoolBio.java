package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 24/11/2016.
 */
public class SchoolBio implements Parcelable {

    public static final Parcelable.Creator<SchoolBio> CREATOR
            = new Parcelable.Creator<SchoolBio>() {
        public SchoolBio createFromParcel(Parcel in) {
            return new SchoolBio(in);
        }
        public SchoolBio[] newArray(int size) {
            return new SchoolBio[size];
        }
    };

    private Long schoolid;
    private String name;
    private String shortName;
    private List<String> phoneNumberSet;
    private List<String> emailSet;
    private List<String> schoolTypeSet;
    private String website;
    private String motto;
    private String physicalAddress;
    private String postalAddress;
    private String bio;
    private String admissionInfo;
    private String feesPaymentInfo;
    private String county;
    private String subCounty;

    public SchoolBio(){}

    public SchoolBio(Parcel input){
        this.schoolid = input.readLong();
        this.name = input.readString();
        this.shortName = input.readString();
        List<String> inPhoneNumberSet = null;
        input.readStringList(inPhoneNumberSet);
        this.phoneNumberSet = inPhoneNumberSet;
        List<String> inEmailSet = null;
        input.readStringList(inEmailSet);
        this.emailSet = inEmailSet;
        List<String> inSchoolTypeSet = null;
        input.readStringList(inSchoolTypeSet);
        this.schoolTypeSet = inSchoolTypeSet;
        this.website = input.readString();
        this.motto = input.readString();
        this.physicalAddress = input.readString();
        this.postalAddress = input.readString();
        this.bio = input.readString();
        this.admissionInfo = input.readString();
        this.feesPaymentInfo = input.readString();
        this.county = input.readString();
        this.subCounty = input.readString();
    }

    public SchoolBio(Long schoolid, String name, String shortName, List<String> phoneNumberSet, List<String> emailSet, List<String> schoolTypeSet, String website, String motto, String physicalAddress, String postalAddress, String bio, String admissionInfo, String feesPaymentInfo, String county, String subCounty) {
        this.schoolid = schoolid;
        this.name = name;
        this.shortName = shortName;
        this.phoneNumberSet = phoneNumberSet;
        this.emailSet = emailSet;
        this.schoolTypeSet = schoolTypeSet;
        this.website = website;
        this.motto = motto;
        this.physicalAddress = physicalAddress;
        this.postalAddress = postalAddress;
        this.bio = bio;
        this.admissionInfo = admissionInfo;
        this.feesPaymentInfo = feesPaymentInfo;
        this.county = county;
        this.subCounty = subCounty;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<String> getPhoneNumberSet() {
        return phoneNumberSet;
    }

    public void setPhoneNumberSet(List<String> phoneNumberSet) {
        this.phoneNumberSet = phoneNumberSet;
    }

    public List<String> getEmailSet() {
        return emailSet;
    }

    public void setEmailSet(List<String> emailSet) {
        this.emailSet = emailSet;
    }

    public List<String> getSchoolTypeSet() {
        return schoolTypeSet;
    }

    public void setSchoolTypeSet(List<String> schoolTypeSet) {
        this.schoolTypeSet = schoolTypeSet;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAdmissionInfo() {
        return admissionInfo;
    }

    public void setAdmissionInfo(String admissionInfo) {
        this.admissionInfo = admissionInfo;
    }

    public String getFeesPaymentInfo() {
        return feesPaymentInfo;
    }

    public void setFeesPaymentInfo(String feesPaymentInfo) {
        this.feesPaymentInfo = feesPaymentInfo;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getSubCounty() {
        return subCounty;
    }

    public void setSubCounty(String subCounty) {
        this.subCounty = subCounty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(schoolid);
        dest.writeString(name);
        dest.writeString(shortName);
        dest.writeStringList(phoneNumberSet);
        dest.writeStringList(emailSet);
        dest.writeStringList(schoolTypeSet);
        dest.writeString(website);
        dest.writeString(motto);
        dest.writeString(physicalAddress);
        dest.writeString(postalAddress);
        dest.writeString(bio);
        dest.writeString(admissionInfo);
        dest.writeString(feesPaymentInfo);
        dest.writeString(county);
        dest.writeString(subCounty);
    }
}
