package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 25/01/2017.
 */

public class SchoolBioMinimal implements Parcelable {

    private static final Parcelable.Creator<SchoolBioMinimal> CREATOR = new Parcelable.Creator<SchoolBioMinimal>(){
        public SchoolBioMinimal createFromParcel(Parcel in) {return new SchoolBioMinimal(in);}
        public SchoolBioMinimal[] newArray(int size){return new SchoolBioMinimal[size];}
    };

    private Long schoolid;
    private String name;
    private String shortName;
    private String motto;
    private String physicalAddress;
    private String bio;
    private String county;

    public SchoolBioMinimal(){}

    public SchoolBioMinimal(Parcel in){
        this.schoolid = in.readLong();
        this.name = in.readString();
        this.shortName = in.readString();
        this.motto = in.readString();
        this.physicalAddress = in.readString();
        this.bio = in.readString();
        this.county = in.readString();
    }

    public SchoolBioMinimal(Long schoolid, String name, String shortName, String motto, String physicalAddress, String bio, String county) {
        this.schoolid = schoolid;
        this.name = name;
        this.shortName = shortName;
        this.motto = motto;
        this.physicalAddress = physicalAddress;
        this.bio = bio;
        this.county = county;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Long getSchoolid() {
        return schoolid;
    }

    public void setSchoolid(Long schoolid) {
        this.schoolid = schoolid;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(schoolid);
        dest.writeString(shortName);
        dest.writeString(motto);
        dest.writeString(physicalAddress);
        dest.writeString(bio);
        dest.writeString(county);
    }
}
