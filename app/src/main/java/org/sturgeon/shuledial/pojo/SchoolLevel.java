package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 06/02/2017.
 */

public class SchoolLevel implements Parcelable {
    private String schoollevelid;
    private String schoollevelname;

    public SchoolLevel(String schoollevelid, String schoollevelname) {
        this.schoollevelid = schoollevelid;
        this.schoollevelname = schoollevelname;
    }

    protected SchoolLevel(Parcel in) {
        schoollevelid = in.readString();
        schoollevelname = in.readString();
    }

    public static final Creator<SchoolLevel> CREATOR = new Creator<SchoolLevel>() {
        @Override
        public SchoolLevel createFromParcel(Parcel in) {
            return new SchoolLevel(in);
        }

        @Override
        public SchoolLevel[] newArray(int size) {
            return new SchoolLevel[size];
        }
    };

    public String getSchoollevelid() {
        return schoollevelid;
    }

    public void setSchoollevelid(String schoollevelid) {
        this.schoollevelid = schoollevelid;
    }

    public String getSchoollevelname() {
        return schoollevelname;
    }

    public void setSchoollevelname(String schoollevelname) {
        this.schoollevelname = schoollevelname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(schoollevelid);
        parcel.writeString(schoollevelname);
    }

    @Override
    public String toString() {
        return schoollevelname;
    }
}
