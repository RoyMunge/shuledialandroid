package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 06/02/2017.
 */

public class SchoolPeriod implements Parcelable{
    private String schoolperiodid;
    private String schoolperoidname;

    public SchoolPeriod(String schoolperiodid, String schoolperoidname) {
        this.schoolperiodid = schoolperiodid;
        this.schoolperoidname = schoolperoidname;
    }

    protected SchoolPeriod(Parcel in) {
        schoolperiodid = in.readString();
        schoolperoidname = in.readString();
    }

    public static final Creator<SchoolPeriod> CREATOR = new Creator<SchoolPeriod>() {
        @Override
        public SchoolPeriod createFromParcel(Parcel in) {
            return new SchoolPeriod(in);
        }

        @Override
        public SchoolPeriod[] newArray(int size) {
            return new SchoolPeriod[size];
        }
    };

    public String getSchoolperiodid() {
        return schoolperiodid;
    }

    public void setSchoolperiodid(String schoolperiodid) {
        this.schoolperiodid = schoolperiodid;
    }

    public String getSchoolperoidname() {
        return schoolperoidname;
    }

    public void setSchoolperoidname(String schoolperoidname) {
        this.schoolperoidname = schoolperoidname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(schoolperiodid);
        parcel.writeString(schoolperoidname);
    }

    @Override
    public String toString() {
        return schoolperoidname;
    }
}
