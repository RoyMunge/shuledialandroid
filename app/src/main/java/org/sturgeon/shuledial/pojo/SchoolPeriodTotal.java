package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by User on 06/02/2017.
 */

public class SchoolPeriodTotal implements Parcelable {
    private String periodName;
    private String levelName;
    private Date startDate;
    private Date endDate;
    private String totalAmount;

    public SchoolPeriodTotal(String periodName, String levelName, Date startDate, Date endDate, String totalAmount) {
        this.periodName = periodName;
        this.levelName = levelName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.totalAmount = totalAmount;
    }

    protected SchoolPeriodTotal(Parcel in) {
        periodName = in.readString();
        levelName = in.readString();
        totalAmount = in.readString();
    }

    public static final Creator<SchoolPeriodTotal> CREATOR = new Creator<SchoolPeriodTotal>() {
        @Override
        public SchoolPeriodTotal createFromParcel(Parcel in) {
            return new SchoolPeriodTotal(in);
        }

        @Override
        public SchoolPeriodTotal[] newArray(int size) {
            return new SchoolPeriodTotal[size];
        }
    };

    public String getPeriodName() {
        return periodName;
    }

    public void setPeriodName(String periodName) {
        this.periodName = periodName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(periodName);
        parcel.writeString(levelName);
        parcel.writeString(totalAmount);
    }
}
