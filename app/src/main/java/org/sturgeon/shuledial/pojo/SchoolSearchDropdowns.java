package org.sturgeon.shuledial.pojo;

import java.util.ArrayList;

/**
 * Created by User on 30/01/2017.
 */

public class SchoolSearchDropdowns {

    ArrayList<County> counties;
    ArrayList<SchoolType> categories;

    public SchoolSearchDropdowns() {
        counties = new ArrayList<>();
        categories = new ArrayList<>();
    }

    public SchoolSearchDropdowns(ArrayList<County> counties, ArrayList<SchoolType> categories) {
        this.counties = counties;
        this.categories = categories;
    }

    public ArrayList<County> getCounties() {
        return counties;
    }

    public void setCounties(ArrayList<County> counties) {
        this.counties = counties;
    }

    public ArrayList<SchoolType> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<SchoolType> categories) {
        this.categories = categories;
    }
}
