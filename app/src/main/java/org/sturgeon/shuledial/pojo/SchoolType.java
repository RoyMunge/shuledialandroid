package org.sturgeon.shuledial.pojo;

/**
 * Created by User on 30/01/2017.
 */

public class SchoolType {

    private Long schooltypeid;
    private String schoolTypeName;

    public SchoolType(){}

    public SchoolType(Long schooltypeid, String schoolTypeName) {
        this.schooltypeid = schooltypeid;
        this.schoolTypeName = schoolTypeName;
    }

    public Long getSchooltypeid() {
        return schooltypeid;
    }

    public void setSchooltypeid(Long schooltypeid) {
        this.schooltypeid = schooltypeid;
    }

    public String getSchoolTypeName() {
        return schoolTypeName;
    }

    public void setSchoolTypeName(String schoolTypeName) {
        this.schoolTypeName = schoolTypeName;
    }

    @Override
    public String toString() {
        return schoolTypeName;
    }
}
