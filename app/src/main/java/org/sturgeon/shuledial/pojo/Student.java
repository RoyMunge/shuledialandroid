package org.sturgeon.shuledial.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by User on 28/05/2016.
 */
public class Student implements Parcelable{

    public static final Parcelable.Creator<Student> CREATOR = new Parcelable.Creator<Student>() {
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    private Long studentid;
    private String admission;
    private String studentName;
    private String schoolName;


    public Student (){}

    public Student(Parcel input) {
        studentid = input.readLong();
        admission = input.readString();
        studentName = input.readString();
        schoolName = input.readString();
    }

    public Student(Long studentid, String admission, String studentName, String schoolName) {
        this.studentid = studentid;
        this.admission = admission;
        this.studentName = studentName;
        this.schoolName = schoolName;
    }

    public Long getStudentid() {
        return studentid;
    }

    public void setStudentid(Long studentid) {
        this.studentid = studentid;
    }

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(studentid);
        dest.writeString(admission);
        dest.writeString(studentName);
        dest.writeString(schoolName);
    }

    @Override
    public String toString() {
        return studentName;
    }
}
