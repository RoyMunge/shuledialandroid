package org.sturgeon.shuledial.pojo;

/**
 * Created by Roy on 04/04/2018.
 */

public class VerificationForm {
    private String phoneNumber;
    private String verificationCode;

    public VerificationForm(String phoneNumber, String verificationCode) {
        this.phoneNumber = phoneNumber;
        this.verificationCode = verificationCode;
    }
}