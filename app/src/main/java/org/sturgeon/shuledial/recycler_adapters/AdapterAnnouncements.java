package org.sturgeon.shuledial.recycler_adapters;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.Announcement;
import org.sturgeon.shuledial.utils.SchoolPeriod;
import org.sturgeon.shuledial.utils.Sorter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;

/**
 * Created by User on 12/06/2016.
 */
public class AdapterAnnouncements extends RecyclerView.Adapter<AdapterAnnouncements.AnnouncementsHolder> {
    private ArrayList<Announcement> announcements = new ArrayList<>();
    private LayoutInflater inflater;
    private SimpleDateFormat monthDate = new SimpleDateFormat("MMM dd");
    private SimpleDateFormat time = new SimpleDateFormat("HH:mm");
    private Sorter sorter = new Sorter();

    public AdapterAnnouncements(Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setAnnouncements(ArrayList<Announcement> announcements) {
        this.announcements = announcements;
        if(!announcements.isEmpty()){
            sorter.sortAnnouncementsByDate(announcements);
            notifyDataSetChanged();
        }
    }

    @Override
    public AnnouncementsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.recycler_row_announcement, parent, false);
            AnnouncementsHolder announcementsHolder = new AnnouncementsHolder(view);
            return announcementsHolder;
    }

    @Override
    public void onBindViewHolder(AnnouncementsHolder holder, int position) {
        Announcement current = announcements.get(position);
        holder.bind(current);

    }

    @Override
    public int getItemCount() {
        return announcements.size();
    }

    public TextDrawable getIcon(String date){
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(date);
        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                .withBorder(0)
                .fontSize(32)
                .endConfig()
                .roundRect(20);
        TextDrawable drawable = builder.build(date,color);
        return drawable;
    }

    class AnnouncementsHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView description;
        TextView date;
        CheckBox starredBox;

        public AnnouncementsHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.announcement_title);
            date = (TextView) view.findViewById(R.id.announcement_date);
            description = (TextView) view.findViewById(R.id.announcement_description);
            starredBox =  (CheckBox) view.findViewById(R.id.starred_box);
        }

        public void bind(Announcement announcement){
            Calendar c = Calendar.getInstance();
            if(monthDate.format(announcement.getPostingDate()).equals(monthDate.format(c.getTime()))){
                date.setText(time.format(announcement.getPostingDate()));
            } else{
                date.setText(monthDate.format(announcement.getPostingDate()));
            }
            title.setText(announcement.getAnnouncementName());
            description.setText(announcement.getDescription());
        }
    }
}
