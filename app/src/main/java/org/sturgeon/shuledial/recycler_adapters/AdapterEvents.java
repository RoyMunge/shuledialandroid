package org.sturgeon.shuledial.recycler_adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.utils.ColorGenerator;
import org.sturgeon.shuledial.utils.SaveEventDialog;
import org.sturgeon.shuledial.utils.Sorter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by User on 12/06/2016.
 */
public class AdapterEvents extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Event> allEvents = new ArrayList<>();
    private ArrayList<Event> adapterEvents = new ArrayList<>();
    private LayoutInflater inflater;
    private Sorter sorter = new Sorter();
    private DateFormat dateMonthFormat = new SimpleDateFormat("dd/MM");
    private DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM HH:mm");
    private DateFormat dateFormat = new SimpleDateFormat("dd");
    private DateFormat dayFormat = new SimpleDateFormat("EEEE");
    private DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    public AdapterEvents(Context context) {
        inflater = LayoutInflater.from(context);

    }

    public void setAllEvents(ArrayList<Event> events) {
        this.allEvents = events;
        if (!allEvents.isEmpty()) {
            sorter.sortEventsByDate(allEvents);
        }
    }
    public void setAdapterEvents(ArrayList<Event> events) {
        this.adapterEvents = events;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.recycler_row_event, parent, false);
            EventsHolder eventsHolder = new EventsHolder(view);
            return eventsHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            EventsHolder eventsHolder = (EventsHolder) holder;
            Event current = adapterEvents.get(position);
            eventsHolder.bind(current);
    }

    @Override
    public int getItemCount() {
        return adapterEvents.size();
    }

    class EventsHolder extends RecyclerView.ViewHolder {
        TextView weekDay;
        TextView monthDate;
        TextView eventName;
        TextView eventLocation;
        TextView eventTime;
        TextView description;
        LinearLayout locationLayout;
        LinearLayout saveEvent;
        LinearLayout shareEvent;
        ColorGenerator generator = ColorGenerator.MATERIAL;

        public EventsHolder(View view) {
            super(view);
            weekDay = (TextView) view.findViewById(R.id.week_day);
            monthDate = (TextView) view.findViewById(R.id.month_date);
            eventName = (TextView) view.findViewById(R.id.event_name);
            eventLocation = (TextView) view.findViewById(R.id.event_location);
            eventTime = (TextView) view.findViewById(R.id.event_time);
            description = (TextView) view.findViewById(R.id.event_description);
            locationLayout = (LinearLayout) view.findViewById(R.id.location_layout);
            saveEvent = (LinearLayout) view.findViewById(R.id.save_event);
            shareEvent = (LinearLayout) view.findViewById(R.id.share_event);

        }

        public void bind(Event e) {
            final Event event = e;

            if (event.getLocation().equals("")) {
                locationLayout.setVisibility(View.GONE);
            } else {
                locationLayout.setVisibility(View.VISIBLE);
                eventLocation.setText(event.getLocation());
            }


            if ((Long.valueOf(event.getEndDate().getTime()).compareTo(0L)) == 0) {
                if (dateMonthFormat.format(event.getEventDate()).equals(dateMonthFormat.format(event.getEndDate()))) {
                    eventTime.setText(timeFormat.format(event.getEventDate()) + " - " + timeFormat.format(event.getEndDate()));
                } else {
                    eventTime.setText(dateTimeFormat.format(event.getEventDate()) + " - " + dateTimeFormat.format(event.getEndDate()));
                }
            } else {
                eventTime.setText(timeFormat.format(event.getEventDate()));
            }

            String dayofweek = dayFormat.format(event.getEventDate());
            int color = generator.getColor(dateMonthFormat.format(event.getEventDate()));
            weekDay.setBackgroundColor(color);
            weekDay.setText(dayofweek);
            String dateofmonth = dateFormat.format(event.getEventDate());
            monthDate.setText(dateofmonth);
            eventName.setText(event.getEventName());
            String eventDescription = event.getDescription();
            if (eventDescription.length() > 100) {
                String descriptionWithLimit = eventDescription.substring(0, 99);
                description.setText(descriptionWithLimit + "...");
            } else {
                description.setText(eventDescription + "...");
            }

            saveEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SaveEventDialog viewDialog = new SaveEventDialog();
                    viewDialog.showDialog(v.getContext(), event, "Save Event", "Save event on your main calender");
                }
            });

            shareEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Student student = MyApplication.getWritableDatabase().readStudent(event.getStudentid());
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, student.getSchoolName() + ":\n" + event.getEventName() + "\n at : " + event.getLocation() + "\n on date: " + event.getEventDate() + "  " + timeFormat.format(event.getEventDate()) + " - " + timeFormat.format(event.getEndDate()));
                    sendIntent.setType("text/plain");
                    v.getContext().startActivity(sendIntent);
                }
            });

        }
    }
}
