package org.sturgeon.shuledial.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.FeeItem;

import java.util.ArrayList;


/**
 * Created by User on 06/02/2017.
 */

public class AdapterFeeItems extends RecyclerView.Adapter<AdapterFeeItems.FeeItemHolder>{
    private ArrayList<FeeItem> feeItems = new ArrayList<>();
    private LayoutInflater inflater;

    public AdapterFeeItems(Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setFeeItems(ArrayList<FeeItem> feeItems){
        this.feeItems = feeItems;
        notifyDataSetChanged();
    }

    @Override
    public FeeItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recycler_row_fee_item, parent, false);
        FeeItemHolder feeItemHolder = new FeeItemHolder(view);
        return feeItemHolder;
    }

    @Override
    public void onBindViewHolder(FeeItemHolder holder, int position) {
        FeeItem current = feeItems.get(position);
        holder.bind(current);
    }

    @Override
    public int getItemCount() {
        return feeItems.size();
    }

    class FeeItemHolder extends RecyclerView.ViewHolder{
        TextView feeItemName;
        TextView feeItemAmount;

        public FeeItemHolder(View itemView) {
            super(itemView);

            feeItemName = (TextView) itemView.findViewById(R.id.feeItemName);
            feeItemAmount = (TextView) itemView.findViewById(R.id.feeItemAmount);
        }

        public void bind(FeeItem feeItem){
            feeItemName.setText(feeItem.getName());
            feeItemAmount.setText(feeItem.getAmount());
        }
    }
}
