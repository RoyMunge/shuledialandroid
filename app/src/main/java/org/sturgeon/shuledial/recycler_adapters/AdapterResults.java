package org.sturgeon.shuledial.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.utils.SchoolPeriod;
import org.sturgeon.shuledial.utils.Sorter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by User on 12/06/2016.
 */
public class AdapterResults extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ExamResultsOverview> results = new ArrayList<>();
    private static final int TYPE_GRAPH = 0;
    private static final int TYPE_RESULTS = 1;
    private LayoutInflater inflater;
    private DateFormat mFormatter = new SimpleDateFormat("dd/MM");
    private LinkedHashMap<SchoolPeriod, Integer> mMapIndex;
    private Sorter sorter = new Sorter();

    public AdapterResults(Context context) {
        inflater = LayoutInflater.from(context);

    }

    public void setResults(ArrayList<ExamResultsOverview> results) {
        this.results = results;
        if (!results.isEmpty()) {
            sorter.sortExamResultsOverviewByDate(results);
            setSections();
            notifyDataSetChanged();
        }
    }

    private void setSections() {
        mMapIndex = new LinkedHashMap<>();

        for (int x = 0; x < results.size(); x++) {
            SchoolPeriod schoolPeriod = new SchoolPeriod(results.get(x).getExamDate());
            if (!mMapIndex.containsKey(schoolPeriod)) {
                mMapIndex.put(schoolPeriod, x);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_GRAPH) {
            View view = inflater.inflate(R.layout.recycler_row_graph, parent, false);
            GraphHolder graphHolder = new GraphHolder(view);
            return graphHolder;

        } else {
            View view = inflater.inflate(R.layout.recycler_row_results, parent, false);
            ResultsHolder resultsHolder = new ResultsHolder(view);
            return resultsHolder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_GRAPH;
        } else {
            return TYPE_RESULTS;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof GraphHolder) {

        } else {
            ResultsHolder resultsHolder = (ResultsHolder) holder;
            ExamResultsOverview current = results.get(position - 1);
            SchoolPeriod schoolPeriod = new SchoolPeriod(current.getExamDate());
            resultsHolder.bind(current, schoolPeriod, mMapIndex.get(schoolPeriod) == position - 1);
        }

    }

    @Override
    public int getItemCount() {
        return results.size() + 1;
    }

    class ResultsHolder extends RecyclerView.ViewHolder {
        TextView sectionHeader;
        TextView resultsDate;
        RatingBar resultsRatingBar;
        TextView resultsTotal;
        TextView resultsTotalVariable;
        TextView resultsAverage;
        TextView resultsAverageVariable;
        TextView resultsGrade;
        TextView resultsGradeVariable;
        TextView resultsPosition;
        TextView resultsPositionVariable;

        public ResultsHolder(View view) {
            super(view);
            sectionHeader = (TextView) view.findViewById(R.id.results_section_header);
            resultsDate = (TextView) view.findViewById(R.id.results_date);
            resultsRatingBar = (RatingBar) view.findViewById(R.id.results_ratingbar);
            resultsTotal = (TextView) view.findViewById(R.id.results_total);
            resultsTotalVariable = (TextView) view.findViewById(R.id.results_total_variable);
            resultsAverage = (TextView) view.findViewById(R.id.results_average);
            resultsAverageVariable = (TextView) view.findViewById(R.id.results_average_variable);
            resultsGrade = (TextView) view.findViewById(R.id.results_grade);
            resultsGradeVariable = (TextView) view.findViewById(R.id.results_grade_variable);
            resultsPosition = (TextView) view.findViewById(R.id.results_position);
            resultsPositionVariable = (TextView) view.findViewById(R.id.results_position_variable);

        }

        public void bind(ExamResultsOverview result, SchoolPeriod schoolPeriod, boolean showHeader) {
            sectionHeader.setText("Term " + Integer.toString(schoolPeriod.getTerm()) + " " + Integer.toString(schoolPeriod.getYear()));
            sectionHeader.setVisibility(showHeader ? View.VISIBLE : View.GONE);
            resultsDate.setText(result.getExamName());
            if (!result.getAverage().equals("")) {
                try {
                    resultsRatingBar.setRating(Float.valueOf(result.getAverage()) / 20.0F);
                } catch (NumberFormatException e) {
                    resultsRatingBar.setRating(0.0F);
                }
            }
            resultsTotalVariable.setText(result.getTotal());
            resultsAverageVariable.setText(result.getAverage());
            resultsGradeVariable.setText(result.getGrade());
            resultsPositionVariable.setText(result.getPosition());
        }
    }

    class GraphHolder extends RecyclerView.ViewHolder {

        private LineChart lineChart;
        private ArrayList<Entry> entries;
        private ArrayList<String> labels;
        private DateFormat mFormatter = new SimpleDateFormat("MMM");

        public GraphHolder(View view) {
            super(view);

            lineChart = (LineChart) view.findViewById(R.id.analysis_chart);
            Description description = new Description();
            description.setText("");
            lineChart.setDescription(description);
            lineChart.setDrawBorders(false);
            lineChart.getLegend().setEnabled(false);
            lineChart.setNoDataText("No Results have been uploaded by the school yet");

            lineChart.setTouchEnabled(true);
            CustomMarkerView mv = new CustomMarkerView(view.getContext(), R.layout.custom_marker_view_layout);
            lineChart.setMarkerView(mv);


            XAxis xAxis = lineChart.getXAxis();
            xAxis.setDrawGridLines(false);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            YAxis leftAxis = lineChart.getAxisLeft();
            leftAxis.enableGridDashedLine(10f, 10f, 0f);
            lineChart.getAxisRight().setEnabled(false);

            if (!results.isEmpty()) {
                createResultsGraph();
            }
        }

        public void createResultsGraph() {
            labels = new ArrayList<>();
            entries = new ArrayList<>();

            int count = 0;
            for (int i = results.size() - 1; i >= 0; i--) {
                labels.add(mFormatter.format(results.get(i).getExamDate()));
                try {
                    entries.add(new Entry(count, Float.valueOf(results.get(i).getTotal())));
                } catch (NumberFormatException e) {
                    entries.add(new Entry(count, 50F));
                }
                count++;
            }

            LineDataSet dataSet = new LineDataSet(entries, "Total Marks");
            dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(dataSet);
            LineData lineData = new LineData(dataSets);

            IAxisValueFormatter formatter = new IAxisValueFormatter() {

                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    return labels.get((int) value);
                }

                // we don't draw numbers, so no decimal digits needed
                @Override
                public int getDecimalDigits() {
                    return 0;
                }
            };

            XAxis xAxis = lineChart.getXAxis();
            xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
            xAxis.setValueFormatter(formatter);

            lineChart.setData(lineData);
            if (entries.size() > 4) {
                lineChart.setVisibleXRange(0, 4);
            }
            lineChart.moveViewToX(lineData.getXMax());
        }
    }

    public class CustomMarkerView extends MarkerView {

        private TextView tvContent;

        public CustomMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);
            // this markerview only displays a textview
            tvContent = (TextView) findViewById(R.id.tvContent);

        }

        // callbacks everytime the MarkerView is redrawn, can be used to update the
        // content (user-interface)
        @Override
        public void refreshContent(Entry e, Highlight highlight) {
            tvContent.setText("" + results.get(results.size() - 1 - Math.round(e.getX())).getExamName() + " : " + Utils.formatNumber(e.getY(), 0, true) + " marks");
            super.refreshContent(e, highlight);
        }

        @Override
        public MPPointF getOffset() {
            return new MPPointF(-(getWidth() / 2), -getHeight());
        }
    }
}
