package org.sturgeon.shuledial.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.Schools;
import org.sturgeon.shuledial.pojo.County;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.pojo.SchoolBioMinimal;
import org.sturgeon.shuledial.pojo.SchoolSearchDropdowns;
import org.sturgeon.shuledial.pojo.SchoolType;
import org.sturgeon.shuledial.tasks.TaskLoadSchoolsSearchedByCountyandType;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by User on 24/11/2016.
 */
public class AdapterSchools extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<SchoolBioMinimal> schoolsBioMinimal = new ArrayList<>();
    ArrayList<County> counties = new ArrayList<>();
    ArrayList<SchoolType> schoolTypes = new ArrayList<>();
    ArrayAdapter<County> countyAdapter;
    ArrayAdapter<SchoolType> categoryAdapter;

    private static final int TYPE_SEARCH=0;
    private static final int TYPE_SCHOOL=1;
    private LayoutInflater inflater;

    public AdapterSchools(Context context){
        inflater = LayoutInflater.from(context);
    }


    public void setSchoolsBio(ArrayList<SchoolBioMinimal> schoolsBioMinimal) {
        this.schoolsBioMinimal = schoolsBioMinimal;

        if(!schoolsBioMinimal.isEmpty()) {
            notifyDataSetChanged();
        }
    }

    public void setSchoolSearchDropdowns(SchoolSearchDropdowns schoolSearchDropdowns){
        counties.clear();
        schoolTypes.clear();
        counties.add(0, new County(Long.parseLong("963852741"),"All Counties"));
        schoolTypes.add(0, new SchoolType(Long.parseLong("963852741"),"All Categories"));
        counties.addAll(schoolSearchDropdowns.getCounties());
        schoolTypes.addAll(schoolSearchDropdowns.getCategories());
        countyAdapter.notifyDataSetChanged();
        categoryAdapter.notifyDataSetChanged();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_SEARCH){
            View view = inflater.inflate(R.layout.recycler_row_search, parent, false);
            SearchHolder searchHolder = new SearchHolder(view);
            return searchHolder;

        } else {
            View view = inflater.inflate(R.layout.recycler_row_school, parent, false);
            SchoolHolder searchHolder = new SchoolHolder(view);
            return searchHolder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0){
            return TYPE_SEARCH;
        }
        else {
            return TYPE_SCHOOL;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SearchHolder){

        } else {
            SchoolHolder schoolHolder = (SchoolHolder) holder;
            SchoolBioMinimal current = schoolsBioMinimal.get(position-1);
            schoolHolder.bind(current);
        }
    }

    @Override
    public int getItemCount() {
        return schoolsBioMinimal.size()+1;
    }


    class SearchHolder extends RecyclerView.ViewHolder{
        Spinner spinnerCounty;
        Spinner spinnerCategory;


        public SearchHolder(final View itemView) {
            super(itemView);
            spinnerCounty = (Spinner) itemView.findViewById(R.id.spinner_county);
            spinnerCategory = (Spinner) itemView.findViewById(R.id.spinner_type);

            countyAdapter = new ArrayAdapter<County>(itemView.getContext(),
                    android.R.layout.simple_spinner_item, counties);
            categoryAdapter = new ArrayAdapter<SchoolType>(itemView.getContext(),android.R.layout.simple_spinner_item, schoolTypes);
            spinnerCounty.setAdapter(countyAdapter);
            spinnerCounty.setSelection(0);
            spinnerCategory.setAdapter(categoryAdapter);
            spinnerCategory.setSelection(0);

            Button submitSearch = (Button) itemView.findViewById(R.id.submit_school_search);
            submitSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    County selectedCounty = (County) spinnerCounty.getSelectedItem();
                    SchoolType selectedType = (SchoolType) spinnerCategory.getSelectedItem();
                    new TaskLoadSchoolsSearchedByCountyandType((Schools) itemView.getContext(), selectedCounty.getCountyid().toString(), selectedType.getSchooltypeid().toString()).execute();

                }
            });
        }
    }

    class SchoolHolder extends RecyclerView.ViewHolder{
        TextView schoolName;
        TextView schoolMotto;
        TextView schoolPhysicalAddress;
        TextView schoolCounty;
        TextView tvSchoolBio;

        public SchoolHolder(View itemView) {
            super(itemView);
            schoolName = (TextView) itemView.findViewById(R.id.school_name);
            schoolMotto = (TextView) itemView.findViewById(R.id.school_motto);
            schoolPhysicalAddress = (TextView) itemView.findViewById(R.id.physical_address);
            schoolCounty = (TextView) itemView.findViewById(R.id.school_county);
            tvSchoolBio = (TextView) itemView.findViewById(R.id.school_bio);
        }

        void bind(SchoolBioMinimal schoolBioMinimal){
            schoolName.setText(schoolBioMinimal.getName());
            schoolMotto.setText(schoolBioMinimal.getMotto());
            schoolPhysicalAddress.setText(schoolBioMinimal.getPhysicalAddress());
            schoolCounty.setText(schoolBioMinimal.getCounty() + " COUNTY");
            tvSchoolBio.setText(schoolBioMinimal.getBio());
        }
    }
}
