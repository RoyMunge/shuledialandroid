package org.sturgeon.shuledial.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.Student;

import java.util.ArrayList;

/**
 * Created by User on 10/07/2016.
 */
public class AdapterStudents extends RecyclerView.Adapter<AdapterStudents.StudentHolder>{

    private ArrayList<Student> students = new ArrayList<>();
    private LayoutInflater inflater;

    public AdapterStudents(Context context){
        inflater = LayoutInflater.from(context);
    }

    public void setStudents(ArrayList<Student> students){
        if(!students.isEmpty()) {
            this.students = students;
            notifyDataSetChanged();
        }
    }

    @Override
    public StudentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recycler_row_student, parent, false);
        StudentHolder studentHolder = new StudentHolder(view);
        return studentHolder;
    }

    @Override
    public void onBindViewHolder(StudentHolder holder, int position) {
        Student current = students.get(position);
        holder.bind(current);
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    class StudentHolder extends RecyclerView.ViewHolder {
        ImageView studentDrawable;
        TextView studentName;
        TextView schoolName;

        public StudentHolder(View view){
            super(view);
            studentDrawable = (ImageView) view.findViewById(R.id.list_student_icon);
            studentName = (TextView) view.findViewById(R.id.list_student_name);
            schoolName = (TextView) view.findViewById(R.id.list_school_name);
            studentDrawable.setImageDrawable(new IconicsDrawable(view.getContext(), GoogleMaterial.Icon.gmd_delete_forever).actionBar().paddingDp(5).colorRes(R.color.textColorSecondary));
        }

        public void bind(Student student){
            studentName.setText(student.getStudentName());
            schoolName.setText(student.getSchoolName());
        }

        public TextDrawable getIcon(String date){
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getColor(date);
            TextDrawable.IBuilder builder = TextDrawable.builder()
                    .beginConfig()
                    .withBorder(0)
                    .endConfig()
                    .round();
            TextDrawable drawable = builder.build(date,color);
            return drawable;
        }
    }
}
