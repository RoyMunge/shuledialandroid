package org.sturgeon.shuledial.setup;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.callbacks.PhoneVerifiedListener;
import org.sturgeon.shuledial.student_accounts.AddStudentActivity;
import org.sturgeon.shuledial.student_accounts.ManageStudentsActivity;
import org.sturgeon.shuledial.tasks.TaskVerifyPhone;
import org.sturgeon.shuledial.utils.CustomVerifyEditText;
import org.sturgeon.shuledial.utils.SessionManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

public class CodeReader extends AppCompatActivity implements OTPListener,PhoneVerifiedListener {

    CustomVerifyEditText etVerificationCode;
    SessionManager sessionManager;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_reader);
        sessionManager = new SessionManager(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        String verificationCode = sessionManager.GetVerificationCode();
        String phoneNumber = sessionManager.GetUser();

        new TaskVerifyPhone(this,phoneNumber,verificationCode).execute();

        setTitle("Verification Code");

        OtpReader.bind(this, "ShuleDial");


        etVerificationCode = (CustomVerifyEditText) findViewById(R.id.etVerificationCode);

        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.accent), PorterDuff.Mode.MULTIPLY);
        progressBar.setVisibility(View.VISIBLE);

        LinearLayout signin = (LinearLayout) findViewById(R.id.tvResendCode);
        assert signin != null;
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CodeReader.this, VerifyPhone.class);
                startActivity(i);
                finish();
            }
        });

        Button btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmCode(etVerificationCode.getText().toString());
            }
        });

    }

    @Override
    public void otpReceived(String messageText) {
        Pattern p = Pattern.compile("(|^)\\d{6}");
        if (messageText != null) {
            Matcher m = p.matcher(messageText);
            if (m.find()) {
                final String code = m.group(0);
                etVerificationCode.setText(code);
                ConfirmCode(code);
            } else {
                //something went wrong
            }
        }
    }

    void ConfirmCode(String code){
        String storedCode = sessionManager.GetVerificationCode();
        String phoneNumber = sessionManager.GetUser();

        if (storedCode.equals(code)){
            progressBar.setVisibility(View.GONE);
            Toast.makeText(CodeReader.this, "verification successful", Toast.LENGTH_SHORT).show();
            sessionManager.createLoginSession(phoneNumber);
            Intent i=new Intent(CodeReader.this, StudentList.class);
            startActivity(i);
            finish();

        } else {
            progressBar.setVisibility(View.GONE);
            AlertDialog.Builder builder = new AlertDialog.Builder(CodeReader.this);
            builder.setTitle("Verification Error")
                    .setMessage("Invalid verification code")
                    .setNegativeButton("Retry", null)
                    .create()
                    .show();
        }
    }

    @Override
    public void onPhoneVerified(String verificationStatus) {
        if(verificationStatus != null){
            if(verificationStatus.equals("isSubscriber")){

            } else if(verificationStatus.equals("notSubscriber")){
                progressBar.setVisibility(View.GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(CodeReader.this);
                builder.setTitle("No Student Account")
                        .setMessage("No Student Account has been setup to your number. Setup Student Account first.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(CodeReader.this, AddStudentActivity.class));
                                finish();
                            }
                        })
                        .create()
                        .show();
            }
        } else{
            progressBar.setVisibility(View.GONE);
            AlertDialog.Builder builder = new AlertDialog.Builder(CodeReader.this);
            builder.setTitle("Error")
                    .setMessage("There seems to be an error verifying your phone number. Please confirm internet connectivity and try again.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(CodeReader.this, VerifyPhone.class));
                            finish();
                        }
                    })
                    .create()
                    .show();
        }
    }
}

