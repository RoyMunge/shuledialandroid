package org.sturgeon.shuledial.setup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.ResultsPage;
import org.sturgeon.shuledial.pojo.VerificationForm;
import org.sturgeon.shuledial.student_accounts.AddStudentActivity;
import org.sturgeon.shuledial.tasks.TaskVerifyPhone;
import org.sturgeon.shuledial.transport.VerificationRequest;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.CustomVerifyEditText;
import org.sturgeon.shuledial.utils.SessionManager;

import java.util.Random;

public class VerifyPhone extends AppCompatActivity {

    SessionManager sessionManager;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        sessionManager = new SessionManager(this);

        setTitle("Verify Phone Number");

        final CustomVerifyEditText etPhoneNumber = (CustomVerifyEditText) findViewById(R.id.etPhoneNumber);
        final Button btnVerify = (Button) findViewById(R.id.btnVerify);

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final View layout = v;

                if(!isOnline()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle("No Internet Connection")
                            .setMessage("please confirm internet connection first")
                            .setNegativeButton("Retry", null)
                            .create()
                            .show();
                }

                FormEditText[] allFields = {etPhoneNumber};
                boolean allValid = true;
                for (FormEditText field : allFields) {
                    allValid = field.testValidity() && allValid;
                }
                if (allValid) {
                    Random rnd = new Random();
                    int num = 100000 + (int)(rnd.nextFloat() * 899900);
                    String verificationCode = String.valueOf(num);
                    String phoneNumber = processPhoneNumber(etPhoneNumber.getText().toString());

                    sessionManager.saveVerificationCode(verificationCode);
                    sessionManager.savePhoneNumber(phoneNumber);

                    layout.getContext().startActivity(new Intent(VerifyPhone.this, CodeReader.class));
                    finish();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle("Phone Number Error")
                            .setMessage("phone number is not valid")
                            .setNegativeButton("Retry", null)
                            .create()
                            .show();
                }
            }
        });

    }

    private String processPhoneNumber(String phoneNumber){
        String intlPhoneNumber = new String();
        if (phoneNumber.startsWith("07")){
            intlPhoneNumber = phoneNumber.replaceFirst("07","2547");
        }
        return intlPhoneNumber;
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
