package org.sturgeon.shuledial.student_accounts;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.School;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.SessionManager;


public class AddStudentActivity extends AppCompatActivity {
    private static final String STATE_SCHOOL = "state_school";
    private static final String STATE_STUDENT = "state_student";
    public SessionManager sessionManager;
    public Button back;
    public Button next;
    public Student student;
    public School school;
    public CollapsingToolbarLayout collapsingToolbar;
    public String currentUserPhone;
    public RequestQueue requestQueue;
    public FrameLayout frameBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            school = savedInstanceState.getParcelable(STATE_SCHOOL);
            student = savedInstanceState.getParcelable(STATE_STUDENT);
        }

        setContentView(R.layout.activity_add_student);

        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        sessionManager = new SessionManager(this);
        currentUserPhone = sessionManager.GetUser();

        back = (Button) findViewById(R.id.button_back);
        next = (Button) findViewById(R.id.button_next);
        next.setEnabled(false);
        frameBackButton = (FrameLayout) findViewById(R.id.frame_back_button);

        collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.add_student_collapsing_toolbar);
        assert collapsingToolbar != null;
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.AppBarTitle);

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.addStudentContent, new SetupSchoolFragment());
            ft.commit();
        }

    }

    public void SetOnNextListener(View.OnClickListener onClickListener) {
        next.setOnClickListener(onClickListener);
    }

    public void SetOnBackListener(View.OnClickListener onClickListener) {
        back.setOnClickListener(onClickListener);
    }

    public void ManageFragements(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.addStudentContent, fragment);
        ft.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_SCHOOL, school);
        outState.putParcelable(STATE_STUDENT, student);
    }
}
