package org.sturgeon.shuledial.student_accounts;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.activities.Welcome;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;


/**
 * Created by User on 16/09/2016.
 */
public class ConfirmStudentFragment extends Fragment {
    Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            mActivity = (AddStudentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_confirm_student, container, false);

        if (mActivity instanceof AddStudentActivity) {
            final AddStudentActivity addStudent = (AddStudentActivity) mActivity;
            addStudent.collapsingToolbar.setTitle("Confirm Student Account");
            addStudent.next.setText("CONFIRM");
            addStudent.frameBackButton.setVisibility(View.VISIBLE);
            addStudent.next.setEnabled(true);

            TextView schoolName = (TextView) layout.findViewById(R.id.confirm_school_name);
            TextView admission = (TextView) layout.findViewById(R.id.confirm_student_admission);
            TextView studentName = (TextView) layout.findViewById(R.id.confirm_student_name);
            schoolName.setText(addStudent.school.getSchoolName());
            studentName.setText(addStudent.student.getStudentName());
            admission.setText(addStudent.student.getAdmission());

            addStudent.next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final View layout = v;
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println("*******the response of whether the request was successfull === " + response);
                            if (response != null && response.equals("success")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                                builder.setTitle("Request Submitted")
                                        .setMessage("You will be notified via SMS after approval to access the Student Account.")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                startActivity(new Intent(layout.getContext(), Welcome.class));
                                                getActivity().finish();
                                            }
                                        })
                                        .create()
                                        .show();

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                                builder.setTitle("Submission Error")
                                        .setMessage("Please try again.")
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }
                        }
                    };
                    Response.ErrorListener errorListener = new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            L.m(error + "");
                        }
                    };

                    Loader.addStudentAccount(VolleySingleton.getInstance().getRequestQueue(), addStudent.student.getStudentid().toString(), addStudent.currentUserPhone, responseListener, errorListener);
                }
            });

            addStudent.back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addStudent.ManageFragements(new SetupStudentFragment());
                }
            });
        }

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
