package org.sturgeon.shuledial.student_accounts;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.activities.MainActivity;
import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.recycler_adapters.AdapterStudents;
import org.sturgeon.shuledial.utils.SessionManager;

import java.util.ArrayList;

public class ManageStudentsActivity extends AppCompatActivity {
    private SessionManager sessionManager;
    private ArrayList<Student> students = new ArrayList<>();
    private AdapterStudents adapterStudents;
    private RecyclerView recyclerStudents;
    private String currentUserPhone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_students);
        sessionManager = new SessionManager(this);
        currentUserPhone = sessionManager.GetUser();

        Toolbar toolbar =
                (Toolbar) findViewById(R.id.manage_students_toolbar);
        toolbar.setTitle("Manage your students");

        recyclerStudents = (RecyclerView) findViewById(R.id.listStudents);
        recyclerStudents.setLayoutManager(new LinearLayoutManager(this));
        adapterStudents = new AdapterStudents(this);
        recyclerStudents.setAdapter(adapterStudents);

        students = MyApplication.getWritableDatabase().readStudents();
        if (!students.isEmpty()) {
            adapterStudents.setStudents(students);
        }

        ImageView addStudentIcon = (ImageView) findViewById(R.id.add_icon);
        addStudentIcon.setImageDrawable(new IconicsDrawable(this, GoogleMaterial.Icon.gmd_add).actionBar().paddingDp(5).colorRes(R.color.textColorSecondary));

        LinearLayout addStudent = (LinearLayout) findViewById(R.id.add_student_row);

        assert addStudent != null;
        addStudent.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(v.getContext(), AddStudentActivity.class));
                    }
                }
        );


        Button bntTakeMe = (Button) findViewById(R.id.btn_take_me);
        assert bntTakeMe != null;
        bntTakeMe.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (students.isEmpty()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                            builder.setMessage("Please add at least one student account")
                                    .setPositiveButton("OK", null)
                                    .create()
                                    .show();
                        } else {
                            startActivity(new Intent(v.getContext(), MainActivity.class));
                            finish();
                        }
                    }
                }
        );


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
