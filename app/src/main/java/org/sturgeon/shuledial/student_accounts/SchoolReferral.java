package org.sturgeon.shuledial.student_accounts;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.School;
import org.sturgeon.shuledial.transport.ReferralRequest;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.CustomVerifyEditText;

public class SchoolReferral extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_referral);
        setTitle("School Details");

        final CustomVerifyEditText etSchoolName = (CustomVerifyEditText) findViewById(R.id.school_name);
        final CustomVerifyEditText etSchoolLocation = (CustomVerifyEditText) findViewById(R.id.school_location);
        final CustomVerifyEditText etContactPerson = (CustomVerifyEditText) findViewById(R.id.contact_person);
        final CustomVerifyEditText etPhoneNumber = (CustomVerifyEditText) findViewById(R.id.phone_number);
        final CustomVerifyEditText etEmail = (CustomVerifyEditText) findViewById(R.id.email);

        Button btnSubmit = (Button) findViewById(R.id.referral_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FormEditText[] allFields = {etSchoolName, etSchoolLocation, etContactPerson, etPhoneNumber, etEmail};

                boolean allValid = true;
                for (FormEditText field : allFields) {
                    allValid = field.testValidity() && allValid;
                }
                if (allValid) {
                    Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            JSONObject jsonResponse = null;
                            try {
                                jsonResponse = response;
                                boolean isRegistered = jsonResponse.getBoolean("isRegistered");

                                if (isRegistered) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(SchoolReferral.this);
                                    builder.setTitle("Success")
                                            .setMessage("Details Sent Successfully. Thank you for your support")
                                            .setPositiveButton("OK", null)
                                            .create()
                                            .show();
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(SchoolReferral.this);
                                    builder.setTitle("Failed")
                                            .setMessage("System error")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    ReferralForm referralForm = new ReferralForm(etSchoolName.getText().toString(), etSchoolLocation.getText().toString(), etContactPerson.getText().toString(),etPhoneNumber.getText().toString(), etEmail.getText().toString());
                    ReferralRequest referralRequest = null;
                    try{
                        referralRequest = new ReferralRequest(new JSONObject(new Gson().toJson(referralForm)), responseListener);
                        VolleySingleton volleySingleton = VolleySingleton.getInstance();
                        RequestQueue queue = volleySingleton.getRequestQueue();
                        queue.add(referralRequest);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SchoolReferral.this);
                    builder.setTitle("Submission Error")
                            .setMessage("Please confirm input details")
                            .setNegativeButton("Retry", null)
                            .create()
                            .show();
                }
            }
        });

    }


    private class ReferralForm {
        private String schoolName;
        private String schoolLocation;
        private String contactPerson;
        private String phoneNumber;
        private String email;

        public ReferralForm(String schoolName, String schoolLocation, String contactPerson, String phoneNumber, String email) {
            this.schoolName = schoolName;
            this.schoolLocation = schoolLocation;
            this.contactPerson = contactPerson;
            this.phoneNumber = phoneNumber;
            this.email = email;
        }
    }
}
