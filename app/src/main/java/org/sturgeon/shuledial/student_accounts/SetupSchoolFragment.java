package org.sturgeon.shuledial.student_accounts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.sturgeon.shuledial.AutoCompleteTextView.DelayAutoCompleteTextView;
import org.sturgeon.shuledial.AutoCompleteTextView.SchoolAutoCompleteAdapter;
import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.School;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;


/**
 * Created by User on 15/09/2016.
 */
public class SetupSchoolFragment extends Fragment {
    Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            mActivity = (AddStudentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_setup_school, container, false);

        if (mActivity instanceof AddStudentActivity){
            final AddStudentActivity addStudent = (AddStudentActivity) mActivity;
            addStudent.collapsingToolbar.setTitle("Search School");
            addStudent.next.setText("NEXT");
            addStudent.frameBackButton.setVisibility(View.GONE);

            final Button showShortName = (Button) layout.findViewById(R.id.showShortName);
            final Button showSchoolName = (Button) layout.findViewById(R.id.showSchoolName);
            final FormEditText etShortName = (FormEditText) layout.findViewById(R.id.etShortName);
            final TextView instruction = (TextView) layout.findViewById(R.id.instruction);

            final DelayAutoCompleteTextView schoolName = (DelayAutoCompleteTextView) layout.findViewById(R.id.et_school_name);
            schoolName.setThreshold(3);
            schoolName.setAdapter(new SchoolAutoCompleteAdapter(getContext()));
            schoolName.setLoadingIndicator(
                    (android.widget.ProgressBar) layout.findViewById(R.id.pb_loading_indicator));
            schoolName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    School school = (School) adapterView.getItemAtPosition(position);
                    schoolName.setText(school.getSchoolName());
                    etShortName.setText(school.getSchoolShortName());
                    showShortName.setVisibility(View.GONE);
                    etShortName.setVisibility(View.VISIBLE);
                    addStudent.next.setEnabled(true);
                }
            });

            showShortName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    schoolName.setVisibility(View.GONE);
                    showShortName.setVisibility(View.GONE);
                    etShortName.setVisibility(View.VISIBLE);
                    showSchoolName.setVisibility(View.VISIBLE);
                    addStudent.next.setEnabled(true);
                    instruction.setText("Type school's short name to search for school.");
                }
            });

            showSchoolName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    schoolName.setVisibility(View.VISIBLE);
                    showShortName.setVisibility(View.VISIBLE);
                    etShortName.setVisibility(View.GONE);
                    showSchoolName.setVisibility(View.GONE);
                    addStudent.next.setEnabled(false);
                    instruction.setText("Type school name to search for school.");
                }
            });

            addStudent.SetOnNextListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final View layout = v;
                    FormEditText[] allFields = {etShortName};
                    boolean allValid = true;
                    for (FormEditText field : allFields) {
                        allValid = field.testValidity() && allValid;
                    }
                    if (allValid) {
                        Response.Listener<School> responseListener = new Response.Listener<School>() {
                            @Override
                            public void onResponse(School response) {
                                if(response != null){
                                    addStudent.school = response;
                                    addStudent.ManageFragements(new SetupStudentFragment());
                                } else{

                                }
                            }
                        };

                        Response.ErrorListener errorListener = new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                L.m(error + "");
                            }
                        };

                        Loader.loadSchool(VolleySingleton.getInstance().getRequestQueue(),etShortName.getText().toString(), responseListener, errorListener);

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                        builder.setTitle("Input Error")
                                .setMessage("Please confirm input")
                                .setNegativeButton("Retry", null)
                                .create()
                                .show();
                    }
                }
            });
        }

        LinearLayout referral = (LinearLayout) layout.findViewById(R.id.referral);
        referral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), SchoolReferral.class));
            }
        });



        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }



}
