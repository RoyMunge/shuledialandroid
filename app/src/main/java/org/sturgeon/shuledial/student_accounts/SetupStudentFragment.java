package org.sturgeon.shuledial.student_accounts;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreabaccega.widget.FormEditText;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

/**
 * Created by User on 27/07/2016.
 */
public class SetupStudentFragment extends Fragment {

    Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            mActivity = (AddStudentActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_setup_student, container, false);

        if (mActivity instanceof AddStudentActivity) {
            final AddStudentActivity addStudent = (AddStudentActivity) mActivity;
            addStudent.collapsingToolbar.setTitle(addStudent.school.getSchoolName());
            addStudent.frameBackButton.setVisibility(View.VISIBLE);
            addStudent.next.setText("NEXT");
            addStudent.back.setEnabled(true);

            final FormEditText admissionNumber = (FormEditText) layout.findViewById(R.id.etAdmissionNumber);
            admissionNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        addStudent.next.setEnabled(true);
                    }
                }
            });

            addStudent.next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final View layout = v;

                    FormEditText[] allFields = {admissionNumber};
                    boolean allValid = true;
                    for (FormEditText field : allFields) {
                        allValid = field.testValidity() && allValid;
                    }
                    if (allValid) {
                        Response.Listener<Student> responseListener = new Response.Listener<Student>() {
                            @Override
                            public void onResponse(Student response) {
                                if (response != null) {
                                    addStudent.student = response;
                                    addStudent.ManageFragements(new ConfirmStudentFragment());
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                                    builder.setTitle("No Student")
                                            .setMessage("No student with the specified admission number exists.")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }
                            }
                        };

                        Response.ErrorListener errorListener = new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                L.m(error + "");
                            }
                        };

                        Loader.loadStudent(VolleySingleton.getInstance().getRequestQueue(), admissionNumber.getText().toString(), addStudent.school.getSchoolShortName(), responseListener, errorListener);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(layout.getContext());
                        builder.setTitle("Input Error")
                                .setMessage("Please confirm input")
                                .setNegativeButton("Retry", null)
                                .create()
                                .show();
                    }
                }
            });

            addStudent.back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addStudent.ManageFragements(new SetupSchoolFragment());
                }
            });
        }


        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
