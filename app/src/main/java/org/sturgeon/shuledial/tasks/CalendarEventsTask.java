package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import org.sturgeon.shuledial.callbacks.CalendarEventsLoadedListener;
import org.sturgeon.shuledial.pojo.Event;

import java.util.ArrayList;

public class CalendarEventsTask extends AsyncTask<Void, Void, ArrayList<CalendarDay>> {
    private CalendarEventsLoadedListener myComponent;
    ArrayList<Event> events;

    public CalendarEventsTask(ArrayList<Event> events, CalendarEventsLoadedListener myComponent) {
        this.myComponent = myComponent;
        this.events = events;
    }

    @Override
    protected ArrayList<CalendarDay> doInBackground(@NonNull Void... voids) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ArrayList<CalendarDay> dates = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            CalendarDay calendarDay = CalendarDay.from(events.get(i).getEventDate());
            dates.add(calendarDay);
        }
        return dates;
    }

    @Override
    protected void onPostExecute(@NonNull ArrayList<CalendarDay> calendarDays) {
        super.onPostExecute(calendarDays);
        myComponent.onCalendarEventsLoaded(calendarDays);
    }
}