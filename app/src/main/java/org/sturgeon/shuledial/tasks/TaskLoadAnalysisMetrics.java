package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.AnalysisMetricLoadedListener;
import org.sturgeon.shuledial.pojo.AnalysisMetric;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public class TaskLoadAnalysisMetrics extends AsyncTask<Void, Void, ArrayList<AnalysisMetric>> {
    AnalysisMetricLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;
    private String examid;

    public TaskLoadAnalysisMetrics(AnalysisMetricLoadedListener myComponent, String studentid, String examid) {
        this.myComponent = myComponent;
        this.studentid = studentid;
        this.examid = examid;
        this.volleySingleton = VolleySingleton.getInstance();
        this.requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<AnalysisMetric> doInBackground(Void... params) {
        ArrayList<AnalysisMetric> analysisMetricArrayList = Loader.loadAnalysisMetric(requestQueue, studentid, examid);
        if(analysisMetricArrayList != null && !analysisMetricArrayList.isEmpty()) {
            MyApplication.getWritableDatabase().insertAnalysisMetric(analysisMetricArrayList, studentid, examid);
        }
        return analysisMetricArrayList;
    }

    @Override
    protected void onPostExecute(ArrayList<AnalysisMetric> analysisMetricArrayList) {
        if(myComponent!=null){
            myComponent.onAnalysisMetricLoaded(analysisMetricArrayList);
        }
    }
}