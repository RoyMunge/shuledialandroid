package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.AnnouncementsLoadedListener;
import org.sturgeon.shuledial.pojo.Announcement;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by User on 29/05/2016.
 */
public class TaskLoadAnnouncements extends AsyncTask<Void, Void, ArrayList<Announcement>> {
    private AnnouncementsLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;


    public TaskLoadAnnouncements(AnnouncementsLoadedListener myComponent, String studentid) {

        this.myComponent = myComponent;
        this.studentid = studentid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }


    @Override
    protected ArrayList<Announcement> doInBackground(Void... params) {

        ArrayList<Announcement> announcements = Loader.loadAnnouncements(requestQueue, studentid);
        return announcements;
    }

    @Override
    protected void onPostExecute(ArrayList<Announcement> announcements) {
        if (myComponent != null) {
            myComponent.onAnnouncementsLoaded(announcements);
        }
    }
}