package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.EventsLoadedListener;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by User on 29/05/2016.
 */
public class TaskLoadEvents extends AsyncTask<Void, Void, ArrayList<Event>> {
    private EventsLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;

    public TaskLoadEvents(EventsLoadedListener myComponent, String studentid) {

        this.myComponent = myComponent;
        this.studentid = studentid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }


    @Override
    protected ArrayList<Event> doInBackground(Void... params) {

        ArrayList<Event> events = Loader.loadEvents(requestQueue, studentid);
        return events;
    }

    @Override
    protected void onPostExecute(ArrayList<Event> events) {
        if (myComponent != null) {
            myComponent.onEventsLoaded(events);
        }
    }
}