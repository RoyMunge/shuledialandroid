package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.ExamMarksLoadedListener;
import org.sturgeon.shuledial.pojo.ExamMarks;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public class TaskLoadExamMarks extends AsyncTask<Void, Void, ArrayList<ExamMarks>> {
    ExamMarksLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;
    private String examid;

    public TaskLoadExamMarks(ExamMarksLoadedListener myComponent, String studentid, String examid) {
        this.myComponent = myComponent;
        this.studentid = studentid;
        this.examid = examid;
        this.volleySingleton = VolleySingleton.getInstance();
        this.requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<ExamMarks> doInBackground(Void... params) {
        ArrayList<ExamMarks> examMarksArrayList = Loader.loadExamMarks(requestQueue, studentid, examid);
        if(examMarksArrayList != null && !examMarksArrayList.isEmpty()) {
            MyApplication.getWritableDatabase().insertExamMarks(examMarksArrayList, studentid, examid);
        }
        return examMarksArrayList;
    }

    @Override
    protected void onPostExecute(ArrayList<ExamMarks> examMarksArrayList) {
        if(myComponent!=null){
            myComponent.onExamMarksLoaded(examMarksArrayList);
        }
    }
}
