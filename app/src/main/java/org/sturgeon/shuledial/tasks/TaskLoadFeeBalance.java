package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.FeeBalanceLoadedListener;
import org.sturgeon.shuledial.pojo.FeeBalance;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

/**
 * Created by User on 06/02/2017.
 */

public class TaskLoadFeeBalance extends AsyncTask<Void, Void, FeeBalance> {

    private FeeBalanceLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;

    public TaskLoadFeeBalance(FeeBalanceLoadedListener myComponent, String studentid){
        this.myComponent = myComponent;
        this.studentid = studentid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }


    @Override
    protected FeeBalance doInBackground(Void... voids) {
        FeeBalance feeBalance = Loader.loadFeeBalance(requestQueue,studentid);
        return feeBalance;
    }

    @Override
    protected void onPostExecute(FeeBalance feeBalance) {
        if (myComponent != null){
            myComponent.onFeeBalanceLoaded(feeBalance);
        }
    }
}
