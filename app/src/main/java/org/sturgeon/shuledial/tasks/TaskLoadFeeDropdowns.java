package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.FeeDropdownsLoadedListener;
import org.sturgeon.shuledial.pojo.FeeDropdowns;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

/**
 * Created by User on 06/02/2017.
 */

public class TaskLoadFeeDropdowns extends AsyncTask<Void, Void, FeeDropdowns> {

    private FeeDropdownsLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;

    public TaskLoadFeeDropdowns(FeeDropdownsLoadedListener myComponent, String studentid){
        this.myComponent = myComponent;
        this.studentid = studentid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected FeeDropdowns doInBackground(Void... voids) {
        FeeDropdowns feeDropdowns = Loader.loadFeeDropdowns(requestQueue,studentid);
        return feeDropdowns;
    }

    @Override
    protected void onPostExecute(FeeDropdowns feeDropdowns) {
        if (myComponent != null){
            myComponent.onFeeDropdownsLoaded(feeDropdowns);
        }
    }
}
