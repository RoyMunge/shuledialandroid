package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.FeeItemLoadedListener;
import org.sturgeon.shuledial.pojo.FeeItem;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by User on 06/02/2017.
 */

public class TaskLoadFeeItems extends AsyncTask<Void, Void, ArrayList<FeeItem>> {
    FeeItemLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String schoolperiodid;
    private String schoollevelid;

    public TaskLoadFeeItems(FeeItemLoadedListener myComponent, String schoolperiodid, String schoollevelid) {
        this.myComponent = myComponent;
        this.schoolperiodid = schoolperiodid;
        this.schoollevelid = schoollevelid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }


    @Override
    protected ArrayList<FeeItem> doInBackground(Void... voids) {
        ArrayList<FeeItem> feeItems = Loader.loadFeeItems(requestQueue, schoolperiodid, schoollevelid);
        return feeItems;
    }

    @Override
    protected void onPostExecute(ArrayList<FeeItem> feeItems) {
        if(myComponent != null){
            myComponent.onFeeItemLoaded(feeItems);
        }
    }
}
