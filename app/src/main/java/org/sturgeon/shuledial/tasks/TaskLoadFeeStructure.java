package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.FeeStructureLoadedListener;
import org.sturgeon.shuledial.pojo.FeeStructure;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public class TaskLoadFeeStructure extends AsyncTask<Void, Void, ArrayList<FeeStructure>> {
    FeeStructureLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;

    public TaskLoadFeeStructure(FeeStructureLoadedListener myComponent, String studentid) {
        this.myComponent = myComponent;
        this.studentid = studentid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<FeeStructure> doInBackground(Void... params) {
        ArrayList<FeeStructure> feeStructureArrayList = Loader.loadFeeStructure(requestQueue,studentid);
        if(feeStructureArrayList != null && !feeStructureArrayList.isEmpty()) {
            MyApplication.getWritableDatabase().insertFeeStructure(feeStructureArrayList, studentid);
        }
        return feeStructureArrayList;
    }

    @Override
    protected void onPostExecute(ArrayList<FeeStructure> feeStructureArrayList) {
        if(myComponent != null){
            myComponent.onFeeStructureLoaded(feeStructureArrayList);
        }
    }
}
