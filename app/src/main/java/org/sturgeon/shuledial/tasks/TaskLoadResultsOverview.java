package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.ExamResultsOverviewLoadedListener;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by Roy on 03/04/2018.
 */

public class TaskLoadResultsOverview extends AsyncTask<Void, Void, ArrayList<ExamResultsOverview>> {
    ExamResultsOverviewLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String studentid;

    public TaskLoadResultsOverview(ExamResultsOverviewLoadedListener myComponent, String studentid){
        this.myComponent = myComponent;
        this.studentid = studentid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<ExamResultsOverview> doInBackground(Void... params) {
        ArrayList<ExamResultsOverview> examResultsOverviewArrayList = Loader.loadExamResultsOverview(requestQueue, studentid);
        if(examResultsOverviewArrayList != null && !examResultsOverviewArrayList.isEmpty()) {
            MyApplication.getWritableDatabase().insertExamResultsOverview(examResultsOverviewArrayList, true);
        }
        return examResultsOverviewArrayList;
    }

    @Override
    protected void onPostExecute(ArrayList<ExamResultsOverview> examResultsOverviewArrayList) {
        if(myComponent!=null){
            myComponent.onExamResultsOverviewLoaded(examResultsOverviewArrayList);
        }
    }


}
