package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.SchoolDetailsLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

/**
 * Created by User on 12/02/2017.
 */

public class TaskLoadSchoolDetails extends AsyncTask<Void, Void, SchoolBio> {

    private SchoolDetailsLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String shortname;

    public TaskLoadSchoolDetails(SchoolDetailsLoadedListener myComponent, String shortname) {
        this.myComponent = myComponent;
        this.shortname = shortname;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected SchoolBio doInBackground(Void... voids) {
        SchoolBio schoolBio = Loader.loadSchoolBio(requestQueue, shortname);
        return schoolBio;
    }

    @Override
    protected void onPostExecute(SchoolBio schoolBio) {
        if (schoolBio != null){
            myComponent.onSchoolDetialsLoadedListener(schoolBio);
        }
    }
}
