package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.SchoolPeriodTotalLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolPeriodTotal;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

/**
 * Created by User on 06/02/2017.
 */

public class TaskLoadSchoolPeriodTotal extends AsyncTask<Void, Void, SchoolPeriodTotal> {

    SchoolPeriodTotalLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String schoolperiodid;
    private String schoollevelid;

    public TaskLoadSchoolPeriodTotal(SchoolPeriodTotalLoadedListener myComponent, String schoolperiodid, String schoollevelid) {
        this.myComponent = myComponent;
        this.schoolperiodid = schoolperiodid;
        this.schoollevelid = schoollevelid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected SchoolPeriodTotal doInBackground(Void... voids) {
        SchoolPeriodTotal schoolPeriodTotal = Loader.loadSchoolPeriodTotal(requestQueue,schoolperiodid, schoollevelid);
        return schoolPeriodTotal;
    }

    @Override
    protected void onPostExecute(SchoolPeriodTotal schoolPeriodTotal) {
        if(myComponent != null){
            myComponent.onSchoolPeriodTotalLoaded(schoolPeriodTotal);
        }
    }
}
