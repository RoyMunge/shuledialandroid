package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.SchoolSearchDropdownsLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolSearchDropdowns;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by User on 31/01/2017.
 */

public class TaskLoadSchoolSearchDropdowns extends AsyncTask<Void, Void, SchoolSearchDropdowns> {
    private SchoolSearchDropdownsLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    public TaskLoadSchoolSearchDropdowns(SchoolSearchDropdownsLoadedListener myComponent){
        this.myComponent = myComponent;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected SchoolSearchDropdowns doInBackground(Void... voids) {
        SchoolSearchDropdowns schoolSearchDropdowns = Loader.loadSchoolSearchDropdowns(requestQueue);
        return schoolSearchDropdowns;
    }

    @Override
    protected void onPostExecute(SchoolSearchDropdowns schoolSearchDropdowns) {
        if (myComponent != null){
            myComponent.onSchoolSearchDropdownLoaded(schoolSearchDropdowns);
        }
    }
}
