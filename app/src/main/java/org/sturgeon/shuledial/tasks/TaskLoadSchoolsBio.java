package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.SchoolsBioLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.pojo.SchoolBioMinimal;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by User on 15/06/2016.
 */
public class TaskLoadSchoolsBio extends AsyncTask<Void, Void, ArrayList<SchoolBioMinimal>> {
    SchoolsBioLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    public TaskLoadSchoolsBio(SchoolsBioLoadedListener myComponent){
        this.myComponent = myComponent;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<SchoolBioMinimal> doInBackground(Void... params) {
        ArrayList<SchoolBioMinimal> schoolsBio = Loader.loadSchoolsBio(requestQueue);
        return schoolsBio;
    }

    @Override
    protected void onPostExecute(ArrayList<SchoolBioMinimal> schoolsBio) {
        if(myComponent!=null){
            myComponent.onSchoolsBioLoadedListener(schoolsBio);
        }
    }
}
