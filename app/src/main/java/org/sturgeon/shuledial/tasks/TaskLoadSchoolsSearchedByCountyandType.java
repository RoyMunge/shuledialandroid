package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.callbacks.SchoolsBioLoadedListener;
import org.sturgeon.shuledial.pojo.SchoolBioMinimal;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by User on 31/01/2017.
 */

public class TaskLoadSchoolsSearchedByCountyandType extends AsyncTask<Void, Void, ArrayList<SchoolBioMinimal>>{
    private SchoolsBioLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String countyid;
    private String schooltypeid;

    public TaskLoadSchoolsSearchedByCountyandType(SchoolsBioLoadedListener myComponent, String countyid, String schooltypeid) {
        this.myComponent = myComponent;
        this.countyid = countyid;
        this.schooltypeid = schooltypeid;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }


    @Override
    protected ArrayList<SchoolBioMinimal> doInBackground(Void... voids) {
        ArrayList<SchoolBioMinimal> schoolsBio = Loader.loadSchoolsBioMinimalByCountyandType(requestQueue, countyid, schooltypeid);
        return schoolsBio;
    }

    @Override
    protected void onPostExecute(ArrayList<SchoolBioMinimal> schoolsBio) {
        if(myComponent!=null){
            myComponent.onSchoolsBioLoadedListener(schoolsBio);
        }
    }
}
