package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.callbacks.StudentsLoadedListener;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by Roy on 5/25/2017.
 */

public class TaskLoadStudents extends AsyncTask<Void, Void, ArrayList<Student>> {
    StudentsLoadedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String phoneNumber;

    public TaskLoadStudents(StudentsLoadedListener myComponent, String phoneNumber){
        this.myComponent = myComponent;
        this.phoneNumber = phoneNumber;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<Student> doInBackground(Void... params) {
        ArrayList<Student> students = Loader.loadStudents(requestQueue, phoneNumber);
        if (!students.isEmpty()) {
            MyApplication.getWritableDatabase().insertStudents(students, true);
        }
        return students;
    }

    @Override
    protected void onPostExecute(ArrayList<Student> students) {
        if(myComponent!=null){
            myComponent.onStudentsLoaded(students);
        }
    }
}
