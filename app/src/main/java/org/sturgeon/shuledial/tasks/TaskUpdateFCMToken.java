package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

/**
 * Created by Roy on 5/16/2017.
 */

public class TaskUpdateFCMToken extends AsyncTask<Void, Void, Void> {
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String phoneNumber;
    private String token;

    public TaskUpdateFCMToken(String phoneNumber, String token) {
        this.phoneNumber = phoneNumber;
        this.token = token;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }


    @Override
    protected Void doInBackground(Void... params) {
        Loader.updateToken(requestQueue, phoneNumber, token);
        return null;
    }
}
