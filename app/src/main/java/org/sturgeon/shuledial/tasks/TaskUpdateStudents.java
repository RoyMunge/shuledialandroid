package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

import java.util.ArrayList;

/**
 * Created by Roy on 5/16/2017.
 */

public class TaskUpdateStudents extends AsyncTask<Void, Void, ArrayList<Student>>{

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String phoneNumber;

    public TaskUpdateStudents(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<Student> doInBackground(Void... params) {
        ArrayList<Student> students = Loader.loadStudents(requestQueue, phoneNumber);
        if (!students.isEmpty() && students != null){
            for (Student student : students) {
                Student storedStudent = MyApplication.getWritableDatabase().readStudent(student.getStudentid());
                if (storedStudent == null){
                    MyApplication.getWritableDatabase().insertStudent(student);
                }
            }
        }
        return students;
    }
}
