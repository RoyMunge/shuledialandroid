package org.sturgeon.shuledial.tasks;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;
import org.sturgeon.shuledial.utils.SessionManager;

/**
 * Created by Roy on 5/16/2017.
 */

public class TaskUpdateValidUntil extends AsyncTask<Void, Void, String> {
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String phoneNumber;
    private SessionManager sessionManager;

    public TaskUpdateValidUntil(String phoneNumber, SessionManager sessionManager) {
        this.phoneNumber = phoneNumber;
        this.sessionManager = sessionManager;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }


    @Override
    protected String doInBackground(Void... params) {
        String validUntil = Loader.getValidUntil(requestQueue,phoneNumber);
        if (validUntil != null) {
            sessionManager.saveValidUntil(validUntil);
        }
        return null;
    }
}
