package org.sturgeon.shuledial.tasks;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.sturgeon.shuledial.callbacks.PhoneVerifiedListener;
import org.sturgeon.shuledial.pojo.VerificationForm;
import org.sturgeon.shuledial.setup.CodeReader;
import org.sturgeon.shuledial.setup.VerifyPhone;
import org.sturgeon.shuledial.student_accounts.AddStudentActivity;
import org.sturgeon.shuledial.transport.VerificationRequest;
import org.sturgeon.shuledial.transport.VolleySingleton;
import org.sturgeon.shuledial.utils.Loader;

/**
 * Created by Roy on 04/04/2018.
 */

public class TaskVerifyPhone extends AsyncTask<Void, Void, String> {
    private PhoneVerifiedListener myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String phoneNumber;
    private String verificationCode;

    public TaskVerifyPhone(PhoneVerifiedListener myComponent, String phoneNumber, String verificationCode) {
        this.myComponent = myComponent;
        this.phoneNumber = phoneNumber;
        this.verificationCode = verificationCode;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected String doInBackground(Void... params) {
        String verificationStatus = Loader.verifyPhone(requestQueue,phoneNumber,verificationCode);
        return verificationStatus;
    }

    @Override
    protected void onPostExecute(String verificationStatus) {
        if(myComponent != null) {
            myComponent.onPhoneVerified(verificationStatus);
        }
    }
}
