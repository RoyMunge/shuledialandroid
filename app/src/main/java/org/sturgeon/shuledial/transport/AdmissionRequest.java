package org.sturgeon.shuledial.transport;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by User on 30/01/2017.
 */

public class AdmissionRequest extends JsonObjectRequest {
    private static final String ADMISSION_REQUEST_URL = "http://core.shuledial.co.ke/mobileapp/admission";
    //private static final String ADMISSION_REQUEST_URL = "http://192.168.0.222:8080/mobileapp/admission";

    public AdmissionRequest(JSONObject admissionForm, Response.Listener<JSONObject> listener) {
        super(ADMISSION_REQUEST_URL, admissionForm, listener, null);
    }
}
