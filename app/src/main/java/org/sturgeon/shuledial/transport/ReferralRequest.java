package org.sturgeon.shuledial.transport;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by Roy on 5/27/2017.
 */

public class ReferralRequest extends JsonObjectRequest {

    //private static final String REFERRAL_REQUEST_URL = "http://192.168.0.222:8080/mobileapp/referral";
    private static final String REFERRAL_REQUEST_URL = "http://core.shuledial.co.ke/mobileapp/referral";

    public ReferralRequest(JSONObject refferalForm, Response.Listener<JSONObject> listener) {
        super(REFERRAL_REQUEST_URL, refferalForm, listener, null);
    }
}
