package org.sturgeon.shuledial.transport;


import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by User on 17/05/2016.
 */
public class RegisterRequest extends JsonObjectRequest {
    //private static final String REGISTER_REQUEST_URL = "http://192.168.0.222:8080/mobileapp/register";
    private static final String REGISTER_REQUEST_URL = "http://core.shuledial.co.ke/mobileapp/register";

    public RegisterRequest(JSONObject registrationForm, Response.Listener<JSONObject> listener) {
        super(REGISTER_REQUEST_URL, registrationForm, listener, null);
    }
}


