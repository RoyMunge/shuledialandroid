package org.sturgeon.shuledial.transport;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONObject;
import org.sturgeon.shuledial.logging.L;
import org.sturgeon.shuledial.pojo.AdmissionDropdowns;
import org.sturgeon.shuledial.pojo.AnalysisMetric;
import org.sturgeon.shuledial.pojo.Announcement;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.pojo.ExamMarks;
import org.sturgeon.shuledial.pojo.ExamResults;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.pojo.FeeBalance;
import org.sturgeon.shuledial.pojo.FeeDropdowns;
import org.sturgeon.shuledial.pojo.FeeItem;
import org.sturgeon.shuledial.pojo.FeeStructure;
import org.sturgeon.shuledial.pojo.School;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.pojo.SchoolBioMinimal;
import org.sturgeon.shuledial.pojo.SchoolPeriodTotal;
import org.sturgeon.shuledial.pojo.SchoolSearchDropdowns;
import org.sturgeon.shuledial.pojo.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by User on 28/05/2016.
 */
public class Requestor {

    public static ArrayList<Announcement> requestAnnouncement(RequestQueue requestQueue, String url) {
        ArrayList<Announcement> announcements = new ArrayList<>();
        RequestFuture<Announcement[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<Announcement[]> request = new GsonRequestGET<>(url, Announcement[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            Announcement[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            announcements = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return announcements;
        }
        return announcements;
    }

    public static ArrayList<Event> requestEvent(RequestQueue requestQueue, String url) {
        ArrayList<Event> events = new ArrayList<>();
        RequestFuture<Event[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<Event[]> request = new GsonRequestGET<>(url, Event[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            Event[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            events = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return events;
        }
        return events;
    }

    public static ArrayList<ExamResults> requestExamResults(RequestQueue requestQueue, String url) {
        ArrayList<ExamResults> examResults = new ArrayList<>();
        RequestFuture<ExamResults[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<ExamResults[]> request = new GsonRequestGET<>(url, ExamResults[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            ExamResults[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            examResults = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return examResults;
        }
        return examResults;
    }

    public static ArrayList<School> requestSchools(RequestQueue requestQueue, String url){
        ArrayList<School> schools = new ArrayList<>();
        RequestFuture<School[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<School[]> request = new GsonRequestGET<>(url, School[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            School[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            schools = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return schools;
        }
        return schools;
    }

    public static ArrayList<SchoolBio> requestSearchBio(RequestQueue requestQueue, String url){
        ArrayList<SchoolBio> schools = new ArrayList<>();
        RequestFuture<SchoolBio[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<SchoolBio[]> request = new GsonRequestGET<>(url, SchoolBio[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            SchoolBio[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            schools = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return schools;
        }
        return schools;
    }

    public static ArrayList<SchoolBioMinimal> requestSchoolsBio(RequestQueue requestQueue, String url){
        ArrayList<SchoolBioMinimal> schoolsBio = new ArrayList<>();
        RequestFuture<SchoolBioMinimal[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<SchoolBioMinimal[]> request = new GsonRequestGET<>(url, SchoolBioMinimal[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            SchoolBioMinimal[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            schoolsBio = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return schoolsBio;
        }
        return schoolsBio;
    }

    public static SchoolBio requestSchoolBio(RequestQueue requestQueue, String url){
        SchoolBio schoolBio = new SchoolBio();
        RequestFuture<SchoolBio> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<SchoolBio> request = new GsonRequestGET<>(url, SchoolBio.class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            schoolBio = requestFuture.get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return null;
        }
        return schoolBio;
    }

    public static SchoolSearchDropdowns requestSchoolSearchDropdowns(RequestQueue requestQueue, String url){
        SchoolSearchDropdowns schoolSearchDropdowns = new SchoolSearchDropdowns();
        RequestFuture<SchoolSearchDropdowns> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<SchoolSearchDropdowns> request = new GsonRequestGET<>(url, SchoolSearchDropdowns.class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            schoolSearchDropdowns = requestFuture.get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return null;
        }
        return schoolSearchDropdowns;
    }

    public static ArrayList<SchoolBioMinimal> requestSchoolsBioMinimalByCountyandType (RequestQueue requestQueue, String url){
        ArrayList<SchoolBioMinimal> schoolsBio = new ArrayList<>();
        RequestFuture<SchoolBioMinimal[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<SchoolBioMinimal[]> request = new GsonRequestGET<>(url, SchoolBioMinimal[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            SchoolBioMinimal[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            schoolsBio = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return schoolsBio;
        }
        return schoolsBio;
    }

    public static FeeBalance requestFeeBalance(RequestQueue requestQueue, String url){
        FeeBalance feeBalance = null;
        RequestFuture<FeeBalance> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<FeeBalance> request = new GsonRequestGET<>(url, FeeBalance.class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            FeeBalance response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            feeBalance = response;

        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return null;
        }

        return feeBalance;
    }

    public static FeeDropdowns requestFeeDropdowns(RequestQueue requestQueue, String url){
        FeeDropdowns feeDropdowns = null;
        RequestFuture<FeeDropdowns> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<FeeDropdowns> request = new GsonRequestGET<>(url, FeeDropdowns.class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            FeeDropdowns response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            feeDropdowns = response;

        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return null;
        }

        return feeDropdowns;
    }

    public static SchoolPeriodTotal requestSchoolPeriodTotal(RequestQueue requestQueue, String url){
        SchoolPeriodTotal schoolPeriodTotal = null;
        RequestFuture<SchoolPeriodTotal> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<SchoolPeriodTotal> request = new GsonRequestGET<>(url, SchoolPeriodTotal.class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            SchoolPeriodTotal response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            schoolPeriodTotal = response;

        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return null;
        }

        return schoolPeriodTotal;
    }

    public static ArrayList<FeeItem> requestFeeItem(RequestQueue requestQueue, String url) {
        ArrayList<FeeItem> feeItems = new ArrayList<>();
        RequestFuture<FeeItem[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<FeeItem[]> request = new GsonRequestGET<>(url, FeeItem[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            FeeItem[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            feeItems = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return feeItems;
        }
        return feeItems;
    }

    public static ArrayList<Student> requestStudents(RequestQueue requestQueue, String url) {
        ArrayList<Student> students = new ArrayList<>();
        RequestFuture<Student[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<Student[]> request = new GsonRequestGET<>(url, Student[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            Student[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            students = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return students;
        }
        return students;
    }

    public static ArrayList<ExamResultsOverview> requestExamResultsOverview(RequestQueue requestQueue, String url) {
        ArrayList<ExamResultsOverview> examResultsOverviewArrayList = new ArrayList<>();
        RequestFuture<ExamResultsOverview[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<ExamResultsOverview[]> request = new GsonRequestGET<>(url, ExamResultsOverview[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            ExamResultsOverview[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            examResultsOverviewArrayList = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return examResultsOverviewArrayList;
        }
        return examResultsOverviewArrayList;
    }

    public static ArrayList<ExamMarks> requestExamMarks(RequestQueue requestQueue, String url) {
        ArrayList<ExamMarks> examMarksArrayList = new ArrayList<>();
        RequestFuture<ExamMarks[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<ExamMarks[]> request = new GsonRequestGET<>(url, ExamMarks[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            ExamMarks[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            examMarksArrayList = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return examMarksArrayList;
        }
        return examMarksArrayList;
    }

    public static ArrayList<AnalysisMetric> requestAnalysisMetric(RequestQueue requestQueue, String url) {
        ArrayList<AnalysisMetric> analysisMetricArrayList = new ArrayList<>();
        RequestFuture<AnalysisMetric[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<AnalysisMetric[]> request = new GsonRequestGET<>(url, AnalysisMetric[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            AnalysisMetric[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            analysisMetricArrayList = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return analysisMetricArrayList;
        }
        return analysisMetricArrayList;
    }

    public static ArrayList<FeeStructure> requestFeeStructure(RequestQueue requestQueue, String url) {
        ArrayList<FeeStructure> feeStructureArrayList = new ArrayList<>();
        RequestFuture<FeeStructure[]> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<FeeStructure[]> request = new GsonRequestGET<>(url, FeeStructure[].class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            FeeStructure[] response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            feeStructureArrayList = new ArrayList<>(Arrays.asList(response));
        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return feeStructureArrayList;
        }
        return feeStructureArrayList;
    }

    public static void requestSchool(RequestQueue requestQueue, String url, Response.Listener<School> listener, Response.ErrorListener errorListener){
        GsonRequestGET<School> request = new GsonRequestGET<>(url, School.class, listener, errorListener);
        requestQueue.add(request);
    }

    public static void requestStudent(RequestQueue requestQueue, String url, Response.Listener<Student> listener, Response.ErrorListener errorListener){
        GsonRequestGET<Student> request = new GsonRequestGET<>(url, Student.class, listener, errorListener);
        requestQueue.add(request);
    }

    public static String stringResponseRequest(RequestQueue requestQueue, String url) {
        String response = null;
        RequestFuture<String> requestFuture = RequestFuture.newFuture();
        GsonRequestGET<String> request = new GsonRequestGET<>(url, String.class, requestFuture, requestFuture);

        requestQueue.add(request);
        try {
            String requestResponse = requestFuture.get(30000, TimeUnit.MILLISECONDS);
            response = requestResponse;

        } catch (InterruptedException e) {
            L.m(e + "");
        } catch (ExecutionException e) {
            L.m(e + "");
        } catch (TimeoutException e) {
            L.m(e + "");
        } catch (NullPointerException e) {
            L.m(e + "");
            return null;
        }

        return response;
    }

    public static void requestAdmissionDropdowns(RequestQueue requestQueue, String url ,Response.Listener<AdmissionDropdowns> listener, Response.ErrorListener errorListener){
        GsonRequestGET<AdmissionDropdowns> request = new GsonRequestGET<>(url, AdmissionDropdowns.class, listener, errorListener);
        requestQueue.add(request);
    }

    public static void addStudentAccount(RequestQueue requestQueue, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        GsonRequestGET<String> request = new GsonRequestGET<>(url, String.class, listener, errorListener);
        requestQueue.add(request);
    }
}
