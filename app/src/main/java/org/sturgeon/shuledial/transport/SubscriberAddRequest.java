package org.sturgeon.shuledial.transport;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by Roy on 5/29/2017.
 */

public class SubscriberAddRequest extends JsonObjectRequest {
    //private static final String SUBSCRIBER_ADD_REQUEST_URL = "http://192.168.0.222:8080/mobileapp/addsubscriber";
    private static final String SUBSCRIBER_ADD_REQUEST_URL = "http://core.shuledial.co.ke/mobileapp/addsubscriber";

    public SubscriberAddRequest(JSONObject subscriberAddForm, Response.Listener<JSONObject> listener) {
        super(SUBSCRIBER_ADD_REQUEST_URL, subscriberAddForm, listener, null);
    }
}
