package org.sturgeon.shuledial.transport;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by Roy on 5/11/2017.
 */

public class VerificationRequest  extends JsonObjectRequest{
    //private static final String VERIFICATION_REQUEST_URL = "http://192.168.0.222:8080/mobileapp/verification";
    private static final String VERIFICATION_REQUEST_URL = "http://core.shuledial.co.ke/mobileapp/verification";

    public VerificationRequest(JSONObject verificationForm, Response.Listener<JSONObject> listener) {
        super(VERIFICATION_REQUEST_URL, verificationForm, listener, null);
    }
}
