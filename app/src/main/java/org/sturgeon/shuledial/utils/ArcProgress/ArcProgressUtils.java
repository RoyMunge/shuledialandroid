package org.sturgeon.shuledial.utils.ArcProgress;

import android.content.res.Resources;

public final class ArcProgressUtils {

    private ArcProgressUtils() {
    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return  dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp){
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }
}