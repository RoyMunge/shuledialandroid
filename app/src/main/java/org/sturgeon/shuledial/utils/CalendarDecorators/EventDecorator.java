package org.sturgeon.shuledial.utils.CalendarDecorators;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import org.sturgeon.shuledial.utils.ColorGenerator;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by User on 27/06/2016.
 */
public class EventDecorator implements DayViewDecorator {

    private HashSet<CalendarDay> dates;
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    public EventDecorator(ArrayList<CalendarDay> dates){
        this.dates = new HashSet<>(dates);
    }

    private static Drawable generateCircleDrawable(final int color) {
        ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
        drawable.getPaint().setColor(color);
        return drawable;
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setBackgroundDrawable(generateCircleDrawable(Color.parseColor("#E91E63")));
        view.addSpan(new ForegroundColorSpan(0xFFFFFFFF));
        view.addSpan(new StyleSpan(Typeface.BOLD));
        view.addSpan(new RelativeSizeSpan(1.4f));

    }
}
