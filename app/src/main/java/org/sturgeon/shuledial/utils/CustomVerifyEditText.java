package org.sturgeon.shuledial.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import com.andreabaccega.widget.FormEditText;

/**
 * Created by User on 10/09/2016.
 */
public class CustomVerifyEditText extends FormEditText {
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            clearFocus();
        }

        return false;
    }
    public CustomVerifyEditText(Context context) {
        super(context);
    }

    public CustomVerifyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVerifyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
