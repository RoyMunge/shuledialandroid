package org.sturgeon.shuledial.utils;

import com.android.volley.RequestQueue;
import com.android.volley.Response;

import org.sturgeon.shuledial.application.MyApplication;
import org.sturgeon.shuledial.pojo.AdmissionDropdowns;
import org.sturgeon.shuledial.pojo.AnalysisMetric;
import org.sturgeon.shuledial.pojo.Announcement;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.pojo.ExamMarks;
import org.sturgeon.shuledial.pojo.ExamResults;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;
import org.sturgeon.shuledial.pojo.FeeBalance;
import org.sturgeon.shuledial.pojo.FeeDropdowns;
import org.sturgeon.shuledial.pojo.FeeItem;
import org.sturgeon.shuledial.pojo.FeeStructure;
import org.sturgeon.shuledial.pojo.School;
import org.sturgeon.shuledial.pojo.SchoolBio;
import org.sturgeon.shuledial.pojo.SchoolBioMinimal;
import org.sturgeon.shuledial.pojo.SchoolPeriodTotal;
import org.sturgeon.shuledial.pojo.SchoolSearchDropdowns;
import org.sturgeon.shuledial.pojo.Student;
import org.sturgeon.shuledial.transport.Requestor;

import java.util.ArrayList;

/**
 * Created by User on 28/05/2016.
 */
public class Loader {
    public static final String ANNOUNCEMENTS_URL = "http://core.shuledial.co.ke/mobileapp/announcements";
    public static final String EVENTS_URL = "http://core.shuledial.co.ke/mobileapp/events";
    public static final String RESULTS_MINIMAL_URL = "http://core.shuledial.co.ke/mobileapp/results";
    public static final String SCHOOLS_URL = "http://core.shuledial.co.ke/mobileapp/schools";
    public static final String SCHOOL_URL = "http://core.shuledial.co.ke/mobileapp/school";
    public static final String STUDENT_URL = "http://core.shuledial.co.ke/mobileapp/student";
    public static final String ADD_STUDENT_URL = "http://core.shuledial.co.ke/mobileapp/addstudent";
    public static final String SCHOOLSBIO_URL = "http://core.shuledial.co.ke/mobileapp/schoolsbio";
    public static final String SCHOOLBIO_URL = "http://core.shuledial.co.ke/mobileapp/schoolbio";
    public static final String SCHOOLBIOSEARCH_URL = "http://core.shuledial.co.ke/mobileapp/schoolsearchbio";
    public static final String ADMISSION_DROPDOWN_URL = "http://core.shuledial.co.ke/mobileapp/admissiondropdowns";
    public static final String SSCHOOL_SEARCH_DROPDOWN_URL = "http://core.shuledial.co.ke/mobileapp/schoolsearchdropdowns";
    public static final String SCHOOL_SEARCH_BY_COUNTY_AND_TYPE_URL = "http://core.shuledial.co.ke/mobileapp/schoolsearchbycountyandtype";
    public static final String FEE_DROPDOWN_URL = "http://core.shuledial.co.ke/mobileapp/feedropdowns";
    public static final String FEE_BALANCE_URL = "http://core.shuledial.co.ke/mobileapp/feebalance";
    public static final String SCHOOL_PERIOD_TOTAL_URL = "http://core.shuledial.co.ke/mobileapp/periodtotal";
    public static final String FEE_ITEMS_URL = "http://core.shuledial.co.ke/mobileapp/feeitems";
    public static final String STUDENTS_URL = "http://core.shuledial.co.ke/mobileapp/students";
    public static final String TOKEN_URL = "http://core.shuledial.co.ke/mobileapp/token";
    public static final String VALID_UNTIL_URL = "http://core.shuledial.co.ke/mobileapp/validuntil";
    public static final String EXAM_RESULTS_OVERVIEW_URL = "http://core.shuledial.co.ke/mobileapp/resultsoverview";
    public static final String EXAM_MARKS_URL = "http://core.shuledial.co.ke/mobileapp/exammarks";
    public static final String ANALYSIS_METRIC_URL = "http://core.shuledial.co.ke/mobileapp/analysismetricentries";
    public static final String FEE_STRUCTURE_URL = "http://core.shuledial.co.ke/mobileapp/feestructure";
    public static final String PHONE_VERIFICATION_URL = "http://core.shuledial.co.ke/mobileapp/verifyphone";


    /*public static final String ANNOUNCEMENTS_URL = "http://192.168.0.222:8080/mobileapp/announcements";
    public static final String EVENTS_URL = "http://192.168.0.222:8080/mobileapp/events";
    public static final String RESULTS_MINIMAL_URL = "http://192.168.0.222:8080/mobileapp/results";
    public static final String SCHOOLS_URL = "http://192.168.0.222:8080/mobileapp/schools";
    public static final String SCHOOL_URL = "http://192.168.0.222:8080/mobileapp/school";
    public static final String STUDENT_URL = "http://192.168.0.222:8080/mobileapp/student";
    public static final String ADD_STUDENT_URL = "http://192.168.0.222:8080/mobileapp/addstudent";
    public static final String SCHOOLSBIO_URL = "http://192.168.0.222:8080/mobileapp/schoolsbio";
    public static final String SCHOOLBIO_URL = "http://192.168.0.222:8080/mobileapp/schoolbio";
    public static final String SCHOOLBIOSEARCH_URL = "http://192.168.0.222:808requestStudent0/mobileapp/schoolsearchbio";
    public static final String ADMISSION_DROPDOWN_URL = "http://192.168.0.222:8080/mobileapp/admissiondropdowns";
    public static final String SSCHOOL_SEARCH_DROPDOWN_URL = "http://192.168.0.222:8080/mobileapp/schoolsearchdropdowns";
    public static final String SCHOOL_SEARCH_BY_COUNTY_AND_TYPE_URL = "http://192.168.0.222:8080/mobileapp/schoolsearchbycountyandtype";
    public static final String FEE_DROPDOWN_URL = "http://192.168.0.222:8080/mobileapp/feedropdowns";
    public static final String FEE_BALANCE_URL = "http://192.168.0.222:8080/mobileapp/feebalance";
    public static final String SCHOOL_PERIOD_TOTAL_URL = "http://192.168.0.222:8080/mobileapp/periodtotal";
    public static final String FEE_ITEMS_URL = "http://192.168.0.222:8080/mobileapp/feeitems";
    public static final String STUDENTS_URL = "http://192.168.0.222:8080/mobileapp/students";
    public static final String TOKEN_URL = "http://192.168.0.222:8080/mobileapp/token";
    public static final String VALID_UNTIL_URL = "http://192.168.0.222:8080/mobileapp/validuntil";
    public static final String EXAM_RESULTS_OVERVIEW_URL = "http://192.168.0.222:8080/mobileapp/resultsoverview";
    public static final String EXAM_MARKS_URL = "http://192.168.0.222:8080/mobileapp/exammarks";
    public static final String ANALYSIS_METRIC_URL = "http://192.168.0.222:8080/mobileapp/analysismetricentries";
    public static final String FEE_STRUCTURE_URL = "http://192.168.0.222:8080/mobileapp/feestructure";
    public static final String PHONE_VERIFICATION_URL = "http://192.168.0.222:8080/mobileapp/verifyphone";*/

    public static ArrayList<Announcement> loadAnnouncements(RequestQueue requestQueue, String studentid) {
        String url = String.format(ANNOUNCEMENTS_URL + "?studentid=%1$s", studentid);
        ArrayList<Announcement> announcements = Requestor.requestAnnouncement(requestQueue, url);
        if (!announcements.isEmpty()) {
            MyApplication.getWritableDatabase().insertAnnouncements(announcements, true);
        }
        return announcements;
    }

    public static ArrayList<Event> loadEvents(RequestQueue requestQueue, String studentid) {
        String url = String.format(EVENTS_URL + "?studentid=%1$s", studentid);
        ArrayList<Event> events = Requestor.requestEvent(requestQueue, url);
        if (!events.isEmpty()) {
            MyApplication.getWritableDatabase().insertEvents(events, true);
        }
        return events;
    }

    public static ArrayList<School> loadSchools(RequestQueue requestQueue, String searchString) {
        String search = searchString.replaceAll(" ", "+");
        String url = String.format(SCHOOLS_URL + "?searchstring=%1$s", search);
        ArrayList<School> schools = Requestor.requestSchools(requestQueue, url);
        return schools;
    }

    public static ArrayList<SchoolBio> loadSearchStringBio(RequestQueue requestQueue, String searchString) {
        String search = searchString.replaceAll(" ", "+");
        String url = String.format(SCHOOLBIOSEARCH_URL + "?searchstring=%1$s", search);
        ArrayList<SchoolBio> schools = Requestor.requestSearchBio(requestQueue, url);
        return schools;
    }

    public static ArrayList<SchoolBioMinimal> loadSchoolsBio(RequestQueue requestQueue) {
        String url = String.format(SCHOOLSBIO_URL);
        ArrayList<SchoolBioMinimal> schoolBios = Requestor.requestSchoolsBio(requestQueue, url);
        return schoolBios;
    }

    public static SchoolBio loadSchoolBio(RequestQueue requestQueue, String shortName) {
        String url = String.format(SCHOOLBIO_URL + "?shortname=%1$s", shortName);
        SchoolBio schoolBio = Requestor.requestSchoolBio(requestQueue, url);
        return schoolBio;
    }

    public static void loadSchool(RequestQueue requestQueue, String shortName, Response.Listener<School> listener, Response.ErrorListener errorListener) {
        String url = String.format(SCHOOL_URL + "?shortname=%1$s", shortName);
        Requestor.requestSchool(requestQueue, url,listener,errorListener);
    }

    public static void loadStudent(RequestQueue requestQueue,String admission, String shortname, Response.Listener<Student> listener, Response.ErrorListener errorListener) {
        String url = String.format(STUDENT_URL + "?admission=%1$s&shortname=%2$s", admission, shortname);
        Requestor.requestStudent(requestQueue, url,listener,errorListener);
    }

    public static void addStudentAccount(RequestQueue requestQueue, String studentid, String phonenumber, Response.Listener<String> listener, Response.ErrorListener errorListener){
        String url = String.format(ADD_STUDENT_URL + "?studentid=%1$s&phonenumber=%2$s", studentid, phonenumber);
        Requestor.addStudentAccount(requestQueue,url, listener, errorListener);
    }

    public static void loadAdmissionDropdowns(RequestQueue requestQueue, String schoolid,Response.Listener<AdmissionDropdowns> listener, Response.ErrorListener errorListener){
        String url = String.format(ADMISSION_DROPDOWN_URL + "?schoolid=%1$s", schoolid);
        Requestor.requestAdmissionDropdowns(requestQueue, url, listener, errorListener);
    }

    public static SchoolSearchDropdowns loadSchoolSearchDropdowns(RequestQueue requestQueue) {
        SchoolSearchDropdowns schoolSearchDropdowns =  Requestor.requestSchoolSearchDropdowns(requestQueue, SSCHOOL_SEARCH_DROPDOWN_URL);
        return schoolSearchDropdowns;
    }

    public static ArrayList<SchoolBioMinimal> loadSchoolsBioMinimalByCountyandType(RequestQueue requestQueue, String countyid, String schooltypeid) {
        String url = String.format(SCHOOL_SEARCH_BY_COUNTY_AND_TYPE_URL + "?countyid=%1$s&schooltypeid=%2$s", countyid, schooltypeid );
        ArrayList<SchoolBioMinimal> schoolBios = Requestor.requestSchoolsBioMinimalByCountyandType(requestQueue, url);
        return schoolBios;
    }

    public static FeeDropdowns loadFeeDropdowns(RequestQueue requestQueue, String studentid) {
        String url = String.format(FEE_DROPDOWN_URL + "?studentid=%1$s", studentid);
        FeeDropdowns feeDropdowns = Requestor.requestFeeDropdowns(requestQueue,url);
        return feeDropdowns;
    }

    public static FeeBalance loadFeeBalance(RequestQueue requestQueue, String studentid) {
        String url = String.format(FEE_BALANCE_URL + "?studentid=%1$s", studentid);
        FeeBalance feeBalance = Requestor.requestFeeBalance(requestQueue,url);
        if(feeBalance != null) {
            MyApplication.getWritableDatabase().insertFeeBalance(feeBalance, studentid);
        }
        return feeBalance;
    }

    public static SchoolPeriodTotal loadSchoolPeriodTotal(RequestQueue requestQueue, String schoolpediodid, String schoollevelid) {
        String url = String.format(SCHOOL_PERIOD_TOTAL_URL + "?schoolperiodid=%1$s&schoollevelid=%2$s", schoolpediodid, schoollevelid );
        SchoolPeriodTotal schoolPeriodTotal = Requestor.requestSchoolPeriodTotal(requestQueue,url);
        return schoolPeriodTotal;
    }

    public static ArrayList<FeeItem> loadFeeItems(RequestQueue requestQueue, String schoolpediodid, String schoollevelid) {
        String url = String.format(FEE_ITEMS_URL + "?schoolperiodid=%1$s&schoollevelid=%2$s", schoolpediodid, schoollevelid );
        ArrayList<FeeItem> feeItems = Requestor.requestFeeItem(requestQueue,url);
        return feeItems;
    }

    public static ArrayList<Student> loadStudents(RequestQueue requestQueue, String phoneNumber) {
        String url = String.format(STUDENTS_URL + "?phonenumber=%1$s", phoneNumber);
        ArrayList<Student> students = Requestor.requestStudents(requestQueue, url);
        return students;
    }

    public static ArrayList<ExamResultsOverview> loadExamResultsOverview(RequestQueue requestQueue, String studentid) {
        String url = String.format(EXAM_RESULTS_OVERVIEW_URL + "?studentid=%1$s", studentid);
        ArrayList<ExamResultsOverview> examResultsOverviewArrayList = Requestor.requestExamResultsOverview(requestQueue, url);
        return examResultsOverviewArrayList;
    }

    public static ArrayList<ExamMarks> loadExamMarks(RequestQueue requestQueue, String studentid, String examid) {
        String url = String.format(EXAM_MARKS_URL + "?studentid=%1$s&examid=%2$s", studentid, examid);
        ArrayList<ExamMarks> examMarksArrayList = Requestor.requestExamMarks(requestQueue, url);

        return examMarksArrayList;
    }

    public static ArrayList<AnalysisMetric> loadAnalysisMetric(RequestQueue requestQueue, String studentid, String examid) {
        String url = String.format(ANALYSIS_METRIC_URL + "?studentid=%1$s&examid=%2$s", studentid, examid);
        ArrayList<AnalysisMetric> analysisMetricArrayList = Requestor.requestAnalysisMetric(requestQueue, url);
        if(analysisMetricArrayList != null && analysisMetricArrayList.isEmpty()) {
            MyApplication.getWritableDatabase().insertAnalysisMetric(analysisMetricArrayList, studentid, examid);
        }
        return analysisMetricArrayList;
    }

    public static ArrayList<FeeStructure> loadFeeStructure(RequestQueue requestQueue, String studentid) {
        String url = String.format(FEE_STRUCTURE_URL + "?studentid=%1$s", studentid);
        ArrayList<FeeStructure> feeStructureArrayList = Requestor.requestFeeStructure(requestQueue, url);

        return feeStructureArrayList;
    }

    public static String verifyPhone(RequestQueue requestQueue, String phonenumber, String verificationCode){
        String url = String.format(PHONE_VERIFICATION_URL + "?phonenumber=%1$s&verificationcode=%2$s", phonenumber, verificationCode);
        String response = Requestor.stringResponseRequest(requestQueue,url);
        return response;
    }

    public static String updateToken(RequestQueue requestQueue, String phonenumber, String token){
        String url = String.format(TOKEN_URL + "?phonenumber=%1$s&token=%2$s", phonenumber, token);
        String response = Requestor.stringResponseRequest(requestQueue,url);
        return response;
    }

    public static String getValidUntil(RequestQueue requestQueue, String phonenumber){
        String url = String.format(VALID_UNTIL_URL + "?phonenumber=%1$s", phonenumber);
        Requestor.stringResponseRequest(requestQueue,url);
        String response = Requestor.stringResponseRequest(requestQueue,url);
        return response;
    }

}
