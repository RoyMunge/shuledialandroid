package org.sturgeon.shuledial.utils.RecyclerViewListener;

import android.view.View;

/**
 * Created by User on 18/06/2016.
 */
public interface ClickListener {
    public void onClick(View view, int position);

    public void onLongClick(View view, int position);
}
