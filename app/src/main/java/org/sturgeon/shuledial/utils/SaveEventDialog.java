package org.sturgeon.shuledial.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import org.sturgeon.shuledial.R;
import org.sturgeon.shuledial.pojo.Event;

public class SaveEventDialog {

    public void showDialog(Context activity, Event evt, String title, String msg) {
        final Context context = activity;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        final Event event = evt;

        TextView dialogTitle = (TextView) dialog.findViewById(R.id.dialogtitle);
        dialogTitle.setText(title);

        TextView dialogText = (TextView) dialog.findViewById(R.id.text_dialog);
        dialogText.setText(msg);

        Button btnDialogCancel = (Button) dialog.findViewById(R.id.btn_dialog_cancel);
        btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btnDialogOk = (Button) dialog.findViewById(R.id.btn_dialog_ok);
        btnDialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_INSERT)
                        .setData(CalendarContract.Events.CONTENT_URI)
                        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.getEventDate().getTime())
                        //.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, event.getEventDate().getTime()+60*60*1000)
                        .putExtra(CalendarContract.Events.TITLE, event.getEventName())
                        .putExtra(CalendarContract.Events.DESCRIPTION, event.getDescription())
                        .putExtra(CalendarContract.Events.EVENT_LOCATION, "")
                        .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true)
                        .putExtra(CalendarContract.Events.HAS_ALARM, true);

                v.getContext().startActivity(intent);

            }
        });

        dialog.show();

    }
}