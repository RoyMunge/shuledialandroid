package org.sturgeon.shuledial.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by User on 12/06/2016.
 */
public class SchoolPeriod {
    private int Year;
    private int Term;

    public SchoolPeriod(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int month = cal.get(Calendar.MONTH);
        this.Year = cal.get(Calendar.YEAR);

        if (month == 0 || month == 1 || month == 2 || month == 3){
            this.Term = 1;
        } else if (month == 4 || month == 5 || month == 6 || month == 7){
            this.Term = 2;
        } else{
            this.Term = 3;
        }
    }

    public int getYear() {
        return Year;
    }

    public int getTerm() {
        return Term;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof SchoolPeriod)){
            return false;
        } else {
            SchoolPeriod comparator = (SchoolPeriod) o;
            if(this.Year == comparator.getYear() && this.Term == comparator.getTerm()){
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        hashCode = hashCode * 37 + this.Year;
        hashCode = hashCode * 37 + this.Term;
        return hashCode;
    }
}
