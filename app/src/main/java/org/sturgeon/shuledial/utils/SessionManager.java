package org.sturgeon.shuledial.utils;

import android.content.Context;
import android.util.StringBuilderPrinter;

import org.sturgeon.shuledial.application.MyApplication;

/**
 * Created by User on 07/06/2016.
 */
public class SessionManager {
    Context context;
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_PHONE = "phoneNumber";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    public static final String KEY_CURRENT_STUDENT = "studentid";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_VERIFICATION_CODE = "VerificationCode";
    private static final String KEY_VALID_UNTIL = "ValidUntil";


    public SessionManager(Context context) {
        this.context = context;
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        MyApplication.saveToPreferences(context,IS_FIRST_TIME_LAUNCH, isFirstTime);
    }

    public boolean isFirstTimeLaunch() {
        return MyApplication.readFromPreferences(context, IS_FIRST_TIME_LAUNCH, true);
    }

    public void createLoginSession(String phoneNumber){
        MyApplication.saveToPreferences(context,IS_LOGIN,true);
        MyApplication.saveToPreferences(context,KEY_PHONE,phoneNumber);
    }

    public boolean isLoggedIn(){
        return MyApplication.readFromPreferences(context,IS_LOGIN,false);
    }

    public String GetUser(){
        return MyApplication.readFromPreferences(context,KEY_PHONE,"");
    }

    public void createStudentSession(String studentid){
        MyApplication.saveToPreferences(context, KEY_CURRENT_STUDENT,studentid);
    }

    public String GetCurrentStudent(){
        return MyApplication.readFromPreferences(context,KEY_CURRENT_STUDENT,"");
    }

    public void savePhoneNumber(String phoneNumber){
        MyApplication.saveToPreferences(context,KEY_PHONE,phoneNumber);
    }

    public void saveVerificationCode(String phoneNumber){
        MyApplication.saveToPreferences(context,KEY_VERIFICATION_CODE,phoneNumber);
    }

    public String GetVerificationCode(){
        return MyApplication.readFromPreferences(context,KEY_VERIFICATION_CODE,"");
    }

    public void saveValidUntil(String validUntil){
        MyApplication.saveToPreferences(context,KEY_VALID_UNTIL,validUntil);
    }

    public String GetValidUntil(){
        return MyApplication.readFromPreferences(context,KEY_VALID_UNTIL,"");
    }

    public void saveFCMToken(String token){
        MyApplication.saveToPreferences(context,KEY_TOKEN,token);
    }

    public String GetFCMToken(){
        return MyApplication.readFromPreferences(context,KEY_TOKEN,"");
    }
}
