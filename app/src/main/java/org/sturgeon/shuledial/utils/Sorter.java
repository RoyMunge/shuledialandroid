package org.sturgeon.shuledial.utils;

import org.sturgeon.shuledial.pojo.Announcement;
import org.sturgeon.shuledial.pojo.Event;
import org.sturgeon.shuledial.pojo.ExamResultsMinimal;
import org.sturgeon.shuledial.pojo.ExamResultsOverview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by User on 12/06/2016.
 */
public class Sorter {
    public void SortByPeriod(ArrayList<SchoolPeriod> schoolPeriods){
        Comparator<SchoolPeriod> comparator  = new Comparator<SchoolPeriod>() {
            @Override
            public int compare(SchoolPeriod lhs, SchoolPeriod rhs) {
                Integer yearlhs = lhs.getYear();
                Integer yearrhs = rhs.getYear();
                int sComp = yearrhs.compareTo(yearlhs);

                if (sComp != 0) {
                    return sComp;
                } else {
                    Integer termlhs = lhs.getTerm();
                    Integer termrhs = rhs.getTerm();
                    return termrhs.compareTo(termlhs);
                }
            }
        };

        Collections.sort(schoolPeriods,comparator);
    }

    public void sortAnnouncementsByDate(ArrayList<Announcement> announcements){

        Collections.sort(announcements, new Comparator<Announcement>() {
            @Override
            public int compare(Announcement lhs, Announcement rhs) {
                if(lhs.getPostingDate()!=null && rhs.getPostingDate()!=null)
                {
                    return rhs.getPostingDate().compareTo(lhs.getPostingDate());
                }
                else {
                    return 0;
                }

            }
        });
    }

    public void sortEventsByDate(ArrayList<Event> events){

        Collections.sort(events, new Comparator<Event>() {
            @Override
            public int compare(Event lhs, Event rhs) {
                if(lhs.getEventDate()!=null && rhs.getEventDate()!=null)
                {
                    return lhs.getEventDate().compareTo(rhs.getEventDate());
                }
                else {
                    return 0;
                }
            }
        });
    }

    public void sortExamResultsOverviewByDate(ArrayList<ExamResultsOverview> results){
        Collections.sort(results, new Comparator<ExamResultsOverview>() {
            @Override
            public int compare(ExamResultsOverview lhs, ExamResultsOverview rhs) {
                if(lhs.getExamDate()!=null && rhs.getExamDate()!=null){
                    return rhs.getExamDate().compareTo(lhs.getExamDate());
                } else{
                    return 0;
                }
            }
        });
    }

    public ArrayList<ExamResultsOverview> sortExamResultsOverviewByDateEarliestFirst(ArrayList<ExamResultsOverview> resultsMinimal){
        ArrayList<ExamResultsOverview> results = resultsMinimal;
        Collections.sort(results, new Comparator<ExamResultsOverview>() {
            @Override
            public int compare(ExamResultsOverview lhs, ExamResultsOverview rhs) {
                if(rhs.getExamDate()!=null && lhs.getExamDate()!=null){
                    return lhs.getExamDate().compareTo(rhs.getExamDate());
                } else{
                    return 0;
                }
            }
        });
        return results;
    }
}
